# Print4Help

[![pipeline status](https://gitlab.com/print4help/print4help/badges/main/pipeline.svg)](https://gitlab.com/print4help/print4help/-/commits/main)
[![coverage report](https://gitlab.com/print4help/print4help/badges/main/coverage.svg)](https://gitlab.com/print4help/print4help/-/commits/main)

## Development

### Start Backend

- install vagrant with hostsupdater plugin
- `cd api`
- `vagrant up`
- ... wait
- `vagrant ssh`
- `make init`

Run Tests: `make dev`

### Start Frontend

- `cd app`
- `nvm use`
- (`npm install yarn -g`) if you don't have yarn installed for this node version yet
- `yarn install`

### Translation

This project uses the [PHP-Translation Symfony Bundle](https://github.com/php-translation/symfony-bundle) 

If the pipeline fails due to missing translations:

- run `bin/console translation:extract app`
- The command will generate the missing translations in the correct translation yaml file
- Locate the blank `<target></target>` elements and enter the translated text.
  - Alternatively, open http://dev.api-print4help.org/translation/_trans and enter the missing translation in the form
- Repeat for every language to verify nothing is missing
  - `bin/console translation:check-missing en`
  - `bin/console translation:check-missing de`  
