<?php

declare(strict_types=1);

namespace App\Tests\Integration;

/** @coversNothing  */
final class DocumentationActionTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $response = self::get('/documentation.json');

        self::assertSuccessful();
        $this->assertMatchesJsonSnapshot($response);
    }
}
