<?php

declare(strict_types=1);

namespace App\Tests\Integration\Cart;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\RemovePieceFromCart;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\Command\ApprovePiece;
use App\Domain\Market\Piece\Command\RejectPiece;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class UpdateCartItemMetadataTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createAndApprovePiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::login('test@test.com');

        $response = self::post('/user/cart/update-cart-item-metadata', [
            'pieceId' => $piece->id,
            'purpose' => 'My old one broke',
            'requirements' => [
                'Color: any',
                'Material: PETG',
            ],
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testSuccessfulMultiple(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createAndApprovePiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::login('test@test.com');

        $response = self::post('/user/cart/update-cart-item-metadata', [
            'pieceId' => $piece->id,
            'purpose' => 'My old one broke',
            'requirements' => [
                'Color: any',
                'Material: PETG',
            ],
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);

        $response = self::post('/user/cart/update-cart-item-metadata', [
            'pieceId' => $piece->id,
            'purpose' => 'My old one broke because it was made of PLA',
            'requirements' => [
                'Color: grey',
                'Material: PETG or ASA',
            ],
        ]);

        $this->assertMatchesSnapshot($response);
    }

    public function invalidData(): iterable
    {
        yield ['purpose' => 1234, 'requirements' => ''];
        yield ['purpose' => str_repeat('A', 1001), 'requirements' => ''];
        yield ['purpose' => '', 'requirements' => ['test' => 'test']];
        yield ['purpose' => '', 'requirements' => ['test' => 1234]];
        yield ['purpose' => '', 'requirements' => ['test' => [1234]]];
        yield ['purpose' => '', 'requirements' => ['test' => ['test']]];
        yield ['purpose' => '', 'requirements' => ['valid', 'invalid.key' => 'invalid']];
        yield ['purpose' => '', 'requirements' => [str_repeat('A', 1001)]];
    }

    /**
     * @dataProvider invalidData
     */
    public function testValidationErrorPurposeAndRequirements(mixed $purpose, mixed $requirements): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createAndApprovePiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::login('test@test.com');

        $response = self::post('/user/cart/update-cart-item-metadata', [
            'pieceId' => $piece->id,
            'purpose' => $purpose,
            'requirements' => $requirements,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedPieceIsNotInCart(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createAndApprovePiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::dispatchCommand(new RemovePieceFromCart($user->getId(), $pieceId));

        self::login('test@test.com');

        $response = self::post('/user/cart/update-cart-item-metadata', [
            'pieceId' => $piece->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedUserHasNoCart(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createPiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece($pieceId, ''));

        self::login('test@test.com');

        $response = self::post('/user/cart/update-cart-item-metadata', [
            'pieceId' => $piece->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedPieceNotApproved(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createAndApprovePiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::dispatchCommand(new RejectPiece($pieceId, ''));

        self::login('test@test.com');

        $response = self::post('/user/cart/update-cart-item-metadata', [
            'pieceId' => $piece->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedPieceNotFound(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createAndApprovePiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));

        self::login('test@test.com');

        $response = self::post('/user/cart/update-cart-item-metadata', [
            'pieceId' => PieceId::create()->toString(),
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
