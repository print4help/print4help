<?php

declare(strict_types=1);

namespace App\Tests\Integration\Cart;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\UpdateCartItemMetadata;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class SubmitCartTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'), );
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );

        self::login('test@test.com');
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::dispatchCommand(new UpdateCartItemMetadata($user->getId(), $pieceId, 'My old one broke', [
            'Color: any',
            'Material: PETG',
        ]));

        $response = self::post('/user/cart/submit');

        self::assertSuccessful();
        self::assertStatusCode(201);
        $this->assertMatchesSnapshot($response);
    }
}
