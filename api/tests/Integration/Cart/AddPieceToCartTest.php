<?php

declare(strict_types=1);

namespace App\Tests\Integration\Cart;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\Command\ApprovePiece;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class AddPieceToCartTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createPiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece($pieceId, ''));
        self::login('test@test.com');

        $response = self::post('/user/cart/add-piece', [
            'pieceId' => $piece->id,
            'quantity' => 1,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(201);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailed(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createPiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::login('test@test.com');

        $response = self::post('/user/cart/add-piece', [
            'pieceId' => $piece->id,
            'quantity' => 0,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
