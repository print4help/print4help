<?php

declare(strict_types=1);

namespace App\Tests\Integration\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\Command\CloseInquiry;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Piece;
use App\Tests\Integration\BaseIntegrationTest;

class CloseInquiryTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        static::dispatchCommand(new SubmitCart($user->getId()));

        self::login('test@test.com');
        $inquiry = self::getInquiryByUser($user->getId());

        $response = self::post(sprintf('/inquiry/%s/close', $inquiry->id), [
            'inquiryId' => $inquiry->id,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testSuccessfulShowInMyList(): void
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        static::dispatchCommand(new SubmitCart($user->getId()));

        self::login('test@test.com');
        $inquiry = self::getInquiryByUser($user->getId());

        $closeResponse = self::post(sprintf('/inquiry/%s/close', $inquiry->id), [
            'inquiryId' => $inquiry->id,
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);

        $response = self::get('/inquiry/list-my');
        $this->assertMatchesSnapshot($response);
    }

    public function testRemoveClosedFromPublicList(): void
    {
        $maker = self::createUser(
            UserId::create(),
            Email::fromString('maker@test.com'),
        );

        /** @var Piece[] $pieces */
        $pieces = [
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 1'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 2'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 3'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 4'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 5'),
        ];

        $users = [];
        foreach ($pieces as $i => $piece) {
            $user = self::createUser(
                UserId::create(),
                Email::fromString(sprintf('user_%s@test.com', $i)),
            );

            static::dispatchCommand(
                new AddPieceToCart($user->getId(), PieceId::fromString($piece->id), 1 + $i)
            );
            static::dispatchCommand(
                new SubmitCart($user->getId())
            );
            $users[] = $user;
        }

        $inquiry = self::getInquiryByUser($users[0]->getId());

        self::login('user_0@test.com');
        $closeResponse = self::post(sprintf('/inquiry/%s/close', $inquiry->id), [
            'inquiryId' => $inquiry->id,
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);

        $responseListWithOneMissing = self::get('/inquiry/list');
        $this->assertMatchesSnapshot($responseListWithOneMissing);
    }

    public function testValidationFailedInquiryExists(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::login('test@test.com');

        $inquiryId = InquiryId::create();
        $response = self::post(sprintf('/inquiry/%s/close', $inquiryId->toString()), [
            'inquiryId' => $inquiryId->toString(),
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryStatusNotOpen(): void
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        static::dispatchCommand(new SubmitCart($user->getId()));

        self::login('test@test.com');
        $inquiry = self::getInquiryByUser($user->getId());
        self::dispatchCommand(new CloseInquiry(InquiryId::fromString($inquiry->id)));

        $response = self::post(sprintf('/inquiry/%s/close', $inquiry->id), [
            'inquiryId' => $inquiry->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
