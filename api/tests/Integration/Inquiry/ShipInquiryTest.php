<?php

declare(strict_types=1);

namespace App\Tests\Integration\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\Command\AbortProduction;
use App\Domain\Market\Inquiry\Command\FinishProduction;
use App\Domain\Market\Inquiry\Command\StartProduction;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\Command\AcceptOffer;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Offer;
use App\Tests\Integration\BaseIntegrationTest;

class ShipInquiryTest extends BaseIntegrationTest
{
    private static function provideOffer(
        string $makerEmail = 'maker@test.com',
        string $customerEmail = 'customer@test.com'
    ): Offer {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString($makerEmail));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $customer = self::createUser(UserId::create(), Email::fromString($customerEmail));
        self::dispatchCommand(new AddPieceToCart($customer->getId(), $pieceId, 10));
        self::dispatchCommand(new SubmitCart($customer->getId()));

        $offerId = OfferId::create();
        $inquiry = self::getInquiryByUser($customer->getId());
        self::dispatchCommand(new CreateOffer(
            $offerId,
            $maker->getId(),
            InquiryId::fromString($inquiry->id),
            'I can make only 5',
            5
        ));

        return self::getOfferByUser($maker->getId());
    }

    public function testSuccessfulFromStatusProductionStarted(): void
    {
        $offer = self::provideOffer();
        self::dispatchCommand(new AcceptOffer(OfferId::fromString($offer->id)));
        self::dispatchCommand(new StartProduction(InquiryId::fromString($offer->inquiryId)));

        self::login('maker@test.com');
        $response = self::post(sprintf('/inquiry/%s/ship', $offer->inquiryId), [
            'inquiryId' => $offer->inquiryId,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testSuccessfulFromStatusProductionFinished(): void
    {
        $offer = self::provideOffer();
        self::dispatchCommand(new AcceptOffer(OfferId::fromString($offer->id)));
        self::dispatchCommand(new FinishProduction(InquiryId::fromString($offer->inquiryId)));

        self::login('maker@test.com');
        $response = self::post(sprintf('/inquiry/%s/ship', $offer->inquiryId), [
            'inquiryId' => $offer->inquiryId,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testSuccessfulFromStatusInExecution(): void
    {
        $offer = self::provideOffer();
        self::dispatchCommand(new AcceptOffer(OfferId::fromString($offer->id)));

        self::login('maker@test.com');
        $response = self::post(sprintf('/inquiry/%s/ship', $offer->inquiryId), [
            'inquiryId' => $offer->inquiryId,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testIdsDoNotMatchException(): void
    {
        $offer1 = self::provideOffer();
        $offer2 = self::provideOffer('maker2@test.com', 'customer2@test.com');

        self::login('maker@test.com');
        $response = self::post(sprintf('/inquiry/%s/ship', $offer1->inquiryId), [
            'inquiryId' => $offer2->inquiryId,
        ]);

        self::assertClientError(400);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedFromStatusOpen(): void
    {
        $offer = self::provideOffer();

        self::login('maker@test.com');
        $response = self::post(sprintf('/inquiry/%s/ship', $offer->inquiryId), [
            'inquiryId' => $offer->inquiryId,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedFromStatusProductionAborted(): void
    {
        $offer = self::provideOffer();
        self::dispatchCommand(new AcceptOffer(OfferId::fromString($offer->id)));
        self::dispatchCommand(new AbortProduction(InquiryId::fromString($offer->inquiryId)));

        self::login('maker@test.com');
        $response = self::post(sprintf('/inquiry/%s/ship', $offer->inquiryId), [
            'inquiryId' => $offer->inquiryId,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryExists(): void
    {
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        self::login('maker@test.com');

        $inquiryId = InquiryId::create();
        $response = self::post(sprintf('/inquiry/%s/ship', $inquiryId->toString()), [
            'inquiryId' => $inquiryId->toString(),
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryDoesNotHaveOfferByCurrentUser(): void
    {
        $offer = self::provideOffer();
        $maker2 = self::createUser(UserId::create(), Email::fromString('maker2@test.com'));
        self::dispatchCommand(
            new CreateOffer(
                OfferId::create(),
                $maker2->getId(),
                InquiryId::fromString($offer->inquiryId),
                'I can make 10',
                10
            )
        );
        self::dispatchCommand(new AcceptOffer(OfferId::fromString($offer->id)));

        self::login('maker2@test.com');
        $response = self::post(sprintf('/inquiry/%s/ship', $offer->inquiryId), [
            'inquiryId' => $offer->inquiryId,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
