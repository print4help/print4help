<?php

declare(strict_types=1);

namespace App\Tests\Integration\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Inquiry;
use App\Tests\Integration\BaseIntegrationTest;

class GetInquiryTest extends BaseIntegrationTest
{
    private static function provideInquiry(): Inquiry
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $customer = self::createUser(UserId::create(), Email::fromString('customer@test.com'));
        self::dispatchCommand(new AddPieceToCart($customer->getId(), $pieceId, 10));
        self::dispatchCommand(new SubmitCart($customer->getId()));

        return self::getInquiryByUser($customer->getId());
    }

    public function testSuccessful(): void
    {
        $inquiry = self::provideInquiry();

        $response = self::get(sprintf('/inquiry/%s/detail', $inquiry->id));

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testSuccessfulWithOffers(): void
    {
        $inquiry = self::provideInquiry();
        $maker2 = self::createUser(UserId::create(), Email::fromString('maker2@test.com'));
        $maker3 = self::createUser(UserId::create(), Email::fromString('maker3@test.com'));
        self::dispatchCommand(
            new CreateOffer(
                OfferId::create(),
                $maker2->getId(),
                InquiryId::fromString($inquiry->id),
                'I can make only 5',
                5
            )
        );

        self::dispatchCommand(
            new CreateOffer(
                OfferId::create(),
                $maker3->getId(),
                InquiryId::fromString($inquiry->id),
                'I can make 10',
                10
            )
        );

        self::login('customer@test.com');
        $response = self::get(sprintf('/inquiry/%s/detail', $inquiry->id));

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesJsonSnapshot($response);
    }


    public function testSuccessfulWithoutOffers(): void
    {
        $inquiry = self::provideInquiry();
        $maker2 = self::createUser(UserId::create(), Email::fromString('maker2@test.com'));
        $maker3 = self::createUser(UserId::create(), Email::fromString('maker3@test.com'));
        self::dispatchCommand(
            new CreateOffer(
                OfferId::create(),
                $maker2->getId(),
                InquiryId::fromString($inquiry->id),
                'I can make only 5',
                5
            )
        );

        self::dispatchCommand(
            new CreateOffer(
                OfferId::create(),
                $maker3->getId(),
                InquiryId::fromString($inquiry->id),
                'I can make 10',
                10
            )
        );

        self::login('maker@test.com');
        $response = self::get(sprintf('/inquiry/%s/detail', $inquiry->id));

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesJsonSnapshot($response);
    }

    public function test404Error(): void
    {
        $response = self::get(sprintf('/inquiry/%s/detail', InquiryId::create()->toString()));
        self::assertClientError(404);
        $this->assertMatchesSnapshot($response);
    }
}
