<?php

declare(strict_types=1);

namespace App\Tests\Integration\Security;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Tests\Integration\BaseIntegrationTest;
use Symfony\Component\HttpFoundation\Response;

final class LoginTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );

        $response = self::post(
            '/login',
            [
                'username' => 'test@test.com',
                'password' => self::PASSWORD_DEFAULT,
            ]
        );

        self::assertSuccessful();
        self::assertArrayHasKey('data', $response);
        self::assertArrayHasKey('token', $response['data']);
    }

    public function testUserNotExistsFailure(): void
    {
        $response = self::post('/login', [
            'username' => 'not-existing@test.com',
            'password' => self::PASSWORD_DEFAULT,
        ]);

        self::assertClientError(401);
        $this->assertMatchesSnapshot($response);
    }

    public function testWrongPasswordFailure(): void
    {
        self::createUser(
            UserId::create(),
            Email::fromString('test@test.com')
        );

        $response = self::post('/login', ['username' => 'test@test.com', 'password' => 'invalid-password']);

        self::assertClientError(401);
        $this->assertMatchesSnapshot($response);
    }

    public function testNotAnEmailFailure(): void
    {
        $response = self::post('/login', ['username' => 'something-else', 'password' => 'invalid-password']);

        self::assertClientError(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertMatchesSnapshot($response);
    }
}
