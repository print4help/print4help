<?php

declare(strict_types=1);

namespace App\Tests\Integration\Chat;

use App\Domain\Account\Model\User;
use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Chat;
use App\Infrastructure\ReadModel\Repository\ChatRepository;
use App\Tests\Integration\BaseIntegrationTest;

class ListMyChatsTest extends BaseIntegrationTest
{
    private static function createChat(
        User $maker,
        User $customer,
        int $createdInquiry = 1
    ): Chat {
        $pieceId = PieceId::create();
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        self::dispatchCommand(new AddPieceToCart($customer->getId(), $pieceId, 10));
        self::dispatchCommand(new SubmitCart($customer->getId()));

        $offerId = OfferId::create();
        $inquiry = self::getInquiryByUser($customer->getId(), $createdInquiry);
        self::dispatchCommand(new CreateOffer(
            $offerId,
            $maker->getId(),
            InquiryId::fromString($inquiry->id),
            'I can make only 5',
            5
        ));

        $offer = self::getOfferByUser($maker->getId());

        /** @var ChatRepository $chatRepository */
        $chatRepository = self::getService(ChatRepository::class);
        return $chatRepository->getByOfferId($offer->id);
    }

    public function testSuccessfulMaker(): void
    {
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $customer = self::createUser(UserId::create(), Email::fromString('customer@test.com'));

        $chat = self::createChat($maker, $customer);

        self::login('maker@test.com');
        $response = self::get('/chat/list-my');

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    public function testSuccessfulCustomer(): void
    {
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $customer = self::createUser(UserId::create(), Email::fromString('customer@test.com'));

        $chat = self::createChat($maker, $customer);

        self::login('customer@test.com');
        $response = self::get('/chat/list-my');

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    public function testSuccessfulMultipleChats(): void
    {
        $maker1 = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $maker2 = self::createUser(UserId::create(), Email::fromString('maker2@test.com'));
        $maker3 = self::createUser(UserId::create(), Email::fromString('maker3@test.com'));
        $customer = self::createUser(UserId::create(), Email::fromString('customer@test.com'));

        $chat = self::createChat($maker1, $customer, 1);
        $chat2 = self::createChat($maker2, $customer, 2);
        $chat3 = self::createChat($maker3, $customer, 3);

        self::login('customer@test.com');
        $response = self::get('/chat/list-my');

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }
}
