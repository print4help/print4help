<?php

declare(strict_types=1);

namespace App\Tests\Integration\Chat;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\Command\CloseChat;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\MessageId;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Chat;
use App\Infrastructure\ReadModel\Repository\ChatRepository;
use App\Tests\Integration\BaseIntegrationTest;
use Symfony\Component\HttpFoundation\Response;

class UpdateMessageTest extends BaseIntegrationTest
{
    private static function provideChat(
        string $makerEmail = 'maker@test.com',
        string $customerEmail = 'customer@test.com'
    ): Chat {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString($makerEmail));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $customer = self::createUser(UserId::create(), Email::fromString($customerEmail));
        self::dispatchCommand(new AddPieceToCart($customer->getId(), $pieceId, 10));
        self::dispatchCommand(new SubmitCart($customer->getId()));

        $offerId = OfferId::create();
        $inquiry = self::getInquiryByUser($customer->getId());
        self::dispatchCommand(new CreateOffer(
            $offerId,
            $maker->getId(),
            InquiryId::fromString($inquiry->id),
            'I can make only 5',
            5
        ));

        $offer = self::getOfferByUser($maker->getId());

        /** @var ChatRepository $chatRepository */
        $chatRepository = self::getService(ChatRepository::class);
        return $chatRepository->getByOfferId($offer->id);
    }

    public function testSuccessfulByMaker(): void
    {
        $chat = self::provideChat();

        $messageIds = $chat->messageIds;
        self::assertArrayHasKey(0, $messageIds);

        self::login('maker@test.com');
        $response = self::post(sprintf('/chat/update-message/%s', $messageIds[0]), [
            'chatId' => $chat->id,
            'messageId' => $messageIds[0],
            'subject' => 'New Subject',
            'message' => 'Updated Message',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(Response::HTTP_OK);
        $this->assertMatchesSnapshot($response);
    }

    public function provideInvalidData(): iterable
    {
        yield ['subject' => 10, 'message' => 10];
        yield ['subject' => 'test', 'message' => null];
        yield ['subject' => 'test', 'message' => str_repeat('A', 1001)];
        yield ['subject' => str_repeat('A', 1001), 'message' => 'A'];
        yield ['subject' => str_repeat('A', 1001), 'message' => str_repeat('A', 1001)];
    }

    /**
     * @dataProvider provideInvalidData
     */
    public function testValidationFailed(mixed $subject, mixed $message): void
    {
        $chat = self::provideChat();
        $messageIds = $chat->messageIds;
        self::assertArrayHasKey(0, $messageIds);

        self::login('maker@test.com');
        $response = self::post(sprintf('/chat/update-message/%s', $messageIds[0]), [
            'chatId' => $chat->id,
            'messageId' => $messageIds[0],
            'subject' => $subject,
            'message' => $message,
        ]);

        self::assertClientError(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedChatStatusNotOpen(): void
    {
        $chat = self::provideChat();
        $messageIds = $chat->messageIds;
        self::assertArrayHasKey(0, $messageIds);

        self::dispatchCommand(new CloseChat(ChatId::fromString($chat->id)));

        self::login('maker@test.com');
        $response = self::post(sprintf('/chat/update-message/%s', $messageIds[0]), [
            'chatId' => $chat->id,
            'messageId' => $messageIds[0],
            'message' => 'Fine next Time!',
        ]);

        self::assertClientError(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedChatExists(): void
    {
        $chat = self::provideChat();
        $messageIds = $chat->messageIds;
        self::assertArrayHasKey(0, $messageIds);

        self::login('maker@test.com');

        $response = self::post(sprintf('/chat/update-message/%s', $messageIds[0]), [
            'chatId' => ChatId::create()->toString(),
            'messageId' => $messageIds[0],
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedMessageExists(): void
    {
        $chat = self::provideChat();

        self::login('maker@test.com');

        $unknownMessageId = MessageId::create()->toString();
        $response = self::post(sprintf('/chat/update-message/%s', $unknownMessageId), [
            'chatId' => $chat->id,
            'messageId' => $unknownMessageId,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedUserIsNotPartOfChat(): void
    {
        $chat = self::provideChat();
        $messageIds = $chat->messageIds;
        self::assertArrayHasKey(0, $messageIds);
        $chat2 = self::provideChat('maker2@test.com', 'customer2@test.com');

        self::login('maker@test.com');

        $response = self::post(sprintf('/chat/update-message/%s', $messageIds[0]), [
            'chatId' => $chat2->id,
            'messageId' => $messageIds[0],
            'message' => 'Can I write into your chat?',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedUserIsNotAuthor(): void
    {
        $chat = self::provideChat();
        $messageIds = $chat->messageIds;
        self::assertArrayHasKey(0, $messageIds);

        // customer is part of chat because he created the inquiry but is not author of the original message
        // (sent with the offer)
        self::login('customer@test.com');

        $response = self::post(sprintf('/chat/update-message/%s', $messageIds[0]), [
            'chatId' => $chat->id,
            'messageId' => $messageIds[0],
            'message' => 'Can I write into your chat?',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
