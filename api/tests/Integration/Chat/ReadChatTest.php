<?php

declare(strict_types=1);

namespace App\Tests\Integration\Chat;

use App\Domain\Account\Model\User;
use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\Command\SendMessage;
use App\Domain\Market\Offer\MessageId;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Chat;
use App\Infrastructure\ReadModel\Repository\ChatRepository;
use App\Tests\Integration\BaseIntegrationTest;
use Symfony\Component\HttpFoundation\Response;

class ReadChatTest extends BaseIntegrationTest
{
    private static function createChat(
        User $maker,
        User $customer,
        int $createdInquiry = 1
    ): Chat {
        $pieceId = PieceId::create();
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        self::dispatchCommand(new AddPieceToCart($customer->getId(), $pieceId, 10));
        self::dispatchCommand(new SubmitCart($customer->getId()));

        $offerId = OfferId::create();
        $inquiry = self::getInquiryByUser($customer->getId(), $createdInquiry);
        self::dispatchCommand(new CreateOffer(
            $offerId,
            $maker->getId(),
            InquiryId::fromString($inquiry->id),
            'I can make only 5',
            5
        ));

        $offer = self::getOfferByUser($maker->getId());

        /** @var ChatRepository $chatRepository */
        $chatRepository = self::getService(ChatRepository::class);
        return $chatRepository->getByOfferId($offer->id);
    }

    private static function sendMessage(ChatId $chatId, UserId $userId, string $body, string $subject = null): void
    {
        self::dispatchCommand(new SendMessage(
            $chatId,
            MessageId::create(),
            $userId,
            $body,
            $subject
        ));
    }

    public function testSuccessfulSeveralMessagesAsMaker(): void
    {
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $customer = self::createUser(UserId::create(), Email::fromString('customer@test.com'));

        $chat = self::createChat($maker, $customer);
        $chatId = ChatId::fromString($chat->id);

        self::sendMessage($chatId, $maker->getId(), 'I forgot to tell you, that I have printed this piece already several times!');
        self::sendMessage($chatId, $customer->getId(), 'That sounds great! How much do you want for it?');
        self::sendMessage($chatId, $maker->getId(), '5 Euros please.');
        self::sendMessage($chatId, $customer->getId(), 'Alright! Sounds good!');

        self::login('maker@test.com');
        $response = self::get(sprintf('/chat/read/%s', $chatId->toString()));

        self::assertSuccessful();
        $this->assertMatchesJsonSnapshot($response);
    }

    public function testSuccessfulSeveralMessagesAsCustomer(): void
    {
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $customer = self::createUser(UserId::create(), Email::fromString('customer@test.com'));

        $chat = self::createChat($maker, $customer);
        $chatId = ChatId::fromString($chat->id);

        self::sendMessage($chatId, $maker->getId(), 'I forgot to tell you, that I have printed this piece already several times!');
        self::sendMessage($chatId, $customer->getId(), 'That sounds great! How much do you want for it?');
        self::sendMessage($chatId, $maker->getId(), '5 Euros please.');
        self::sendMessage($chatId, $customer->getId(), 'Alright! Sounds good!');

        self::login('customer@test.com');
        $response = self::get(sprintf('/chat/read/%s', $chatId->toString()));

        self::assertSuccessful();
        $this->assertMatchesJsonSnapshot($response);
    }

    public function testChatNotFoundException(): void
    {
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));

        self::login('maker@test.com');
        $response = self::get(sprintf('/chat/read/%s', ChatId::create()->toString()));

        self::assertClientError(Response::HTTP_NOT_FOUND);
        $this->assertMatchesSnapshot($response);
    }

    public function testAccessDeniedException(): void
    {
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $customer = self::createUser(UserId::create(), Email::fromString('customer@test.com'));
        $maker2 = self::createUser(UserId::create(), Email::fromString('maker2@test.com'));

        $chat = self::createChat($maker, $customer);
        $chatId = ChatId::fromString($chat->id);

        self::login('maker2@test.com');
        $response = self::get(sprintf('/chat/read/%s', $chatId->toString()));

        self::assertClientError(Response::HTTP_FORBIDDEN);
        $this->assertMatchesSnapshot($response);
    }
}
