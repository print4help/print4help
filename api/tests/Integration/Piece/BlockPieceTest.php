<?php

declare(strict_types=1);

namespace App\Tests\Integration\Piece;

use App\Domain\Account\UserId;
use App\Domain\Admin\AdminUserId;
use App\Domain\Email;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class BlockPieceTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        self::login('test@test.com');
        self::post(sprintf('/piece/%s/request-review', $piece->id), [
            'id' => $piece->id,
            'message' => 'Please review my piece!',
        ]);

        $admin = self::createAdminUser(
            AdminUserId::create(),
            Email::fromString('admin@test.com')
        );
        self::login('admin@test.com');

        $response = self::post(sprintf('/piece/%s/block', $piece->id), [
            'id' => $piece->id,
            'message' => 'We don\'t allow these kind of pieces on our platform!',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testSameStatusValidationError(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        $admin = self::createAdminUser(
            AdminUserId::create(),
            Email::fromString('admin@test.com')
        );
        self::login('admin@test.com');

        self::post(sprintf('/piece/%s/block', $piece->id), [
            'id' => $piece->id,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);

        $response = self::post(sprintf('/piece/%s/block', $piece->id), [
            'id' => $piece->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testAccessDenied(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        self::login('test@test.com');

        self::post(sprintf('/piece/%s/block', $piece->id), [
            'id' => $piece->id,
        ]);

        self::assertClientError(403);
    }

    public function testIdsDoNotMatch(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        $admin = self::createAdminUser(
            AdminUserId::create(),
            Email::fromString('admin@test.com')
        );
        self::login('admin@test.com');

        $response = self::post(sprintf('/piece/%s/block', $piece->id), [
            'id' => '12345',
            'name' => 'AB',
            'summary' => '',
        ]);

        self::assertClientError(400);
        $this->assertMatchesSnapshot($response);
    }
}
