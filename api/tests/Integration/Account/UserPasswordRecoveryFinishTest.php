<?php

declare(strict_types=1);

namespace App\Tests\Integration\Account;

use App\Domain\Account\UserId;
use App\Domain\DateTimeHelper;
use App\Domain\Email;
use App\Tests\Integration\BaseIntegrationTest;
use DateTimeImmutable;

final class UserPasswordRecoveryFinishTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );

        $passwordRecovery = self::createUserPasswordRecovery($user);

        $response = self::post('/password/reset', [
            'token' => $passwordRecovery->getPasswordRecoveryToken(),
            'newPassword' => 'Abcdef1!',
            'newPasswordRepeat' => 'Abcdef1!',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);

        $response = self::post(
            '/login',
            [
                'username' => 'test@test.com',
                'password' => 'Abcdef1!',
            ]
        );

        self::assertSuccessful();
        self::assertArrayHasKey('data', $response);
        self::assertArrayHasKey('token', $response['data']);
    }

    public function testPasswordRecoveryTokenIsExpired(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );

        $dateInPast = new DateTimeImmutable();
        DateTimeHelper::override($dateInPast->modify('-48 Hours'));
        $passwordRecovery = self::createUserPasswordRecovery($user);
        DateTimeHelper::reset();

        $response = self::post('/password/reset', [
            'token' => $passwordRecovery->getPasswordRecoveryToken(),
            'newPassword' => 'Abcdef1!',
            'newPasswordRepeat' => 'Abcdef1!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testPasswordRecoveryTokenCanOnlyBeUsedOnce(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );

        $passwordRecovery = self::createUserPasswordRecovery($user);

        $response = self::post('/password/reset', [
            'token' => $passwordRecovery->getPasswordRecoveryToken(),
            'newPassword' => 'Abcdef1!',
            'newPasswordRepeat' => 'Abcdef1!',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);

        // 2nd try
        $response = self::post('/password/reset', [
            'token' => $passwordRecovery->getPasswordRecoveryToken(),
            'password' => 'Abcdef1!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testInvalidPasswordRecoveryToken(): void
    {
        $response = self::post('/password/reset', [
            'token' => 'invalid-token',
            'password' => 'Abcdef1!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
