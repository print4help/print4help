<?php

declare(strict_types=1);

namespace App\Tests\Integration\Account;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Tests\Integration\BaseIntegrationTest;

final class UserRegistrationFinishTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $userRegistration = $user->getRegistration();
        $token = $userRegistration->getRegistrationToken();

        $response = self::post('/registration/finish', [
            'registrationToken' => $token,
            'password' => 'Abcdef1!',
            'passwordRepeat' => 'Abcdef1!',
        ]);

        self::assertSuccessful();
    }

    public function testUserRegistrationTokenDoesNotExists(): void
    {
        $response = self::post('/registration/finish', [
            'registrationToken' => '1234567890',
            'password' => 'Abcdef1!',
            'passwordRepeat' => 'Abcdef1!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testPasswordIsTooSimple(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $userRegistration = $user->getRegistration();
        $token = $userRegistration->getRegistrationToken();

        $response = self::post('/registration/finish', [
            'registrationToken' => $token,
            'password' => '1234567890',
            'passwordRepeat' => '1234567890',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
