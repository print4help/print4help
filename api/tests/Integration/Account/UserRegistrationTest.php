<?php

declare(strict_types=1);

namespace App\Tests\Integration\Account;

use App\Tests\Integration\BaseIntegrationTest;

final class UserRegistrationTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $response = self::post('/registration', [
            'email' => 'test@test.com',
            'firstName' => 'John',
            'lastName' => 'Doe',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(201);
        $this->assertMatchesSnapshot($response);
    }

    public function testUserAlreadyExists(): void
    {
        $response = self::post('/registration', [
            'email' => 'test@test.com',
            'firstName' => 'John',
            'lastName' => 'Doe',
        ]);

        self::assertStatusCode(201);
        self::assertSuccessful();

        $response = self::post('/registration', [
            'email' => 'test@test.com',
            'firstName' => 'John',
            'lastName' => 'Doe',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidation(): void
    {
        $response = self::post('/registration', [
            'email' => 'invalid-email',
            'firstName' => '',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
