<?php

declare(strict_types=1);

namespace App\Tests\Integration\Account;

use App\Domain\Account\UserId;
use App\Domain\DateTimeHelper;
use App\Domain\Email;
use App\Tests\Integration\BaseIntegrationTest;
use DateTimeImmutable;

final class UserEmailChangeFinishTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $emailChange = self::createUserEmailChange($user, Email::fromString('new-email@test.com'));

        $response = self::post('/user/settings/change-email-finish', [
            'email' => 'new-email@test.com',
            'emailChangeToken' => $emailChange->getEmailChangeToken(),
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testEmailChangeTokenIsExpired(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $dateInPast = new DateTimeImmutable();
        DateTimeHelper::override($dateInPast->modify('-48 Hours'));
        $emailChange = self::createUserEmailChange($user, Email::fromString('new-email@test.com'));
        DateTimeHelper::reset();

        $response = self::post('/user/settings/change-email-finish', [
            'email' => 'new-email@test.com',
            'emailChangeToken' => $emailChange->getEmailChangeToken(),
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testEmailChangeTokenDoesNotExist(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $response = self::post('/user/settings/change-email-finish', [
            'email' => 'new-email@test.com',
            'emailChangeToken' => 'invalid-token',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
