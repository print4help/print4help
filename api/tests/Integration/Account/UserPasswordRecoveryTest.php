<?php

declare(strict_types=1);

namespace App\Tests\Integration\Account;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Tests\Integration\BaseIntegrationTest;

final class UserPasswordRecoveryTest extends BaseIntegrationTest
{
    public function testSuccessful(): void
    {
        self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );

        $response = self::post('/password/recovery', [
            'email' => 'test@test.com',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }
}
