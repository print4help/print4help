<?php

declare(strict_types=1);

namespace App\Tests\Integration\Offer;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\Command\CloseInquiry;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Inquiry;
use App\Tests\Integration\BaseIntegrationTest;

class CreateOfferTest extends BaseIntegrationTest
{
    private static function provideInquiry(): Inquiry
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $customer = self::createUser(UserId::create(), Email::fromString('customer@test.com'));
        self::dispatchCommand(new AddPieceToCart($customer->getId(), $pieceId, 10));
        self::dispatchCommand(new SubmitCart($customer->getId()));

        return self::getInquiryByUser($customer->getId());
    }

    public function testSuccessful(): void
    {
        $inquiry = self::provideInquiry();

        self::login('maker@test.com');
        $response = self::post('/offer/create', [
            'inquiryId' => $inquiry->id,
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(201);
        $this->assertMatchesSnapshot($response);
    }

    public function provideInvalidData(): iterable
    {
        yield ['quantity' => 10, 'message' => 0];
        yield ['quantity' => 10, 'message' => null];
        yield ['quantity' => 10, 'message' => ''];
        yield ['quantity' => 10, 'message' => str_repeat('A', 1001)];
        yield ['quantity' => -1, 'message' => 'A'];
        yield ['quantity' => 0, 'message' => 'A'];
        yield ['quantity' => null, 'message' => 'A'];
        yield ['quantity' => 101, 'message' => 'A'];
    }

    /**
     * @dataProvider provideInvalidData
     */
    public function testValidationFailed(mixed $quantity, mixed $message): void
    {
        $inquiry = self::provideInquiry();

        self::login('maker@test.com');
        $response = self::post('/offer/create', [
            'inquiryId' => $inquiry->id,
            'quantity' => $quantity,
            'message' => $message,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryStatusNotOpen(): void
    {
        $inquiry = self::provideInquiry();
        self::dispatchCommand(new CloseInquiry(InquiryId::fromString($inquiry->id)));

        self::login('maker@test.com');
        $response = self::post('/offer/create', [
            'inquiryId' => $inquiry->id,
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedUserHasNoOfferForThisInquiry(): void
    {
        $inquiry = self::provideInquiry();

        self::login('maker@test.com');
        self::post('/offer/create', [
            'inquiryId' => $inquiry->id,
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);
        self::assertSuccessful();

        $response = self::post('/offer/create', [
            'inquiryId' => $inquiry->id,
            'quantity' => 10,
            'message' => 'Glad to help you again!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryBelongsNotToCurrentUser(): void
    {
        $inquiry = self::provideInquiry();

        self::login('customer@test.com');
        $response = self::post('/offer/create', [
            'inquiryId' => $inquiry->id,
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryExists(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::login('test@test.com');
        $response = self::post('/offer/create', [
            'inquiryId' => InquiryId::create(),
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
