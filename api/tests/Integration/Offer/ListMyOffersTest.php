<?php

declare(strict_types=1);

namespace App\Tests\Integration\Offer;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Offer;
use App\Tests\Integration\BaseIntegrationTest;

class ListMyOffersTest extends BaseIntegrationTest
{
    private static function provideOffer(
        string $makerEmail = 'maker@test.com',
        string $customerEmail = 'customer@test.com'
    ): Offer {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString($makerEmail));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $customer = self::createUser(UserId::create(), Email::fromString($customerEmail));
        self::dispatchCommand(new AddPieceToCart($customer->getId(), $pieceId, 10));
        self::dispatchCommand(new SubmitCart($customer->getId()));

        $offerId = OfferId::create();
        $inquiry = self::getInquiryByUser($customer->getId());
        self::dispatchCommand(new CreateOffer(
            $offerId,
            $maker->getId(),
            InquiryId::fromString($inquiry->id),
            'I can make only 5',
            5
        ));

        return self::getOfferByUser($maker->getId());
    }

    public function testSuccessful(): void
    {
        $offer = self::provideOffer();

        self::login('maker@test.com');
        $response = self::get('/offer/list-my');

        self::assertSuccessful();
        $this->assertMatchesJsonSnapshot($response);
    }
}
