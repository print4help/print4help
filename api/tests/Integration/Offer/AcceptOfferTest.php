<?php

declare(strict_types=1);

namespace App\Tests\Integration\Offer;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\Command\CloseInquiry;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Offer;
use App\Tests\Integration\BaseIntegrationTest;

class AcceptOfferTest extends BaseIntegrationTest
{
    private static function provideOffer(
        string $makerEmail = 'maker@test.com',
        string $customerEmail = 'customer@test.com'
    ): Offer {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString($makerEmail));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $customer = self::createUser(UserId::create(), Email::fromString($customerEmail));
        self::dispatchCommand(new AddPieceToCart($customer->getId(), $pieceId, 10));
        self::dispatchCommand(new SubmitCart($customer->getId()));

        $offerId = OfferId::create();
        $inquiry = self::getInquiryByUser($customer->getId());
        self::dispatchCommand(new CreateOffer(
            $offerId,
            $maker->getId(),
            InquiryId::fromString($inquiry->id),
            'I can make only 5',
            5
        ));

        return self::getOfferByUser($maker->getId());
    }

    public function testSuccessful(): void
    {
        $offer = self::provideOffer();
        self::login('customer@test.com');
        $response = self::post(sprintf('/offer/%s/accept', $offer->id), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $offer->id,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testSuccessfulMultipleOffers(): void
    {
        $offer = self::provideOffer();
        $maker2 = self::createUser(UserId::create(), Email::fromString('maker2@test.com'));
        $maker3 = self::createUser(UserId::create(), Email::fromString('maker3@test.com'));

        $inquiryId = InquiryId::fromString($offer->inquiryId);
        self::dispatchCommand(
            new CreateOffer(OfferId::create(), $maker2->getId(), $inquiryId, 'I can make 10', 10)
        );
        self::dispatchCommand(
            new CreateOffer(OfferId::create(), $maker3->getId(), $inquiryId, 'I can make 20', 20)
        );

        self::login('customer@test.com');

        $responseBefore = self::get(sprintf('/inquiry/%s/detail', $offer->inquiryId));

        self::post(sprintf('/offer/%s/accept', $offer->id), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $offer->id,
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);

        $responseAfter = self::get(sprintf('/inquiry/%s/detail', $offer->inquiryId));
        self::assertSuccessful();
        self::assertStatusCode(200);

        $this->assertMatchesJsonSnapshot([
            'beforeAccepting' => $responseBefore,
            'afterAccepting' => $responseAfter,
        ]);
    }

    public function testIdsDoNotMatchException(): void
    {
        $offer1 = self::provideOffer();
        $offer2 = self::provideOffer('maker2@test.com', 'customer2@test.com');

        self::login('maker@test.com');
        $response = self::post(sprintf('/offer/%s/accept', $offer1->id), [
            'inquiryId' => $offer1->inquiryId,
            'offerId' => $offer2->id,
        ]);

        self::assertClientError(400);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryStatusNotOpen(): void
    {
        $offer = self::provideOffer();

        self::dispatchCommand(new CloseInquiry(InquiryId::fromString($offer->inquiryId)));

        self::login('customer@test.com');
        $response = self::post(sprintf('/offer/%s/accept', $offer->id), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $offer->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryExists(): void
    {
        $offer = self::provideOffer();
        self::login('maker@test.com');

        $response = self::post(sprintf('/offer/%s/accept', $offer->id), [
            'inquiryId' => InquiryId::create()->toString(),
            'offerId' => $offer->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedOfferDoesNotExist(): void
    {
        $offer = self::provideOffer();
        self::login('maker@test.com');

        $unknownOfferId = OfferId::create();
        $response = self::post(sprintf('/offer/%s/accept', $unknownOfferId->toString()), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $unknownOfferId->toString(),
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryBelongsToCurrentUser(): void
    {
        $wrongUser = self::createUser(UserId::create(), Email::fromString('maker2@test.com'));
        $offer = self::provideOffer();

        self::login('maker2@test.com');
        $response = self::post(sprintf('/offer/%s/accept', $offer->id), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $offer->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedOfferBelongsToInquiry(): void
    {
        $offer1 = self::provideOffer();
        $offer2 = self::provideOffer('maker2@test.com', 'customer2@test.com');

        self::login('customer@test.com');
        $response = self::post(sprintf('/offer/%s/accept', $offer2->id), [
            'inquiryId' => $offer1->inquiryId,
            'offerId' => $offer2->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
