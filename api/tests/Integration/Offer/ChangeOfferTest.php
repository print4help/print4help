<?php

declare(strict_types=1);

namespace App\Tests\Integration\Offer;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\Command\CloseInquiry;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Offer;
use App\Tests\Integration\BaseIntegrationTest;

class ChangeOfferTest extends BaseIntegrationTest
{
    private static function provideOffer(
        string $makerEmail = 'maker@test.com',
        string $customerEmail = 'customer@test.com'
    ): Offer {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString($makerEmail));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $customer = self::createUser(UserId::create(), Email::fromString($customerEmail));
        self::dispatchCommand(new AddPieceToCart($customer->getId(), $pieceId, 10));
        self::dispatchCommand(new SubmitCart($customer->getId()));

        $offerId = OfferId::create();
        $inquiry = self::getInquiryByUser($customer->getId());
        self::dispatchCommand(new CreateOffer(
            $offerId,
            $maker->getId(),
            InquiryId::fromString($inquiry->id),
            'I can make only 5',
            5
        ));

        return self::getOfferByUser($maker->getId());
    }

    public function testSuccessful(): void
    {
        $offer = self::provideOffer();

        self::login('maker@test.com');
        $response = self::post(sprintf('/offer/%s/change', $offer->id), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $offer->id,
            'quantity' => 10,
            'message' => 'Just checked my filament and I can only offer to make 4. sorry..',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    public function testIdsDoNotMatchException(): void
    {
        $offer1 = self::provideOffer();
        $offer2 = self::provideOffer('maker2@test.com', 'customer2@test.com');

        self::login('maker@test.com');
        $response = self::post(sprintf('/offer/%s/change', $offer1->id), [
            'inquiryId' => $offer1->inquiryId,
            'offerId' => $offer2->id,
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(400);
        $this->assertMatchesSnapshot($response);
    }

    public function provideInvalidData(): iterable
    {
        yield ['quantity' => 10, 'message' => 0];
        yield ['quantity' => 10, 'message' => null];
        yield ['quantity' => 10, 'message' => ''];
        yield ['quantity' => 10, 'message' => str_repeat('A', 1001)];
        yield ['quantity' => -1, 'message' => 'A'];
        yield ['quantity' => 0, 'message' => 'A'];
        yield ['quantity' => null, 'message' => 'A'];
        yield ['quantity' => 101, 'message' => 'A'];
    }

    /**
     * @dataProvider provideInvalidData
     */
    public function testValidationFailed(mixed $quantity, mixed $message): void
    {
        $offer = self::provideOffer();

        self::login('maker@test.com');
        $response = self::post(sprintf('/offer/%s/change', $offer->id), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $offer->id,
            'quantity' => $quantity,
            'message' => $message,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryStatusNotOpen(): void
    {
        $offer = self::provideOffer();

        self::dispatchCommand(new CloseInquiry(InquiryId::fromString($offer->inquiryId)));

        self::login('maker@test.com');
        $response = self::post(sprintf('/offer/%s/change', $offer->id), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $offer->id,
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedInquiryExists(): void
    {
        $offer = self::provideOffer();
        self::login('maker@test.com');

        $response = self::post(sprintf('/offer/%s/change', $offer->id), [
            'inquiryId' => InquiryId::create(),
            'offerId' => $offer->id,
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedOfferDoesNotExist(): void
    {
        $offer = self::provideOffer();
        self::login('maker@test.com');

        $unknownOfferId = OfferId::create();
        $response = self::post(sprintf('/offer/%s/change', $unknownOfferId->toString()), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $unknownOfferId->toString(),
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedOfferBelongsToCurrentUser(): void
    {
        $wrongUser = self::createUser(UserId::create(), Email::fromString('maker2@test.com'));
        $offer = self::provideOffer();

        self::login($wrongUser->getEmail()->toString());
        $response = self::post(sprintf('/offer/%s/change', $offer->id), [
            'inquiryId' => $offer->inquiryId,
            'offerId' => $offer->id,
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    public function testValidationFailedOfferBelongsToInquiry(): void
    {
        $offer1 = self::provideOffer();
        $offer2 = self::provideOffer('maker2@test.com', 'customer2@test.com');

        self::login('maker@test.com');
        $response = self::post(sprintf('/offer/%s/change', $offer1->id), [
            'inquiryId' => $offer2->inquiryId,
            'offerId' => $offer1->id,
            'quantity' => 10,
            'message' => 'Glad to help you!',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
