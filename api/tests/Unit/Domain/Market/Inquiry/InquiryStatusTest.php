<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Market\Inquiry;

use App\Domain\Market\Inquiry\InquiryStatus;
use PHPUnit\Framework\TestCase;

class InquiryStatusTest extends TestCase
{
    public function providePositiveStatusList(): iterable
    {
        foreach (InquiryStatus::keys() as $status) {
            yield ['statusList' => InquiryStatus::values(), $status];
        }
    }

    /**
     * @covers       \App\Domain\Market\Inquiry\InquiryStatus
     * @dataProvider providePositiveStatusList
     */
    public function testEqualsOneOfSuccessful(array $statusList, string $status): void
    {
        $inquiry = InquiryStatus::fromString($status);
        self::assertTrue($inquiry->equalsOneOf($statusList));
    }

    public function provideNegativeStatusList(): iterable
    {
        foreach (InquiryStatus::keys() as $i => $status) {
            $statusList = InquiryStatus::values();
            array_splice($statusList, $i, 1);
            yield ['statusList' => $statusList, $status];
        }
    }

    /**
     * @covers       \App\Domain\Market\Inquiry\InquiryStatus
     * @dataProvider provideNegativeStatusList
     */
    public function testEqualsOneOfNotSuccessful(array $statusList, string $status): void
    {
        $inquiry = InquiryStatus::fromString($status);
        self::assertFalse($inquiry->equalsOneOf($statusList));
    }
}
