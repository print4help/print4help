<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Market\Inquiry\Handler;

use App\Domain\Account\UserId;
use App\Domain\Market\Inquiry\Command\Ship;
use App\Domain\Market\Inquiry\Exception\InquiryNotInExpectedStatus;
use App\Domain\Market\Inquiry\Handler\ShipHandler;
use App\Domain\Market\Inquiry\Inquiry;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Piece\PieceId;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class ShipHandlerTest extends TestCase
{
    use ProphecyTrait;

    public static function provideInquiriesWithInvalidStatus(): iterable
    {
        $open = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        yield [$open];

        $closed = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $closed->close();
        yield [$closed];

        $productionAborted = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $productionAborted->abortProduction();
        yield [$productionAborted];

        $shipped = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $shipped->ship();
        yield [$shipped];

        $completed = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $completed->complete();
        yield [$completed];
    }

    /**
     * @covers \App\Domain\Market\Inquiry\Handler\ShipHandler
     * @dataProvider provideInquiriesWithInvalidStatus
     */
    public function testExpectInquiryNotInStatusException(Inquiry $inquiry): void
    {
        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->get($inquiry->id())->willReturn($inquiry);

        $this->expectException(InquiryNotInExpectedStatus::class);

        $command = new Ship($inquiry->id());
        $startProductionHandler = new ShipHandler(
            $inquiryRepository->reveal()
        );

        $startProductionHandler->__invoke($command);
    }

    public static function provideInquiriesWithValidStatus(): iterable
    {
        $inExecution = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $inExecution->lock();
        yield [$inExecution];

        $productionStarted = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $productionStarted->startProduction();
        yield [$productionStarted];

        $productionFinished = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $productionFinished->startProduction();
        yield [$productionFinished];
    }

    /**
     * @covers \App\Domain\Market\Inquiry\Handler\ShipHandler
     * @dataProvider provideInquiriesWithValidStatus
     */
    public function testShip(Inquiry $inquiry): void
    {
        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->get($inquiry->id())->willReturn($inquiry);
        $inquiryRepository->save($inquiry)->shouldBeCalled();

        $command = new Ship($inquiry->id());
        $startProductionHandler = new ShipHandler(
            $inquiryRepository->reveal()
        );

        $startProductionHandler->__invoke($command);
    }
}
