<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Market\Inquiry\Handler;

use App\Domain\Account\UserId;
use App\Domain\Market\Inquiry\Command\StartProduction;
use App\Domain\Market\Inquiry\Exception\InquiryNotInExpectedStatus;
use App\Domain\Market\Inquiry\Handler\StartProductionHandler;
use App\Domain\Market\Inquiry\Inquiry;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Piece\PieceId;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class StartProductionHandlerTest extends TestCase
{
    use ProphecyTrait;

    public static function provideInquiriesWithInvalidStatus(): iterable
    {
        $open = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        yield [$open];

        $closed = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $closed->close();
        yield [$closed];

        $productionStarted = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $productionStarted->startProduction();
        yield [$productionStarted];

        $productionAborted = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $productionAborted->abortProduction();
        yield [$productionAborted];

        $productionFinished = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $productionFinished->finishProduction();
        yield [$productionFinished];

        $shipped = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $shipped->ship();
        yield [$shipped];

        $completed = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $completed->complete();
        yield [$completed];
    }

    /**
     * @covers \App\Domain\Market\Inquiry\Handler\StartProductionHandler
     * @dataProvider provideInquiriesWithInvalidStatus
     */
    public function testExpectInquiryNotInStatusException(Inquiry $inquiry): void
    {
        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->get($inquiry->id())->willReturn($inquiry);

        $this->expectException(InquiryNotInExpectedStatus::class);

        $command = new StartProduction($inquiry->id());
        $startProductionHandler = new StartProductionHandler(
            $inquiryRepository->reveal()
        );

        $startProductionHandler->__invoke($command);
    }

    /**
     * @covers \App\Domain\Market\Inquiry\Handler\StartProductionHandler
     */
    public function testStartProduction(): void
    {
        $inquiry = Inquiry::create(UserId::create(), InquiryId::create(), PieceId::create(), 1, '', []);
        $inquiry->lock();

        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->get($inquiry->id())->willReturn($inquiry);
        $inquiryRepository->save($inquiry)->shouldBeCalled();

        $command = new StartProduction($inquiry->id());
        $startProductionHandler = new StartProductionHandler(
            $inquiryRepository->reveal()
        );

        $startProductionHandler->__invoke($command);
    }
}
