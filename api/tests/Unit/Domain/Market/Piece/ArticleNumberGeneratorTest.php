<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Market\Piece;

use App\Domain\Market\Piece\ArticleNumberGenerator;
use PHPUnit\Framework\TestCase;

class ArticleNumberGeneratorTest extends TestCase
{
    /**
     * @covers \App\Domain\Market\Piece\ArticleNumberGenerator::generate
     */
    public function testGenerate(): void
    {
        $articleNumber = ArticleNumberGenerator::generate();
        $isValid = ArticleNumberGenerator::isValid($articleNumber);
        self::assertTrue($isValid);
    }

    /**
     * @covers \App\Domain\Market\Piece\ArticleNumberGenerator::dummy
     * @covers \App\Domain\Market\Piece\ArticleNumberGenerator::generate
     */
    public function testDummyGenerate(): void
    {
        ArticleNumberGenerator::dummy();
        $articleNumber1 = ArticleNumberGenerator::generate();
        $articleNumber2 = ArticleNumberGenerator::generate();
        $articleNumber3 = ArticleNumberGenerator::generate();
        self::assertEquals('AAA-AAA', $articleNumber1);
        self::assertEquals('AAA-AAB', $articleNumber2);
        self::assertEquals('AAA-AAC', $articleNumber3);
    }
}
