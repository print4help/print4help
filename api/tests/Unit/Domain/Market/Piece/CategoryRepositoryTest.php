<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Market\Piece;

use App\Domain\Market\Piece\CategoryId;
use App\Domain\Market\Piece\CategoryRepository;
use PHPUnit\Framework\TestCase;

class CategoryRepositoryTest extends TestCase
{
    /**
     * @covers \App\Domain\Market\Piece\CategoryRepository::__construct
     * @covers \App\Domain\Market\Piece\CategoryRepository::getAll
     */
    public function testConfigurationCount(): void
    {
        $categoryRepository = new CategoryRepository();
        $ids = [];
        foreach ($categoryRepository->getAll() as $category) {
            $ids[] = [$category->id()->toString()];
            $ids[] = $category->allChildrenIdsAsString();
        }

        $merged = array_merge(...$ids);
        self::assertCount(count(CategoryId::keys()), $merged);
    }

    public function allCategoryIds(): iterable
    {
        foreach (CategoryId::values() as $categoryId) {
            yield [$categoryId];
        }
    }

    /**
     * @dataProvider allCategoryIds
     * @covers       \App\Domain\Market\Piece\CategoryRepository::getByCategoryId
     */
    public function testGetCategoryById(CategoryId $id): void
    {
        $categoryRepository = new CategoryRepository();
        $category = $categoryRepository->getByCategoryId($id);
        self::assertEquals($id, $category->id());
    }

    /**
     * @covers \App\Domain\Market\Piece\CategoryRepository::getCategoryIdsWithAllParentIds
     */
    public function testCategoryIdsWithAllParentIds(): void
    {
        $categoryRepository = new CategoryRepository();
        $categoryIds = [
            CategoryId::toysGames_ActionFigures(),
            CategoryId::toysGames_RgpFigures(),
            CategoryId::fashion_Rings(),
        ];

        $idsWithParents = $categoryRepository->getCategoryIdsWithAllParentIds($categoryIds);

        self::assertEquals([
            'fashion',
            'fashion.rings',
            'toys-games',
            'toys-games.action-figures',
            'toys-games.rpg-figures',
        ], $idsWithParents);
    }
}
