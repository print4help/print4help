<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Market\Offer\Handler;

use App\Domain\Account\UserId;
use App\Domain\Market\Offer\Chat;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\ChatRepository;
use App\Domain\Market\Offer\Command\UpdateMessage;
use App\Domain\Market\Offer\Exception\MessageCanNotBeUpdated;
use App\Domain\Market\Offer\Handler\UpdateMessageHandler;
use App\Domain\Market\Offer\Message;
use App\Domain\Market\Offer\MessageId;
use App\Domain\Market\Offer\MessageRepository;
use App\Domain\Market\Offer\OfferId;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Webmozart\Assert\InvalidArgumentException;

/**
 * @covers \App\Domain\Market\Offer\Handler\UpdateMessageHandler
 */
class UpdateMessageHandlerTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @var mixed|ChatRepository|ObjectProphecy
     */
    private mixed $chatRepository;
    /**
     * @var mixed|MessageRepository|ObjectProphecy
     */
    private mixed $messageRepository;

    private UpdateMessageHandler $updateMessageHandler;
    private Message $message;
    private Chat $chat;
    private UserId $userId;

    public function setUp(): void
    {
        $this->userId = UserId::create();

        $this->chat = Chat::create(
            ChatId::create(),
            OfferId::create(),
            [$this->userId, UserId::create()],
            'Subject'
        );

        $this->message = Message::create(
            MessageId::create(),
            $this->chat->id(),
            $this->userId,
            'Test',
            'Subject'
        );

        $this->chatRepository = $this->prophesize(ChatRepository::class);
        $this->chatRepository->get($this->chat->id())->willReturn($this->chat);

        $this->messageRepository = $this->prophesize(MessageRepository::class);
        $this->messageRepository->get($this->message->id())->willReturn($this->message);

        $this->updateMessageHandler = new UpdateMessageHandler(
            $this->chatRepository->reveal(),
            $this->messageRepository->reveal()
        );
    }

    public function testSuccessful(): void
    {
        $command = new UpdateMessage(
            $this->message->id(),
            $this->userId,
            'subject',
            'messageBody'
        );

        $this->messageRepository->save()->shouldBeCalledOnce()->withArguments([Argument::type(Message::class)]);

        $this->updateMessageHandler->__invoke($command);
    }

    public function testFailureOnInvalidAuthorId(): void
    {
        $command = new UpdateMessage(
            $this->message->id(),
            UserId::create(),
            'subject',
            'messageBody'
        );

        $this->expectException(InvalidArgumentException::class);

        $this->updateMessageHandler->__invoke($command);
    }

    public function testFailureOnClosedChat(): void
    {
        $this->chat->close();

        $command = new UpdateMessage(
            $this->message->id(),
            $this->userId,
            'subject',
            'messageBody'
        );

        $this->expectException(MessageCanNotBeUpdated::class);
        $this->updateMessageHandler->__invoke($command);
    }
}
