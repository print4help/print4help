<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Market\Offer\Handler;

use App\Domain\Account\UserId;
use App\Domain\Market\Inquiry\Inquiry;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Offer\Chat;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\ChatRepository;
use App\Domain\Market\Offer\Command\CreateChat;
use App\Domain\Market\Offer\Exception\ChatCanNotBeCreated;
use App\Domain\Market\Offer\Handler\CreateChatHandler;
use App\Domain\Market\Offer\Message;
use App\Domain\Market\Offer\MessageRepository;
use App\Domain\Market\Offer\Offer;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Offer\OfferRepository;
use App\Domain\Market\Piece\PieceId;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use ReflectionClass;

/**
 * @covers \App\Domain\Market\Offer\Handler\CreateChatHandler
 */
class CreateChatHandlerTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @var mixed|ChatRepository|ObjectProphecy
     */
    private mixed $chatRepository;
    /**
     * @var mixed|MessageRepository|ObjectProphecy
     */
    private mixed $messageRepository;

    private CreateChatHandler $createChatHandler;
    private Inquiry $inquiry;
    private Offer $offer;

    public function setUp(): void
    {
        $this->inquiry = Inquiry::create(
            UserId::create(),
            InquiryId::create(),
            PieceId::create(),
            1
        );

        $this->offer = Offer::place(
            UserId::create(),
            OfferId::create(),
            $this->inquiry->id(),
            $this->inquiry->pieceId(),
            'message',
            1
        );

        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->get($this->inquiry->id())->willReturn($this->inquiry);

        $offerRepository = $this->prophesize(OfferRepository::class);
        $offerRepository->get($this->offer->id())->willReturn($this->offer);

        $this->chatRepository = $this->prophesize(ChatRepository::class);
        $this->messageRepository = $this->prophesize(MessageRepository::class);

        $this->createChatHandler = new CreateChatHandler(
            $offerRepository->reveal(),
            $inquiryRepository->reveal(),
            $this->chatRepository->reveal(),
            $this->messageRepository->reveal()
        );
    }

    public function testSuccessful(): void
    {
        $command = new CreateChat(
            ChatId::create(),
            $this->offer->id(),
            'subject',
            'messageBody'
        );

        $this->chatRepository->save()->shouldBeCalledOnce()->withArguments([Argument::type(Chat::class)]);
        $this->messageRepository->save()->shouldBeCalledOnce()->withArguments([Argument::type(Message::class)]);

        $this->createChatHandler->__invoke($command);
    }

    public function provideInquiryStatusChanges(): iterable
    {
        yield ['lock'];
        yield ['close'];
        yield ['startProduction'];
        yield ['abortProduction'];
        yield ['finishProduction'];
        yield ['ship'];
        yield ['complete'];
    }

    /**
     * @dataProvider provideInquiryStatusChanges
     */
    public function testExpectExceptionInquiryNotOpen(string $eventMethodName): void
    {
        $command = new CreateChat(
            ChatId::create(),
            $this->offer->id(),
            'subject',
            'messageBody'
        );

        $refl = new ReflectionClass(Inquiry::class);
        $method = $refl->getMethod($eventMethodName);
        $method->invoke($this->inquiry);

        $this->expectException(ChatCanNotBeCreated::class);
        $this->createChatHandler->__invoke($command);
    }

    public function provideOfferStatusChanges(): iterable
    {
        yield ['accept'];
        yield ['reject'];
        yield ['withdraw'];
    }

    /**
     * @dataProvider provideOfferStatusChanges
     */
    public function testExpectExceptionOfferNotPlaced(string $eventMethodName): void
    {
        $command = new CreateChat(
            ChatId::create(),
            $this->offer->id(),
            'subject',
            'messageBody'
        );

        $refl = new ReflectionClass(Offer::class);
        $method = $refl->getMethod($eventMethodName);
        $method->invoke($this->offer);

        $this->expectException(ChatCanNotBeCreated::class);
        $this->createChatHandler->__invoke($command);
    }

    public function testExtractUserIds(): void
    {
        $command = new CreateChat(
            ChatId::create(),
            $this->offer->id(),
            'subject',
            'messageBody'
        );

        $refl = new ReflectionClass(CreateChatHandler::class);
        $method = $refl->getMethod('extractUserIds');
        $method->setAccessible(true);

        $userIds = $method->invoke($this->createChatHandler, $command);

        self::assertEquals(
            [$this->offer->userId(), $this->inquiry->userId()],
            $userIds
        );
    }
}
