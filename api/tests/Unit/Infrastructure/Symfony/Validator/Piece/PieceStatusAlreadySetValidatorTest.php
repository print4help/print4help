<?php

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Validator\Piece;

use App\Infrastructure\MongoDb\Repository\PieceRepository;
use App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySet;
use App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySetValidator;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class PieceStatusAlreadySetValidatorTest extends TestCase
{
    use ProphecyTrait;

    public function getInvalidStatus(): iterable
    {
        yield [''];
        yield ['invalid'];
    }

    /**
     * @covers       \App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySetValidator
     * @dataProvider getInvalidStatus
     */
    public function testTargetStatusBlank(string $status): void
    {
        $pieceRepository = $this->prophesize(PieceRepository::class);

        $constraint = new PieceStatusAlreadySet('Test', $status);
        $validator = new PieceStatusAlreadySetValidator($pieceRepository->reveal());

        $this->expectException(UnexpectedValueException::class);
        $validator->validate('', $constraint);
    }
}
