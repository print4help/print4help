<?php

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Validator\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Market\Inquiry\InquiryStatus;
use App\Domain\Market\Offer\OfferStatus;
use App\Infrastructure\MongoDb\Repository\InquiryRepository;
use App\Infrastructure\ReadModel\Exception\InquiryByInquiryIdNotFoundException;
use App\Infrastructure\ReadModel\Inquiry;
use App\Infrastructure\ReadModel\Offer;
use App\Infrastructure\Symfony\Security\User;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryHasAcceptedOfferFromCurrentUser;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryHasAcceptedOfferFromCurrentUserValidator;
use Prophecy\PhpUnit\ProphecyTrait;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @covers \App\Infrastructure\Symfony\Validator\Inquiry\InquiryHasAcceptedOfferFromCurrentUserValidator
 */
class InquiryHasAcceptedOfferFromCurrentUserValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    /**
     * @var InquiryHasAcceptedOfferFromCurrentUserValidator
     */
    protected $validator;
    private Inquiry $inquiry;
    private User $user;

    protected function createTokenStorage(User $user = null): TokenStorageInterface
    {
        if ($user === null) {
            $this->user = new User(
                Uuid::fromString('ff8392e0-d778-4bd7-9a60-4ff99e45676c'),
                'maker@test.com',
                ['ROLE_USER']
            );
        } else {
            $this->user = $user;
        }

        $tokenInterface = $this->prophesize(TokenInterface::class);
        $tokenInterface->getUser()->willReturn($this->user);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($tokenInterface->reveal());

        return $tokenStorage->reveal();
    }

    protected function createValidator()
    {
        $this->inquiry = new Inquiry();
        $this->inquiry->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $this->inquiry->status = InquiryStatus::open()->toString();

        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->getWithOffers($this->inquiry->id)->willReturn($this->inquiry);

        $validator = new InquiryHasAcceptedOfferFromCurrentUserValidator($inquiryRepository->reveal());
        $validator->setTokenStorage($this->createTokenStorage());

        return $validator;
    }

    public function testNoViolation(): void
    {
        $offer = new Offer();
        $offer->userId = $this->user->getUserIdentifier();
        $offer->id = '5d407f06-beaa-4d05-a966-63daab612581';
        $offer->status = OfferStatus::accepted()->toString();

        $this->inquiry->offers = [
            $offer,
        ];

        $constraint = new InquiryHasAcceptedOfferFromCurrentUser('Test');

        $this->validator->validate($this->inquiry->id, $constraint);
        $this->assertNoViolation();
    }

    public function testBuildViolation(): void
    {
        $offer1 = new Offer();
        $offer1->id = '5d407f06-beaa-4d05-a966-63daab612581';
        $offer1->status = OfferStatus::placed()->toString();
        $offer1->userId = $this->user->getUserIdentifier();

        $offer2 = new Offer();
        $offer2->id = 'c1faaf18-2820-43b5-aca3-3ced928826dc';
        $offer2->status = OfferStatus::accepted()->toString();
        $offer2->userId = UserId::create()->toString();

        $this->inquiry->offers = [$offer1, $offer2];

        $constraint = new InquiryHasAcceptedOfferFromCurrentUser('Test');

        $this->validator->validate($this->inquiry->id, $constraint);

        $this->buildViolation($constraint->message)
            ->setTranslationDomain($constraint->translationDomain)
            ->assertRaised();
    }

    public function testNoAssertionWhenInquiryDoesNotExist(): void
    {
        $inquiry = new Inquiry();
        $inquiry->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $inquiry->status = 'open';

        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->getWithOffers($inquiry->id)->willThrow(InquiryByInquiryIdNotFoundException::class);

        $this->validator = new InquiryHasAcceptedOfferFromCurrentUserValidator($inquiryRepository->reveal());
        $this->validator->setTokenStorage($this->createTokenStorage());
        $this->validator->initialize($this->context);

        $constraint = new InquiryHasAcceptedOfferFromCurrentUser('Test');

        $this->validator->validate($inquiry->id, $constraint);
        $this->assertNoViolation();
    }

    public function testNoAssertionWhenInquiryDoesNotHaveAnyAcceptedOffers(): void
    {
        $offer1 = new Offer();
        $offer1->id = '5d407f06-beaa-4d05-a966-63daab612581';
        $offer1->status = OfferStatus::placed()->toString();
        $offer1->userId = $this->user->getUserIdentifier();

        $offer2 = new Offer();
        $offer2->id = 'c1faaf18-2820-43b5-aca3-3ced928826dc';
        $offer2->status = OfferStatus::placed()->toString();
        $offer2->userId = UserId::create()->toString();

        $this->inquiry->offers = [$offer1, $offer2];

        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->getWithOffers($this->inquiry->id)->willThrow(InquiryByInquiryIdNotFoundException::class);

        $constraint = new InquiryHasAcceptedOfferFromCurrentUser('Test');

        $this->validator->validate($this->inquiry->id, $constraint);
        $this->assertNoViolation();
    }

    public function testNoAssertionWhenUserIsAdmin(): void
    {
        $offer1 = new Offer();
        $offer1->id = '5d407f06-beaa-4d05-a966-63daab612581';
        $offer1->status = OfferStatus::placed()->toString();
        $offer1->userId = $this->user->getUserIdentifier();

        $offer2 = new Offer();
        $offer2->id = 'c1faaf18-2820-43b5-aca3-3ced928826dc';
        $offer2->status = OfferStatus::accepted()->toString();
        $offer2->userId = UserId::create()->toString();

        $this->inquiry->offers = [$offer1, $offer2];

        $user = new User(
            Uuid::fromString('c2b2e1ba-8132-4b3e-a424-9e992f36612f'),
            'admin@test.com',
            ['ROLE_ADMIN']
        );

        $this->validator->setTokenStorage($this->createTokenStorage($user));

        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->getWithOffers($this->inquiry->id)->willThrow(InquiryByInquiryIdNotFoundException::class);

        $constraint = new InquiryHasAcceptedOfferFromCurrentUser('Test');

        $this->validator->validate($this->inquiry->id, $constraint);
        $this->assertNoViolation();
    }

    public function provideInvalidValues(): iterable
    {
        yield [null];
        yield [123];
        yield [true];
        yield [''];
        yield [[]];
    }

    /**
     * @dataProvider provideInvalidValues
     */
    public function testNoAssertionForInvalidValue(mixed $value): void
    {
        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $this->validator = new InquiryHasAcceptedOfferFromCurrentUserValidator($inquiryRepository->reveal());
        $constraint = new InquiryHasAcceptedOfferFromCurrentUser('Test');

        $this->validator->validate($value, $constraint);
        $this->assertNoViolation();
    }

    public function testUnexpectedTypeException(): void
    {
        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $this->validator = new InquiryHasAcceptedOfferFromCurrentUserValidator($inquiryRepository->reveal());
        $constraint = new InquiryExists('Test');

        $this->expectException(UnexpectedTypeException::class);
        $this->validator->validate('open', $constraint);
    }
}
