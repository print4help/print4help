<?php

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Validator\Inquiry;

use App\Domain\Market\Inquiry\InquiryStatus;
use App\Infrastructure\MongoDb\Repository\InquiryRepository;
use App\Infrastructure\ReadModel\Exception\InquiryByInquiryIdNotFoundException;
use App\Infrastructure\ReadModel\Inquiry;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatusValidator;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @covers \App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatusValidator
 */
class InquiryIsStatusValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    private Inquiry $inquiry;

    protected function createValidator()
    {
        $this->inquiry = new Inquiry();
        $this->inquiry->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $this->inquiry->status = InquiryStatus::open()->toString();

        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->get($this->inquiry->id)->willReturn($this->inquiry);

        return new InquiryIsStatusValidator($inquiryRepository->reveal());
    }

    public function provideStatus(): iterable
    {
        yield [
            [InquiryStatus::inExecution()->toString()],
            'in_execution',
        ];
        yield [
            [InquiryStatus::inExecution()->toString(), InquiryStatus::productionStarted()->toString(),],
            'in_execution|production_started',
        ];
    }

    /**
     * @dataProvider provideStatus
     */
    public function testNoViolation(array $status): void
    {
        $this->inquiry->status = $status[0];

        $constraint = new InquiryIsStatus('Test', $status);

        $this->validator->validate($this->inquiry->id, $constraint);
        $this->assertNoViolation();
    }

    /**
     * @dataProvider provideStatus
     */
    public function testBuildViolation(array $status, string $parameter): void
    {
        $constraint = new InquiryIsStatus('Test', $status);

        $this->validator->validate($this->inquiry->id, $constraint);

        $this->buildViolation($constraint->message)
            ->setInvalidValue($this->inquiry->status)
            ->setParameter('{{ expected_status }}', $parameter)
            ->setTranslationDomain($constraint->translationDomain)
            ->assertRaised();
    }

    public function getInvalidStatus(): iterable
    {
        yield [['']];
        yield [['invalid']];
        yield [['invalid', '']];
        yield [['invalid', 'open']];
        yield [['open', 'invalid']];
    }

    /**
     * @dataProvider getInvalidStatus
     * @param array<int, string> $status
     */
    public function testStatusBlankInConstraint(array $status): void
    {
        $inquiryRepository = $this->prophesize(InquiryRepository::class);

        $constraint = new InquiryIsStatus('Test', $status);
        $validator = new InquiryIsStatusValidator($inquiryRepository->reveal());

        $this->expectException(UnexpectedValueException::class);
        $validator->validate('', $constraint);
    }

    public function provideInvalidValues(): iterable
    {
        yield [null];
        yield [123];
        yield [true];
        yield [''];
        yield [[]];
    }

    /**
     * @dataProvider provideInvalidValues
     */
    public function testNoAssertionForInvalidValue(mixed $value): void
    {
        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $this->validator = new InquiryIsStatusValidator($inquiryRepository->reveal());
        $constraint = new InquiryIsStatus('Test', ['in_execution']);

        $this->validator->validate($value, $constraint);
        $this->assertNoViolation();
    }

    public function testUnexpectedTypeException(): void
    {
        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $this->validator = new InquiryIsStatusValidator($inquiryRepository->reveal());
        $constraint = new InquiryExists('Test');

        $this->expectException(UnexpectedTypeException::class);
        $this->validator->validate('open', $constraint);
    }

    public function testNoAssertionWhenInquiryDoesNotExist(): void
    {
        $inquiry = new Inquiry();
        $inquiry->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $inquiry->status = InquiryStatus::open()->toString();

        $inquiryRepository = $this->prophesize(InquiryRepository::class);
        $inquiryRepository->get($inquiry->id)->willThrow(InquiryByInquiryIdNotFoundException::class);

        $this->validator = new InquiryIsStatusValidator($inquiryRepository->reveal());
        $this->validator->initialize($this->context);

        $constraint = new InquiryIsStatus('Test', ['in_execution']);

        $this->validator->validate($inquiry->id, $constraint);
        $this->assertNoViolation();
    }
}
