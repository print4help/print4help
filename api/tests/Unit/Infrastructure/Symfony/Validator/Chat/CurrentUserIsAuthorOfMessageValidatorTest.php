<?php

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Validator\Chat;

use App\Infrastructure\ReadModel\Chat;
use App\Infrastructure\ReadModel\Exception\MessageByMessageIdNotFoundException;
use App\Infrastructure\ReadModel\Message;
use App\Infrastructure\ReadModel\Repository\MessageRepository;
use App\Infrastructure\Symfony\Security\User;
use App\Infrastructure\Symfony\Validator\Chat\CurrentUserIsAuthorOfMessage;
use App\Infrastructure\Symfony\Validator\Chat\CurrentUserIsAuthorOfMessageValidator;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use Prophecy\PhpUnit\ProphecyTrait;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @covers \App\Infrastructure\Symfony\Validator\Chat\CurrentUserIsAuthorOfMessageValidator
 */
class CurrentUserIsAuthorOfMessageValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    /**
     * @var CurrentUserIsAuthorOfMessageValidator
     */
    protected $validator;
    private Chat $chat;
    private Message $message;
    private User $user;

    protected function createTokenStorage(User $user = null): TokenStorageInterface
    {
        if ($user === null) {
            $this->user = new User(
                Uuid::fromString('ff8392e0-d778-4bd7-9a60-4ff99e45676c'),
                'maker@test.com',
                ['ROLE_USER']
            );
        } else {
            $this->user = $user;
        }

        $tokenInterface = $this->prophesize(TokenInterface::class);
        $tokenInterface->getUser()->willReturn($this->user);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($tokenInterface->reveal());

        return $tokenStorage->reveal();
    }

    protected function createValidator()
    {
        $this->message = new Message();
        $this->message->id = '8b2b3e0f-1511-43c4-979a-a90fcae1eb57';

        $messageRepository = $this->prophesize(MessageRepository::class);
        $messageRepository->get($this->message->id)->willReturn($this->message);

        $validator = new CurrentUserIsAuthorOfMessageValidator($messageRepository->reveal());
        $validator->setTokenStorage($this->createTokenStorage());

        return $validator;
    }

    public function testNoViolation(): void
    {
        $this->message->authorId = $this->user->getUserIdentifier();

        $constraint = new CurrentUserIsAuthorOfMessage('Test');

        $this->validator->validate($this->message->id, $constraint);
        $this->assertNoViolation();
    }

    public function testBuildViolation(): void
    {
        $this->message->authorId = '5d407f06-beaa-4d05-a966-63daab612581';

        $constraint = new CurrentUserIsAuthorOfMessage('Test');

        $this->validator->validate($this->message->id, $constraint);

        $this->buildViolation($constraint->message)
            ->setTranslationDomain($constraint->translationDomain)
            ->setInvalidValue($this->message->id)
            ->setParameter('{{ messageId }}', $this->message->id)
            ->assertRaised();
    }

    public function testThrowAccessDeniedException(): void
    {
        $this->message->authorId = '5d407f06-beaa-4d05-a966-63daab612581';
        $constraint = new CurrentUserIsAuthorOfMessage('Test', true);

        $this->expectException(AccessDeniedException::class);
        $this->validator->validate($this->message->id, $constraint);
        $this->assertNoViolation();
    }

    public function testNoAssertionWhenMessageDoesNotExist(): void
    {
        $message = new Message();
        $message->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';

        $chatRepository = $this->prophesize(MessageRepository::class);
        $chatRepository->get($message->id)->willThrow(MessageByMessageIdNotFoundException::class);

        $this->validator = new CurrentUserIsAuthorOfMessageValidator($chatRepository->reveal());
        $this->validator->setTokenStorage($this->createTokenStorage());
        $this->validator->initialize($this->context);

        $constraint = new CurrentUserIsAuthorOfMessage('Test');

        $this->validator->validate($message->id, $constraint);
        $this->assertNoViolation();
    }

    public function testNoAssertionWhenUserIsAdmin(): void
    {
        $this->message->authorId = 'c1faaf18-2820-43b5-aca3-3ced928826dc';

        $user = new User(
            Uuid::fromString('c2b2e1ba-8132-4b3e-a424-9e992f36612f'),
            'admin@test.com',
            ['ROLE_ADMIN']
        );

        $this->validator->setTokenStorage($this->createTokenStorage($user));

        $constraint = new CurrentUserIsAuthorOfMessage('Test');

        $this->validator->validate($this->message->id, $constraint);
        $this->assertNoViolation();
    }

    public function provideInvalidValues(): iterable
    {
        yield [null];
        yield [123];
        yield [true];
        yield [''];
        yield [[]];
    }

    /**
     * @dataProvider provideInvalidValues
     */
    public function testNoAssertionForInvalidValue(mixed $value): void
    {
        $chatRepository = $this->prophesize(MessageRepository::class);
        $this->validator = new CurrentUserIsAuthorOfMessageValidator($chatRepository->reveal());
        $constraint = new CurrentUserIsAuthorOfMessage('Test');

        $this->validator->validate($value, $constraint);
        $this->assertNoViolation();
    }

    public function testUnexpectedTypeException(): void
    {
        $chatRepository = $this->prophesize(MessageRepository::class);
        $this->validator = new CurrentUserIsAuthorOfMessageValidator($chatRepository->reveal());
        $constraint = new InquiryExists('Test');

        $this->expectException(UnexpectedTypeException::class);
        $this->validator->validate('test', $constraint);
    }
}
