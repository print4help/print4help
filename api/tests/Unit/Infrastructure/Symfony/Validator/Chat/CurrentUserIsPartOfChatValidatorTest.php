<?php

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Validator\Chat;

use App\Domain\Market\Offer\ChatStatus;
use App\Infrastructure\MongoDb\Repository\ChatRepository;
use App\Infrastructure\ReadModel\Chat;
use App\Infrastructure\ReadModel\Exception\ChatByChatIdNotFoundException;
use App\Infrastructure\Symfony\Security\User;
use App\Infrastructure\Symfony\Validator\Chat\CurrentUserIsPartOfChat;
use App\Infrastructure\Symfony\Validator\Chat\CurrentUserIsPartOfChatValidator;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use Prophecy\PhpUnit\ProphecyTrait;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @covers \App\Infrastructure\Symfony\Validator\Chat\CurrentUserIsPartOfChatValidator
 */
class CurrentUserIsPartOfChatValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    /**
     * @var CurrentUserIsPartOfChatValidator
     */
    protected $validator;
    private Chat $chat;
    private User $user;

    protected function createTokenStorage(User $user = null): TokenStorageInterface
    {
        if ($user === null) {
            $this->user = new User(
                Uuid::fromString('ff8392e0-d778-4bd7-9a60-4ff99e45676c'),
                'maker@test.com',
                ['ROLE_USER']
            );
        } else {
            $this->user = $user;
        }

        $tokenInterface = $this->prophesize(TokenInterface::class);
        $tokenInterface->getUser()->willReturn($this->user);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($tokenInterface->reveal());

        return $tokenStorage->reveal();
    }

    protected function createValidator()
    {
        $this->chat = new Chat();
        $this->chat->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $this->chat->status = ChatStatus::open()->toString();

        $chatRepository = $this->prophesize(ChatRepository::class);
        $chatRepository->get($this->chat->id)->willReturn($this->chat);

        $validator = new CurrentUserIsPartOfChatValidator($chatRepository->reveal());
        $validator->setTokenStorage($this->createTokenStorage());

        return $validator;
    }

    public function testNoViolation(): void
    {
        $this->chat->userIds = [
            $this->user->getUserIdentifier(),
            '5d407f06-beaa-4d05-a966-63daab612581',
        ];

        $constraint = new CurrentUserIsPartOfChat('Test');

        $this->validator->validate($this->chat->id, $constraint);
        $this->assertNoViolation();
    }

    public function testBuildViolation(): void
    {
        $this->chat->userIds = [
            '5d407f06-beaa-4d05-a966-63daab612581',
            '5d407f06-beaa-4d05-a966-63daab612582',
        ];

        $constraint = new CurrentUserIsPartOfChat('Test');

        $this->validator->validate($this->chat->id, $constraint);

        $this->buildViolation($constraint->message)
            ->setTranslationDomain($constraint->translationDomain)
            ->setInvalidValue($this->chat->id)
            ->setParameter('{{ chatId }}', $this->chat->id)
            ->assertRaised();
    }

    public function testThrowAccessDeniedException(): void
    {
        $this->chat->userIds = [
            '5d407f06-beaa-4d05-a966-63daab612581',
            '5d407f06-beaa-4d05-a966-63daab612582',
        ];

        $constraint = new CurrentUserIsPartOfChat('Test', true);
        $this->expectException(AccessDeniedException::class);

        $this->validator->validate($this->chat->id, $constraint);
        $this->assertNoViolation();
    }

    public function testNoAssertionWhenChatDoesNotExist(): void
    {
        $chat = new Chat();
        $chat->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $chat->status = 'open';

        $chatRepository = $this->prophesize(ChatRepository::class);
        $chatRepository->get($chat->id)->willThrow(ChatByChatIdNotFoundException::class);

        $this->validator = new CurrentUserIsPartOfChatValidator($chatRepository->reveal());
        $this->validator->setTokenStorage($this->createTokenStorage());
        $this->validator->initialize($this->context);

        $constraint = new CurrentUserIsPartOfChat('Test');

        $this->validator->validate($chat->id, $constraint);
        $this->assertNoViolation();
    }

    public function testNoAssertionWhenUserIsAdmin(): void
    {
        $this->chat->userIds = [
            'c1faaf18-2820-43b5-aca3-3ced928826dc',
            '5d407f06-beaa-4d05-a966-63daab612581',
        ];

        $user = new User(
            Uuid::fromString('c2b2e1ba-8132-4b3e-a424-9e992f36612f'),
            'admin@test.com',
            ['ROLE_ADMIN']
        );

        $this->validator->setTokenStorage($this->createTokenStorage($user));

        $constraint = new CurrentUserIsPartOfChat('Test');

        $this->validator->validate($this->chat->id, $constraint);
        $this->assertNoViolation();
    }

    public function provideInvalidValues(): iterable
    {
        yield [null];
        yield [123];
        yield [true];
        yield [''];
        yield [[]];
    }

    /**
     * @dataProvider provideInvalidValues
     */
    public function testNoAssertionForInvalidValue(mixed $value): void
    {
        $chatRepository = $this->prophesize(ChatRepository::class);
        $this->validator = new CurrentUserIsPartOfChatValidator($chatRepository->reveal());
        $constraint = new CurrentUserIsPartOfChat('Test');

        $this->validator->validate($value, $constraint);
        $this->assertNoViolation();
    }

    public function testUnexpectedTypeException(): void
    {
        $chatRepository = $this->prophesize(ChatRepository::class);
        $this->validator = new CurrentUserIsPartOfChatValidator($chatRepository->reveal());
        $constraint = new InquiryExists('Test');

        $this->expectException(UnexpectedTypeException::class);
        $this->validator->validate('test', $constraint);
    }
}
