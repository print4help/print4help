<?php

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Validator\Chat;

use App\Infrastructure\ReadModel\Exception\MessageByMessageIdNotFoundException;
use App\Infrastructure\ReadModel\Message;
use App\Infrastructure\ReadModel\Repository\MessageRepository;
use App\Infrastructure\Symfony\Validator\Chat\ChatExists;
use App\Infrastructure\Symfony\Validator\Chat\MessageExists;
use App\Infrastructure\Symfony\Validator\Chat\MessageExistsValidator;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @covers \App\Infrastructure\Symfony\Validator\Chat\MessageExistsValidator
 */
class MessageExistsValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    private Message $message;

    protected function createValidator()
    {
        $this->message = new Message();
        $this->message->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $this->message->chatId = '48565503-4037-498e-94a0-d6233068091f';
        $this->message->authorId = 'e58275b8-d1f2-4863-8931-d9df9f011b09';
        $this->message->subject = 'Test Message Subject';
        $this->message->body = 'Test Message Body';

        $messageRepository = $this->prophesize(MessageRepository::class);
        $messageRepository->get($this->message->id)->willReturn($this->message);

        return new MessageExistsValidator($messageRepository->reveal());
    }

    public function testNoViolation(): void
    {
        $constraint = new MessageExists('Test');

        $this->validator->validate($this->message->id, $constraint);
        $this->assertNoViolation();
    }

    public function testBuildViolation(): void
    {
        $constraint = new MessageExists('Test');

        $notFoundMessageId = '6b687fb5-7c90-488f-a0ed-e8ce9d64af96';

        $chatRepository = $this->prophesize(MessageRepository::class);
        $chatRepository->get($notFoundMessageId)->willThrow(MessageByMessageIdNotFoundException::class);

        $this->validator = new MessageExistsValidator($chatRepository->reveal());
        $this->validator->initialize($this->context);
        $this->validator->validate($notFoundMessageId, $constraint);

        $this->buildViolation($constraint->message)
            ->setInvalidValue($notFoundMessageId)
            ->setParameter('{{ messageId }}', $notFoundMessageId)
            ->setTranslationDomain($constraint->translationDomain)
            ->assertRaised();
    }

    public function testThrowNotFoundException(): void
    {
        $constraint = new MessageExists('Test', true);

        $notFoundMessageId = '6b687fb5-7c90-488f-a0ed-e8ce9d64af96';

        $chatRepository = $this->prophesize(MessageRepository::class);
        $chatRepository->get($notFoundMessageId)->willThrow(MessageByMessageIdNotFoundException::class);

        $this->expectException(NotFoundHttpException::class);

        $this->validator = new MessageExistsValidator($chatRepository->reveal());
        $this->validator->initialize($this->context);
        $this->validator->validate($notFoundMessageId, $constraint);

        $this->assertNoViolation();
    }

    public function provideInvalidValues(): iterable
    {
        yield [null];
        yield [123];
        yield [true];
        yield [''];
        yield [[]];
    }

    /**
     * @dataProvider provideInvalidValues
     */
    public function testNoAssertionForInvalidValue(mixed $value): void
    {
        $chatRepository = $this->prophesize(MessageRepository::class);
        $this->validator = new MessageExistsValidator($chatRepository->reveal());
        $constraint = new MessageExists('Test');

        $this->validator->validate($value, $constraint);
        $this->assertNoViolation();
    }

    public function testUnexpectedTypeException(): void
    {
        $chatRepository = $this->prophesize(MessageRepository::class);
        $this->validator = new MessageExistsValidator($chatRepository->reveal());
        $constraint = new ChatExists('Test');

        $this->expectException(UnexpectedTypeException::class);
        $this->validator->validate('test', $constraint);
    }
}
