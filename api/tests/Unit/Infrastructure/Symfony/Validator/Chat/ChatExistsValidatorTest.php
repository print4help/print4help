<?php

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Validator\Chat;

use App\Infrastructure\ReadModel\Chat;
use App\Infrastructure\ReadModel\Exception\ChatByChatIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\ChatRepository;
use App\Infrastructure\Symfony\Validator\Chat\ChatExists;
use App\Infrastructure\Symfony\Validator\Chat\ChatExistsValidator;
use App\Infrastructure\Symfony\Validator\Chat\MessageExists;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @covers \App\Infrastructure\Symfony\Validator\Chat\ChatExistsValidator
 */
class ChatExistsValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    private Chat $chat;

    protected function createValidator()
    {
        $this->chat = new Chat();
        $this->chat->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $this->chat->offerId = 'e58275b8-d1f2-4863-8931-d9df9f011b09';
        $this->chat->status = 'open';

        $chatRepository = $this->prophesize(ChatRepository::class);
        $chatRepository->get($this->chat->id)->willReturn($this->chat);

        return new ChatExistsValidator($chatRepository->reveal());
    }

    public function testNoViolation(): void
    {
        $constraint = new ChatExists('Test');

        $this->validator->validate($this->chat->id, $constraint);
        $this->assertNoViolation();
    }

    public function testBuildViolation(): void
    {
        $constraint = new ChatExists('Test');

        $notFoundChatId = '6b687fb5-7c90-488f-a0ed-e8ce9d64af96';

        $chatRepository = $this->prophesize(ChatRepository::class);
        $chatRepository->get($notFoundChatId)->willThrow(ChatByChatIdNotFoundException::class);

        $this->validator = new ChatExistsValidator($chatRepository->reveal());
        $this->validator->initialize($this->context);
        $this->validator->validate($notFoundChatId, $constraint);

        $this->buildViolation($constraint->message)
            ->setInvalidValue($notFoundChatId)
            ->setParameter('{{ chatId }}', $notFoundChatId)
            ->setTranslationDomain($constraint->translationDomain)
            ->assertRaised();
    }

    public function testThrowNotFoundException(): void
    {
        $constraint = new ChatExists('Test', true);

        $notFoundMessageId = '6b687fb5-7c90-488f-a0ed-e8ce9d64af96';

        $chatRepository = $this->prophesize(ChatRepository::class);
        $chatRepository->get($notFoundMessageId)->willThrow(ChatByChatIdNotFoundException::class);

        $this->expectException(NotFoundHttpException::class);

        $this->validator = new ChatExistsValidator($chatRepository->reveal());
        $this->validator->initialize($this->context);
        $this->validator->validate($notFoundMessageId, $constraint);

        $this->assertNoViolation();
    }

    public function provideInvalidValues(): iterable
    {
        yield [null];
        yield [123];
        yield [true];
        yield [''];
        yield [[]];
    }

    /**
     * @dataProvider provideInvalidValues
     */
    public function testNoAssertionForInvalidValue(mixed $value): void
    {
        $chatRepository = $this->prophesize(ChatRepository::class);
        $this->validator = new ChatExistsValidator($chatRepository->reveal());
        $constraint = new ChatExists('Test');

        $this->validator->validate($value, $constraint);
        $this->assertNoViolation();
    }

    public function testUnexpectedTypeException(): void
    {
        $chatRepository = $this->prophesize(ChatRepository::class);
        $this->validator = new ChatExistsValidator($chatRepository->reveal());
        $constraint = new MessageExists('Test');

        $this->expectException(UnexpectedTypeException::class);
        $this->validator->validate('test', $constraint);
    }
}
