<?php

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Validator\Chat;

use App\Domain\Market\Offer\ChatStatus;
use App\Infrastructure\MongoDb\Repository\ChatRepository;
use App\Infrastructure\ReadModel\Chat;
use App\Infrastructure\ReadModel\Exception\ChatByChatIdNotFoundException;
use App\Infrastructure\Symfony\Validator\Chat\ChatExists;
use App\Infrastructure\Symfony\Validator\Chat\ChatIsStatus;
use App\Infrastructure\Symfony\Validator\Chat\ChatIsStatusValidator;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

/**
 * @covers \App\Infrastructure\Symfony\Validator\Chat\ChatIsStatusValidator
 */
class ChatIsStatusValidatorTest extends ConstraintValidatorTestCase
{
    use ProphecyTrait;

    private Chat $chat;

    protected function createValidator()
    {
        $this->chat = new Chat();
        $this->chat->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $this->chat->status = ChatStatus::open()->toString();

        $chatRepository = $this->prophesize(ChatRepository::class);
        $chatRepository->get($this->chat->id)->willReturn($this->chat);

        return new ChatIsStatusValidator($chatRepository->reveal());
    }

    public function provideStatus(): iterable
    {
        yield [
            [ChatStatus::open()->toString()],
            'open',
        ];
        yield [
            [ChatStatus::open()->toString(), ChatStatus::closed()->toString(),],
            'open|closed',
        ];
    }

    /**
     * @dataProvider provideStatus
     */
    public function testNoViolation(array $status): void
    {
        $this->chat->status = $status[0];

        $constraint = new ChatIsStatus('Test', $status);

        $this->validator->validate($this->chat->id, $constraint);
        $this->assertNoViolation();
    }

    /**
     * @dataProvider provideStatus
     */
    public function testBuildViolation(array $status, string $parameter): void
    {
        $constraint = new ChatIsStatus('Test', $status);

        $this->chat->status = 'invalid'; // setting invalid status here just to check for all provided datas
        $this->validator->validate($this->chat->id, $constraint);

        $this->buildViolation($constraint->message)
            ->setInvalidValue($this->chat->status)
            ->setParameter('{{ expected_status }}', $parameter)
            ->setTranslationDomain($constraint->translationDomain)
            ->assertRaised();
    }

    public function getInvalidStatus(): iterable
    {
        yield [['']];
        yield [['invalid']];
        yield [['invalid', '']];
        yield [['invalid', 'open']];
        yield [['open', 'invalid']];
    }

    /**
     * @dataProvider getInvalidStatus
     * @param array<int, string> $status
     */
    public function testStatusBlankInConstraint(array $status): void
    {
        $chatRepository = $this->prophesize(ChatRepository::class);

        $constraint = new ChatIsStatus('Test', $status);
        $validator = new ChatIsStatusValidator($chatRepository->reveal());

        $this->expectException(UnexpectedValueException::class);
        $validator->validate('', $constraint);
    }

    public function provideInvalidValues(): iterable
    {
        yield [null];
        yield [123];
        yield [true];
        yield [''];
        yield [[]];
    }

    /**
     * @dataProvider provideInvalidValues
     */
    public function testNoAssertionForInvalidValue(mixed $value): void
    {
        $chatRepository = $this->prophesize(ChatRepository::class);
        $this->validator = new ChatIsStatusValidator($chatRepository->reveal());
        $constraint = new ChatIsStatus('Test', ['open']);

        $this->validator->validate($value, $constraint);
        $this->assertNoViolation();
    }

    public function testUnexpectedTypeException(): void
    {
        $chatRepository = $this->prophesize(ChatRepository::class);
        $this->validator = new ChatIsStatusValidator($chatRepository->reveal());
        $constraint = new ChatExists('Test');

        $this->expectException(UnexpectedTypeException::class);
        $this->validator->validate('open', $constraint);
    }

    public function testNoAssertionWhenInquiryDoesNotExist(): void
    {
        $chat = new Chat();
        $chat->id = '75fdbf2d-d58a-4795-951a-3662eea34abf';
        $chat->status = ChatStatus::open()->toString();

        $chatRepository = $this->prophesize(ChatRepository::class);
        $chatRepository->get($chat->id)->willThrow(ChatByChatIdNotFoundException::class);

        $this->validator = new ChatIsStatusValidator($chatRepository->reveal());
        $this->validator->initialize($this->context);

        $constraint = new ChatIsStatus('Test', ['open']);

        $this->validator->validate($chat->id, $constraint);
        $this->assertNoViolation();
    }
}
