<?php

declare(strict_types=1);

namespace App\Domain;

use Throwable;

final class UuidNotValidException extends DomainException
{
    public function __construct(string $uuid, string $className, Throwable $previous = null)
    {
        parent::__construct(sprintf('%s [%s] is not a valid Uuid', $className, $uuid), 0, $previous);
    }
}
