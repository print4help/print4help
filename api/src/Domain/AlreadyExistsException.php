<?php

declare(strict_types=1);

namespace App\Domain;

abstract class AlreadyExistsException extends DomainException
{
}
