<?php

declare(strict_types=1);

namespace App\Domain;

use \RuntimeException;
use DateTimeImmutable;

final class DateTimeHelper
{
    private static ?DateTimeImmutable $overrodeDateTime = null;

    public static function override(DateTimeImmutable $dateTimeImmutable): void
    {
        self::$overrodeDateTime = $dateTimeImmutable;
    }

    public static function sleep(int $seconds): void
    {
        if (self::$overrodeDateTime instanceof DateTimeImmutable) {
            self::$overrodeDateTime = self::$overrodeDateTime->modify(sprintf('+%s seconds', $seconds));

            return;
        }

        sleep($seconds);
    }

    public static function create(): DateTimeImmutable
    {
        if (self::$overrodeDateTime instanceof DateTimeImmutable) {
            return self::$overrodeDateTime;
        }

        return new DateTimeImmutable('now');
    }

    public static function reset(): void
    {
        self::$overrodeDateTime = null;
    }

    public static function createFromString(string $dateTime): DateTimeImmutable
    {
        $date = DateTimeImmutable::createFromFormat(DateTimeImmutable::ATOM, $dateTime);

        if (!$date instanceof DateTimeImmutable) {
            throw new RuntimeException(sprintf("Can't parse Date in format ATOM from [%s]", $dateTime));
        }

        return $date;
    }
}
