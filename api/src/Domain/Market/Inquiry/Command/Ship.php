<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Command;

use App\Domain\Market\Inquiry\InquiryId;

class Ship
{
    private string $inquiryId;

    public function __construct(InquiryId $inquiryId, )
    {
        $this->inquiryId = $inquiryId->toString();
    }

    public function inquiryId(): InquiryId
    {
        return InquiryId::fromString($this->inquiryId);
    }
}
