<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Command;

use App\Domain\Market\Inquiry\InquiryId;

class UpdateInquiryMetadata
{
    private string $inquiryId;
    private int $quantity;
    private ?string $purpose;
    /**
     * @var array<int, string>
     */
    private array $requirements;

    /**
     * @param array<int, string> $requirements
     */
    public function __construct(
        InquiryId $inquiryId,
        int $quantity,
        ?string $purpose = null,
        array $requirements = []
    ) {
        $this->inquiryId = $inquiryId->toString();
        $this->quantity = $quantity;
        $this->purpose = $purpose;
        $this->requirements = $requirements;
    }

    public function inquiryId(): InquiryId
    {
        return InquiryId::fromString($this->inquiryId);
    }

    public function quantity(): int
    {
        return $this->quantity;
    }

    public function purpose(): ?string
    {
        return $this->purpose;
    }

    /**
     * @return array<int, string>
     */
    public function requirements(): array
    {
        return $this->requirements;
    }
}
