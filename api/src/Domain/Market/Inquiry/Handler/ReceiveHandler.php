<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\Command\Receive;
use App\Domain\Market\Inquiry\Exception\InquiryNotInExpectedStatus;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Inquiry\InquiryStatus;

class ReceiveHandler implements CommandHandlerInterface
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function __invoke(Receive $command): void
    {
        $inquiry = $this->inquiryRepository->get($command->inquiryId());

        $validTransitionStatus = [
            InquiryStatus::inExecution(),
            InquiryStatus::productionStarted(),
            InquiryStatus::productionFinished(),
            InquiryStatus::shipped(),
        ];

        if (
            $inquiry->status()->equalsOneOf($validTransitionStatus) === false
        ) {
            throw new InquiryNotInExpectedStatus(
                $command->inquiryId(),
                $inquiry->status(),
                InquiryStatus::productionAborted(),
                $validTransitionStatus
            );
        }

        $inquiry->complete();
        $this->inquiryRepository->save($inquiry);
    }
}
