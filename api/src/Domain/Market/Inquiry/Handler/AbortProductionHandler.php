<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\Command\AbortProduction;
use App\Domain\Market\Inquiry\Exception\InquiryNotInExpectedStatus;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Inquiry\InquiryStatus;

class AbortProductionHandler implements CommandHandlerInterface
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function __invoke(AbortProduction $command): void
    {
        $inquiry = $this->inquiryRepository->get($command->inquiryId());

        $validTransitionStatus = [
            InquiryStatus::inExecution(),
            InquiryStatus::productionStarted(),
        ];

        if (
            $inquiry->status()->equalsOneOf($validTransitionStatus) === false
        ) {
            throw new InquiryNotInExpectedStatus(
                $command->inquiryId(),
                $inquiry->status(),
                InquiryStatus::productionAborted(),
                $validTransitionStatus
            );
        }

        $inquiry->abortProduction();
        $this->inquiryRepository->save($inquiry);
    }
}
