<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\Command\StartProduction;
use App\Domain\Market\Inquiry\Exception\InquiryNotInExpectedStatus;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Inquiry\InquiryStatus;

class StartProductionHandler implements CommandHandlerInterface
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function __invoke(StartProduction $command): void
    {
        $inquiry = $this->inquiryRepository->get($command->inquiryId());

        $validTransitionStatus = [
            InquiryStatus::inExecution(),
        ];

        if ($inquiry->status()->equalsOneOf($validTransitionStatus) === false) {
            throw new InquiryNotInExpectedStatus(
                $command->inquiryId(),
                $inquiry->status(),
                InquiryStatus::inExecution(),
                $validTransitionStatus
            );
        }

        $inquiry->startProduction();
        $this->inquiryRepository->save($inquiry);
    }
}
