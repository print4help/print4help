<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\Command\UpdateInquiryMetadata;
use App\Domain\Market\Inquiry\Exception\InquiryNotInExpectedStatus;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Inquiry\InquiryStatus;

class UpdateInquiryMetadataHandler implements CommandHandlerInterface
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function __invoke(UpdateInquiryMetadata $command): void
    {
        $inquiry = $this->inquiryRepository->get($command->inquiryId());

        if ($inquiry->status()->equals(InquiryStatus::open()) === false) {
            throw new InquiryNotInExpectedStatus(
                $command->inquiryId(),
                $inquiry->status(),
                InquiryStatus::inExecution(),
                [InquiryStatus::open()]
            );
        }

        $inquiry->updateMetadata(
            $command->quantity(),
            $command->purpose(),
            $command->requirements()
        );

        $this->inquiryRepository->save($inquiry);
    }
}
