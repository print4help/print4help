<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Inquiry\InquiryId;
use Throwable;

final class InquiryNotFound extends DomainException
{
    public function __construct(InquiryId $inquiryId, Throwable $previous = null)
    {
        parent::__construct(sprintf('Inquiry with ID [%s] not found', $inquiryId->toString()), 0, $previous);
    }
}
