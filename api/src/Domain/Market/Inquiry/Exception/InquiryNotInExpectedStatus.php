<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryStatus;
use Throwable;

final class InquiryNotInExpectedStatus extends DomainException
{
    /**
     * @param non-empty-array<int, InquiryStatus> $expectedStatus
     */
    public function __construct(
        InquiryId $inquiryId,
        InquiryStatus $currentStatus,
        InquiryStatus $targetStatus,
        array $expectedStatus,
        Throwable $previous = null
    ) {
        $expectedStatusList = array_map(
            static fn (InquiryStatus $inquiryStatus): string => $inquiryStatus->toString(),
            $expectedStatus
        );
        parent::__construct(
            sprintf(
                'Inquiry with ID [%s] can not be transitioned to [%s] because the status is [%s]. Expected Status [%s]',
                $inquiryId->toString(),
                $targetStatus->toString(),
                $currentStatus->toString(),
                implode('|', $expectedStatusList)
            ),
            0,
            $previous
        );
    }
}
