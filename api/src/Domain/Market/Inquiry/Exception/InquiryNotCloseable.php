<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryStatus;
use Throwable;

final class InquiryNotCloseable extends DomainException
{
    public function __construct(InquiryId $inquiryId, InquiryStatus $status, Throwable $previous = null)
    {
        parent::__construct(
            sprintf(
                'Inquiry with ID [%s] can not be closed because the status is [%s]',
                $inquiryId->toString(),
                $status->toString()
            ),
            0,
            $previous
        );
    }
}
