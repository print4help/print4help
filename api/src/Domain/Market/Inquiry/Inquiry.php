<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Market\Inquiry\Event\Closed;
use App\Domain\Market\Inquiry\Event\Completed;
use App\Domain\Market\Inquiry\Event\InquiryCreated;
use App\Domain\Market\Inquiry\Event\Locked;
use App\Domain\Market\Inquiry\Event\MetadataUpdated;
use App\Domain\Market\Inquiry\Event\OfferAdded;
use App\Domain\Market\Inquiry\Event\OfferRemoved;
use App\Domain\Market\Inquiry\Event\ProductionAborted;
use App\Domain\Market\Inquiry\Event\ProductionFinished;
use App\Domain\Market\Inquiry\Event\ProductionStarted;
use App\Domain\Market\Inquiry\Event\Shipped;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateRoot;
use Webmozart\Assert\Assert;

class Inquiry extends AggregateRoot
{
    private InquiryId $id;
    private UserId $userId;
    private PieceId $pieceId;
    private InquiryStatus $status;
    private int $quantity;
    private ?string $purpose;
    /**
     * @var array<int, string>
     */
    private array $requirements;
    /**
     * @var array<int, string>
     */
    private array $offerIds;
    private ?DateTimeImmutable $createdAt;
    private ?DateTimeImmutable $updatedAt;
    private ?DateTimeImmutable $inExecutionAt;
    private ?DateTimeImmutable $productionStartedAt;
    private ?DateTimeImmutable $productionFinishedAt;
    private ?DateTimeImmutable $productionAbortedAt;
    private ?DateTimeImmutable $shippedAt;
    private ?DateTimeImmutable $completedAt;

    public function aggregateRootId(): string
    {
        return $this->id->toString();
    }

    public function id(): InquiryId
    {
        return $this->id;
    }

    public function status(): InquiryStatus
    {
        return $this->status;
    }

    public function pieceId(): PieceId
    {
        return $this->pieceId;
    }

    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return array<int, OfferId>
     */
    public function offerIds(): array
    {
        return array_map(
            fn (string $offerId): OfferId => OfferId::fromString($offerId),
            $this->offerIds
        );
    }

    /**
     * @param array<int, string> $requirements
     */
    public static function create(
        UserId $userId,
        InquiryId $inquiryId,
        PieceId $pieceId,
        int $quantity,
        ?string $purpose = null,
        array $requirements = []
    ): self {
        Assert::greaterThan($quantity, 0);
        Assert::lessThan($quantity, 100);
        Assert::allMaxLength($requirements, 1000);
        Assert::nullOrString($purpose);
        if (is_string($purpose)) {
            Assert::maxLength($purpose, 1000);
        }

        $self = new self();
        $self->apply(InquiryCreated::raise(
            $userId,
            $inquiryId,
            $pieceId,
            InquiryStatus::open(),
            $quantity,
            $purpose,
            $requirements
        ));

        return $self;
    }

    public function applyInquiryCreated(InquiryCreated $event): void
    {
        $this->id = $event->inquiryId();
        $this->status = $event->status();
        $this->userId = $event->userId();
        $this->pieceId = $event->pieceId();
        $this->quantity = $event->quantity();
        $this->purpose = $event->purpose();
        $this->requirements = $event->requirements();
        $this->createdAt = $event->createdAt();
    }

    /**
     * @param array<int, string> $requirements
     */
    public function updateMetadata(
        int $quantity,
        ?string $purpose,
        array $requirements = []
    ): void {
        Assert::greaterThan($quantity, 0);
        Assert::lessThan($quantity, 100);
        Assert::allMaxLength($requirements, 1000);
        Assert::nullOrString($purpose);
        if (is_string($purpose)) {
            Assert::maxLength($purpose, 1000);
        }

        $this->apply(MetadataUpdated::raise(
            $this->id,
            $quantity,
            $purpose,
            $requirements
        ));
    }

    public function applyMetadataUpdated(MetadataUpdated $event): void
    {
        $this->quantity = $event->quantity();
        $this->purpose = $event->purpose();
        $this->requirements = $event->requirements();
        $this->updatedAt = $event->updatedAt();
    }

    public function lock(): void
    {
        $this->apply(
            Locked::raise($this->id, InquiryStatus::inExecution())
        );
    }

    public function applyLocked(Locked $event): void
    {
        $this->status = $event->status();
        $this->inExecutionAt = $event->inExecutionAt();
    }

    public function close(): void
    {
        $this->apply(
            Closed::raise($this->id, InquiryStatus::closed())
        );
    }

    public function applyClosed(Closed $event): void
    {
        $this->status = $event->status();
        $this->updatedAt = $event->updatedAt();
    }

    public function addOffer(OfferId $offerId): void
    {
        $this->apply(
            OfferAdded::raise($this->id, $offerId)
        );
    }

    public function applyOfferAdded(OfferAdded $event): void
    {
        $this->offerIds[] = $event->offerId()->toString();
    }

    public function removeOffer(OfferId $offerId): void
    {
        $this->apply(
            OfferRemoved::raise($this->id, $offerId)
        );
    }

    public function applyOfferRemoved(OfferRemoved $event): void
    {
        $index = array_search($event->offerId()->toString(), $this->offerIds, true);
        unset($this->offerIds[$index]);
        // reset index
        $this->offerIds = array_values($this->offerIds);
    }

    public function startProduction(): void
    {
        $this->apply(
            ProductionStarted::raise($this->id, InquiryStatus::productionStarted())
        );
    }

    public function applyProductionStarted(ProductionStarted $event): void
    {
        $this->status = $event->status();
        $this->productionStartedAt = $event->productionStartedAt();
    }

    public function abortProduction(): void
    {
        $this->apply(
            ProductionAborted::raise($this->id, InquiryStatus::productionAborted())
        );
    }

    public function applyProductionAborted(ProductionAborted $event): void
    {
        $this->status = $event->status();
        $this->productionAbortedAt = $event->productionAbortedAt();
    }

    public function finishProduction(): void
    {
        $this->apply(
            ProductionFinished::raise($this->id, InquiryStatus::productionFinished())
        );
    }

    public function applyProductionFinished(ProductionFinished $event): void
    {
        $this->status = $event->status();
        $this->productionFinishedAt = $event->productionFinishedAt();
    }

    public function ship(): void
    {
        $this->apply(
            Shipped::raise($this->id, InquiryStatus::shipped())
        );
    }

    public function applyShipped(Shipped $event): void
    {
        $this->status = $event->status();
        $this->shippedAt = $event->shippedAt();
    }

    public function complete(): void
    {
        $this->apply(
            Completed::raise($this->id, InquiryStatus::completed())
        );
    }

    public function applyCompleted(Completed $event): void
    {
        $this->status = $event->status();
        $this->completedAt = $event->completedAt();
    }
}
