<?php declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryStatus;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class Completed extends AggregateChanged
{
    public static function raise(
        InquiryId $id,
        InquiryStatus $status,
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'status' => $status->toString(),
                'completedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function inquiryId(): InquiryId
    {
        return InquiryId::fromString($this->aggregateId);
    }

    public function status(): InquiryStatus
    {
        return InquiryStatus::fromString($this->payload['status']);
    }

    public function completedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['completedAt']);
    }
}
