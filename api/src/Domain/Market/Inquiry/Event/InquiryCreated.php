<?php declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Event;

use App\Domain\Account\UserId;
use App\Domain\DateTimeHelper;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryStatus;
use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class InquiryCreated extends AggregateChanged
{
    /**
     * @param array<int, string> $requirements
     */
    public static function raise(
        UserId $userId,
        InquiryId $id,
        PieceId $pieceId,
        InquiryStatus $status,
        int $quantity,
        ?string $purpose = null,
        array $requirements = []
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'userId' => $userId->toString(),
                'pieceId' => $pieceId->toString(),
                'status' => $status->toString(),
                'quantity' => $quantity,
                'purpose' => $purpose,
                'requirements' => $requirements,
                'createdAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->payload['userId']);
    }

    public function inquiryId(): InquiryId
    {
        return InquiryId::fromString($this->aggregateId);
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->payload['pieceId']);
    }

    public function status(): InquiryStatus
    {
        return InquiryStatus::fromString($this->payload['status']);
    }

    public function quantity(): int
    {
        return $this->payload['quantity'];
    }

    public function purpose(): ?string
    {
        return $this->payload['purpose'];
    }

    /**
     * @return array<int, string>
     */
    public function requirements(): array
    {
        return $this->payload['requirements'];
    }

    public function createdAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['createdAt']);
    }
}
