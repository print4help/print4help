<?php declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Event;

use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\OfferId;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class OfferRemoved extends AggregateChanged
{
    public static function raise(
        InquiryId $id,
        OfferId $offerId,
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'offerId' => $offerId->toString(),
            ]
        );
    }

    public function inquiryId(): InquiryId
    {
        return InquiryId::fromString($this->aggregateId);
    }

    public function offerId(): OfferId
    {
        return OfferId::fromString($this->payload['offerId']);
    }
}
