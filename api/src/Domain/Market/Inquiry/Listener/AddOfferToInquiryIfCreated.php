<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Listener;

use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Offer\Event\OfferCreated;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class AddOfferToInquiryIfCreated implements Listener
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof OfferCreated) {
            return;
        }

        $inquiry = $this->inquiryRepository->get($event->inquiryId());

        $inquiry->addOffer($event->offerId());
        $this->inquiryRepository->save($inquiry);

        // todo send notification to customer
    }
}
