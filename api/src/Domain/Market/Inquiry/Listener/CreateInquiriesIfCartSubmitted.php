<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Listener;

use App\Domain\Market\Cart\CartRepository;
use App\Domain\Market\Cart\Event\CartSubmitted;
use App\Domain\Market\Inquiry\Inquiry;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryRepository;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class CreateInquiriesIfCartSubmitted implements Listener
{
    public function __construct(
        private CartRepository $cartRepository,
        private InquiryRepository $inquiryRepository
    ) {
    }

    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof CartSubmitted) {
            return;
        }

        $cart = $this->cartRepository->get($event->cartId());

        foreach ($cart->cartItems() as $cartItem) {
            $inquiry = Inquiry::create(
                $cart->userId(),
                InquiryId::create(),
                $cartItem->pieceId(),
                $cartItem->quantity(),
                $cartItem->purpose(),
                $cartItem->requirements()
            );
            $this->inquiryRepository->save($inquiry);
        }
    }
}
