<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Listener;

use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Offer\Event\OfferAccepted;
use App\Domain\Market\Offer\OfferRepository;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class LockInquiryIfOfferAccepted implements Listener
{
    public function __construct(
        private OfferRepository $offerRepository,
        private InquiryRepository $inquiryRepository
    ) {
    }

    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof OfferAccepted) {
            return;
        }

        $offer = $this->offerRepository->get($event->offerId());
        $inquiry = $this->inquiryRepository->get($offer->inquiryId());

        $inquiry->lock();
        $this->inquiryRepository->save($inquiry);
    }
}
