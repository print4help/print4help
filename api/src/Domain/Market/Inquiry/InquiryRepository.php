<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry;

use App\Domain\Market\Inquiry\Exception\InquiryNotFound;

interface InquiryRepository
{
    /**
     * @throws InquiryNotFound
     */
    public function get(InquiryId $inquiryId): Inquiry;
    public function save(Inquiry $inquiry): void;
}
