<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer;

use Patchlevel\Enum\ExtendedEnum;

/**
 * @psalm-immutable
 */
final class OfferStatus extends ExtendedEnum
{
    private const PLACED = 'placed';
    private const ACCEPTED = 'accepted';
    private const REJECTED = 'rejected';
    private const WITHDRAWN = 'withdrawn';

    public static function placed(): self
    {
        return self::get(self::PLACED);
    }

    public static function accepted(): self
    {
        return self::get(self::ACCEPTED);
    }

    public static function rejected(): self
    {
        return self::get(self::REJECTED);
    }

    public static function withdrawn(): self
    {
        return self::get(self::WITHDRAWN);
    }
}
