<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer;

use Patchlevel\Enum\ExtendedEnum;

/**
 * @psalm-immutable
 */
final class ChatStatus extends ExtendedEnum
{
    private const OPEN = 'open';
    private const CLOSED = 'closed';

    public static function open(): self
    {
        return self::get(self::OPEN);
    }

    public static function closed(): self
    {
        return self::get(self::CLOSED);
    }
}
