<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer;

use App\Domain\UuidBehaviour;

class MessageId
{
    use UuidBehaviour;
}
