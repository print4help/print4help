<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer;

use App\Domain\Account\UserId;
use App\Domain\Market\Offer\Event\ChatClosed;
use App\Domain\Market\Offer\Event\ChatCreated;
use App\Domain\Market\Offer\Event\MessageAdded;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateRoot;
use Webmozart\Assert\Assert;

class Chat extends AggregateRoot
{
    private ChatId $id;
    private ChatStatus $status;
    private OfferId $offerId;
    private string $subject;
    /**
     * @var array<array-key, MessageId>
     */
    private array $messages = [];
    /**
     * @var array<array-key, UserId>
     */
    private array $userIds = [];
    private ?DateTimeImmutable $createdAt;
    private ?DateTimeImmutable $updatedAt;
    private ?DateTimeImmutable $closedAt;

    public function aggregateRootId(): string
    {
        return $this->id->toString();
    }

    public function id(): ChatId
    {
        return $this->id;
    }

    public function offerId(): OfferId
    {
        return $this->offerId;
    }

    public function status(): ChatStatus
    {
        return $this->status;
    }

    /**
     * @param array<array-key, UserId> $userIds
     */
    public static function create(
        ChatId $id,
        OfferId $offerId,
        array $userIds,
        string $subject
    ): self {
        Assert::maxLength($subject, 100);

        $self = new self();
        $self->apply(ChatCreated::raise(
            $id,
            $offerId,
            ChatStatus::open(),
            $userIds,
            $subject
        ));

        return $self;
    }

    public function applyChatCreated(ChatCreated $event): void
    {
        $this->id = $event->chatId();
        $this->status = $event->status();
        $this->offerId = $event->offerId();
        $this->userIds = $event->userIds();
        $this->subject = $event->subject();
        $this->createdAt = $event->createdAt();
    }

    public function addMessage(MessageId $messageId): void
    {
        $this->apply(MessageAdded::raise(
            $this->id,
            $messageId
        ));
    }

    public function applyMessageAdded(MessageAdded $event): void
    {
        $this->messages[] = $event->messageId();
        $this->updatedAt = $event->updatedAt();
    }

    public function close(): void
    {
        $this->apply(ChatClosed::raise(
            $this->id,
            ChatStatus::closed()
        ));
    }

    public function applyChatClosed(ChatClosed $event): void
    {
        $this->status = $event->status();
        $this->closedAt = $event->closedAt();
    }
}
