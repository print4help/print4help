<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\ChatStatus;
use Throwable;

final class ChatStatusNotOpen extends DomainException
{
    public function __construct(ChatId $chatId, ChatStatus $status, Throwable $previous = null)
    {
        parent::__construct(sprintf(
            'Chat with ID [%s] does not have status [open]. Status [%s] instead',
            $chatId->toString(),
            $status->toString()
        ), 0, $previous);
    }
}
