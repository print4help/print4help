<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Offer\MessageId;
use Throwable;

final class MessageNotFound extends DomainException
{
    public function __construct(MessageId $messageId, Throwable $previous = null)
    {
        parent::__construct(sprintf('Message with ID [%s] not found', $messageId->toString()), 0, $previous);
    }
}
