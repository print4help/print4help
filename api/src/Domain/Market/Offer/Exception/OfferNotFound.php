<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Offer\OfferId;
use Throwable;

final class OfferNotFound extends DomainException
{
    public function __construct(OfferId $offerId, Throwable $previous = null)
    {
        parent::__construct(
            sprintf('Offer with ID [%s] not found', $offerId->toString()),
            0,
            $previous
        );
    }
}
