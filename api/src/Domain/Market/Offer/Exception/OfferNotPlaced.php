<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Offer\OfferStatus;
use Throwable;

final class OfferNotPlaced extends DomainException
{
    public function __construct(OfferId $offerId, OfferStatus $status, Throwable $previous = null)
    {
        parent::__construct(
            sprintf(
                'Offer with ID [%s] must be placed, but its status is [%s]',
                $offerId->toString(),
                $status->toString()
            ),
            0,
            $previous
        );
    }
}
