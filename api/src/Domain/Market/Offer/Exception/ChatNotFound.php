<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Offer\ChatId;
use Throwable;

final class ChatNotFound extends DomainException
{
    public function __construct(ChatId $chatId, Throwable $previous = null)
    {
        parent::__construct(sprintf('Chat with ID [%s] not found', $chatId->toString()), 0, $previous);
    }
}
