<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\ChatStatus;
use Throwable;

final class MessageCanNotBeUpdated extends DomainException
{
    public function __construct(
        ChatId $chatId,
        ChatStatus $chatStatus,
        Throwable $previous = null
    ) {
        parent::__construct(
            sprintf(
                'Message can not be updated because Chat is status [%s:%s]',
                $chatId->toString(),
                $chatStatus->toString()
            ),
            0,
            $previous
        );
    }
}
