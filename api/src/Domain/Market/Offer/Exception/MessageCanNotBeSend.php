<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryStatus;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Offer\OfferStatus;
use Throwable;

final class MessageCanNotBeSend extends DomainException
{
    public function __construct(
        OfferId $offerId,
        OfferStatus $offerStatus,
        InquiryId $inquiryId,
        InquiryStatus $inquiryStatus,
        Throwable $previous = null
    ) {
        parent::__construct(
            sprintf(
                'Message can not be send because Offer is status [%s:%s] or Inquiry is status [%s:%s]',
                $offerId->toString(),
                $offerStatus->toString(),
                $inquiryId->toString(),
                $inquiryStatus->toString()
            ),
            0,
            $previous
        );
    }
}
