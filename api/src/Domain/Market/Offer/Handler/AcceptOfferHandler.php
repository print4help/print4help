<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\Exception\InquiryNotOpen;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Inquiry\InquiryStatus;
use App\Domain\Market\Offer\Command\AcceptOffer;
use App\Domain\Market\Offer\Exception\OfferNotPlaced;
use App\Domain\Market\Offer\OfferRepository;
use App\Domain\Market\Offer\OfferStatus;

class AcceptOfferHandler implements CommandHandlerInterface
{
    public function __construct(
        private OfferRepository $offerRepository,
        private InquiryRepository $inquiryRepository,
    ) {
    }

    public function __invoke(AcceptOffer $command): void
    {
        $offer = $this->offerRepository->get($command->offerId());

        if ($offer->status()->equals(OfferStatus::placed()) === false) {
            throw new OfferNotPlaced($command->offerId(), $offer->status());
        }

        $inquiry = $this->inquiryRepository->get($offer->inquiryId());
        
        if ($inquiry->status()->equals(InquiryStatus::open()) === false) {
            throw new InquiryNotOpen($offer->inquiryId(), $inquiry->status());
        }

        $offer->accept();
        $this->offerRepository->save($offer);
    }
}
