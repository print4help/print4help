<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Offer\Chat;
use App\Domain\Market\Offer\ChatRepository;
use App\Domain\Market\Offer\ChatStatus;
use App\Domain\Market\Offer\Command\CloseChat;
use App\Domain\Market\Offer\Exception\ChatStatusNotOpen;
use App\Domain\Market\Offer\Message;

class CloseChatHandler implements CommandHandlerInterface
{
    public function __construct(private ChatRepository $chatRepository)
    {
    }

    public function __invoke(CloseChat $command): void
    {
        $chat = $this->chatRepository->get($command->chatId());

        if ($chat->status()->equals(ChatStatus::open()) === false) {
            throw new ChatStatusNotOpen($chat->id(), $chat->status());
        }

        $chat->close();

        $this->chatRepository->save($chat);

        // todo: send message to users that chat is closed ?
    }
}
