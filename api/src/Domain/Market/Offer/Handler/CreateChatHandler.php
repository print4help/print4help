<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Handler;

use App\Domain\Account\UserId;
use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Inquiry\InquiryStatus;
use App\Domain\Market\Offer\Chat;
use App\Domain\Market\Offer\ChatRepository;
use App\Domain\Market\Offer\Command\CreateChat;
use App\Domain\Market\Offer\Exception\ChatCanNotBeCreated;
use App\Domain\Market\Offer\Message;
use App\Domain\Market\Offer\MessageId;
use App\Domain\Market\Offer\MessageRepository;
use App\Domain\Market\Offer\OfferRepository;
use App\Domain\Market\Offer\OfferStatus;
use Webmozart\Assert\Assert;

class CreateChatHandler implements CommandHandlerInterface
{
    public function __construct(
        private OfferRepository $offerRepository,
        private InquiryRepository $inquiryRepository,
        private ChatRepository $chatRepository,
        private MessageRepository $messageRepository
    ) {
    }

    public function __invoke(CreateChat $command): void
    {
        $this->assertChatCanBeCreated($command);

        // we force a subject when we create a new chat
        $subject = $command->subject();
        Assert::stringNotEmpty($subject);

        $offer = $this->offerRepository->get($command->offerId());
        $userIds = $this->extractUserIds($command);

        $chat = Chat::create(
            $command->chatId(),
            $command->offerId(),
            $userIds,
            $subject
        );

        $message = Message::create(
            MessageId::create(),
            $chat->id(),
            $offer->userId(),
            $command->messageBody()
        );

        $chat->addMessage($message->id());

        $this->chatRepository->save($chat);
        $this->messageRepository->save($message);
    }

    private function assertChatCanBeCreated(CreateChat $command): void
    {
        $offer = $this->offerRepository->get($command->offerId());
        $inquiry = $this->inquiryRepository->get($offer->inquiryId());

        $exception = new ChatCanNotBeCreated(
            $offer->id(),
            $offer->status(),
            $inquiry->id(),
            $inquiry->status()
        );

        if ($inquiry->status()->equals(InquiryStatus::open()) === false) {
            throw $exception;
        }

        if ($offer->status()->equals(OfferStatus::placed()) === false) {
            throw $exception;
        }
    }

    /**
     * @return array<array-key, UserId>
     */
    private function extractUserIds(CreateChat $command): array
    {
        $userIds = [];

        $offer = $this->offerRepository->get($command->offerId());
        $inquiry = $this->inquiryRepository->get($offer->inquiryId());

        $userIds[] = $offer->userId();
        $userIds[] = $inquiry->userId();

        return $userIds;
    }
}
