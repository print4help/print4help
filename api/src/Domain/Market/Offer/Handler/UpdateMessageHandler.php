<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Offer\Chat;
use App\Domain\Market\Offer\ChatRepository;
use App\Domain\Market\Offer\ChatStatus;
use App\Domain\Market\Offer\Command\UpdateMessage;
use App\Domain\Market\Offer\Exception\MessageCanNotBeUpdated;
use App\Domain\Market\Offer\MessageRepository;
use Webmozart\Assert\Assert;

class UpdateMessageHandler implements CommandHandlerInterface
{
    public function __construct(
        private ChatRepository $chatRepository,
        private MessageRepository $messageRepository,
    ) {
    }

    public function __invoke(UpdateMessage $command): void
    {
        $message = $this->messageRepository->get($command->messageId());
        $chat = $this->chatRepository->get($message->chatId());

        Assert::eq($message->authorId()->toString(), $command->authorId()->toString());
        $this->assertMessageCanBeUpdated($chat);

        $message->update(
            $command->body(),
            $command->subject()
        );

        $this->messageRepository->save($message);
    }

    private function assertMessageCanBeUpdated(Chat $chat): void
    {
        if ($chat->status()->equals(ChatStatus::closed())) {
            throw new MessageCanNotBeUpdated(
                $chat->id(),
                $chat->status()
            );
        }
    }
}
