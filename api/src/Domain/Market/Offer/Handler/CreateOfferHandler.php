<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Handler;

use App\Domain\Account\Exception\UserNotActive;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\Exception\InquiryNotOpen;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Inquiry\InquiryStatus;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\Offer;
use App\Domain\Market\Offer\OfferRepository;

class CreateOfferHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private InquiryRepository $inquiryRepository,
        private OfferRepository $offerRepository
    ) {
    }

    public function __invoke(CreateOffer $command): void
    {
        $user = $this->userRepository->get($command->userId());
        if (!$user->isActive()) {
            throw new UserNotActive($command->userId());
        }

        $inquiry = $this->inquiryRepository->get($command->inquiryId());
        if ($inquiry->status()->equals(InquiryStatus::open()) === false) {
            throw new InquiryNotOpen($command->inquiryId(), $inquiry->status());
        }

        $offer = Offer::place(
            $command->userId(),
            $command->offerId(),
            $command->inquiryId(),
            $inquiry->pieceId(),
            $command->message(),
            $command->quantity()
        );

        $this->offerRepository->save($offer);
    }
}
