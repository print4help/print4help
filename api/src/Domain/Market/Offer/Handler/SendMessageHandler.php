<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Inquiry\InquiryStatus;
use App\Domain\Market\Offer\Chat;
use App\Domain\Market\Offer\ChatRepository;
use App\Domain\Market\Offer\Command\SendMessage;
use App\Domain\Market\Offer\Exception\MessageCanNotBeSend;
use App\Domain\Market\Offer\Message;
use App\Domain\Market\Offer\MessageRepository;
use App\Domain\Market\Offer\OfferRepository;
use App\Domain\Market\Offer\OfferStatus;

class SendMessageHandler implements CommandHandlerInterface
{
    public function __construct(
        private OfferRepository $offerRepository,
        private InquiryRepository $inquiryRepository,
        private ChatRepository $chatRepository,
        private MessageRepository $messageRepository
    ) {
    }

    public function __invoke(SendMessage $command): void
    {
        $chat = $this->chatRepository->get($command->chatId());

        $this->assertMessageCanBeSend($chat);

        $message = Message::create(
            $command->messageId(),
            $chat->id(),
            $command->authorId(),
            $command->body(),
            $command->subject()
        );

        $this->messageRepository->save($message);

        $chat->addMessage($message->id());
        $this->chatRepository->save($chat);
    }

    private function assertMessageCanBeSend(Chat $chat): void
    {
        $offer = $this->offerRepository->get($chat->offerId());
        $inquiry = $this->inquiryRepository->get($offer->inquiryId());

        $exception = new MessageCanNotBeSend(
            $offer->id(),
            $offer->status(),
            $inquiry->id(),
            $inquiry->status()
        );

        if ($inquiry->status()->equals(InquiryStatus::closed())) {
            throw $exception;
        }

        if ($inquiry->status()->equals(InquiryStatus::completed())) {
            throw $exception;
        }

        if ($offer->status()->equals(OfferStatus::rejected())) {
            throw $exception;
        }

        if ($offer->status()->equals(OfferStatus::withdrawn())) {
            throw $exception;
        }
    }
}
