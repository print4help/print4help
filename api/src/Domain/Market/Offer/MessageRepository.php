<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer;

interface MessageRepository
{
    public function get(MessageId $messageId): Message;

    public function save(Message $message): void;
}
