<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer;

use App\Domain\Account\UserId;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\Event\OfferAccepted;
use App\Domain\Market\Offer\Event\OfferChanged;
use App\Domain\Market\Offer\Event\OfferCreated;
use App\Domain\Market\Offer\Event\OfferRejected;
use App\Domain\Market\Offer\Event\OfferWithdrawn;
use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateRoot;
use Webmozart\Assert\Assert;

class Offer extends AggregateRoot
{
    private OfferId $id;
    private UserId $userId;
    private InquiryId $inquiryId;
    private PieceId $pieceId;
    private OfferStatus $status;
    private string $message;
    private int $quantity;

    private ?DateTimeImmutable $createdAt;
    private ?DateTimeImmutable $updatedAt;
    private ?DateTimeImmutable $acceptedAt;
    private ?DateTimeImmutable $rejectedAt;
    private ?DateTimeImmutable $withdrawnAt;

    public function aggregateRootId(): string
    {
        return $this->id->toString();
    }

    public function id(): OfferId
    {
        return $this->id;
    }

    public function userId(): UserId
    {
        return $this->userId;
    }

    public function inquiryId(): InquiryId
    {
        return $this->inquiryId;
    }

    public function status(): OfferStatus
    {
        return $this->status;
    }

    public static function place(
        UserId $userId,
        OfferId $offerId,
        InquiryId $inquiryId,
        PieceId $pieceId,
        string $message,
        int $quantity
    ): self {
        Assert::notEmpty($message);
        Assert::maxLength($message, 1000);
        Assert::greaterThan($quantity, 0);
        Assert::lessThan($quantity, 100);

        $self = new self();
        $self->apply(OfferCreated::raise(
            $userId,
            $offerId,
            $inquiryId,
            $pieceId,
            OfferStatus::placed(),
            $message,
            $quantity
        ));

        return $self;
    }

    public function applyOfferCreated(OfferCreated $event): void
    {
        $this->id = $event->offerId();
        $this->userId = $event->userId();
        $this->inquiryId = $event->inquiryId();
        $this->pieceId = $event->pieceId();
        $this->status = $event->status();
        $this->quantity = $event->quantity();
        $this->message = $event->message();
        $this->createdAt = $event->createdAt();
    }

    public function change(
        string $message,
        int $quantity
    ): void {
        Assert::maxLength($message, 1000);
        Assert::greaterThan($quantity, 0);
        Assert::lessThan($quantity, 100);

        $this->apply(OfferChanged::raise(
            $this->id,
            $message,
            $quantity
        ));
    }

    public function applyOfferChanged(OfferChanged $event): void
    {
        $this->quantity = $event->quantity();
        $this->message = $event->message();
        $this->updatedAt = $event->updatedAt();
    }

    public function accept(): void
    {
        $this->apply(
            OfferAccepted::raise($this->id, OfferStatus::accepted())
        );
    }

    public function applyOfferAccepted(OfferAccepted $event): void
    {
        $this->status = $event->status();
        $this->acceptedAt = $event->acceptedAt();
    }

    public function reject(): void
    {
        $this->apply(
            OfferRejected::raise($this->id, OfferStatus::rejected())
        );
    }

    public function applyOfferRejected(OfferRejected $event): void
    {
        $this->status = $event->status();
        $this->rejectedAt = $event->rejectedAt();
    }

    public function withdraw(): void
    {
        $this->apply(
            OfferWithdrawn::raise($this->id, OfferStatus::withdrawn())
        );
    }

    public function applyOfferWithdrawn(OfferWithdrawn $event): void
    {
        $this->status = $event->status();
        $this->withdrawnAt = $event->withdrawnAt();
    }
}
