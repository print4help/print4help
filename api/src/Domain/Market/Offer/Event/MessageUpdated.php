<?php declare(strict_types=1);

namespace App\Domain\Market\Offer\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Offer\MessageId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class MessageUpdated extends AggregateChanged
{
    public static function raise(
        MessageId $id,
        string $body,
        ?string $subject
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'body' => $body,
                'subject' => $subject,
                'updatedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function messageId(): MessageId
    {
        return MessageId::fromString($this->aggregateId);
    }

    public function body(): string
    {
        return $this->payload['body'];
    }

    public function subject(): ?string
    {
        return $this->payload['subject'];
    }

    public function updatedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['updatedAt']);
    }
}
