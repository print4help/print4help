<?php declare(strict_types=1);

namespace App\Domain\Market\Offer\Event;

use App\Domain\Account\UserId;
use App\Domain\DateTimeHelper;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\ChatStatus;
use App\Domain\Market\Offer\MessageId;
use App\Domain\Market\Offer\OfferId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class ChatCreated extends AggregateChanged
{
    /**
     * @param array<array-key, UserId> $userIds
     */
    public static function raise(
        ChatId $id,
        OfferId $offerId,
        ChatStatus $status,
        array $userIds,
        string $subject
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'offerId' => $offerId->toString(),
                'status' => $status->toString(),
                'userIds' => array_map(
                    static fn (UserId $userId): string => $userId->toString(),
                    $userIds
                ),
                'subject' => $subject,
                'createdAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function chatId(): ChatId
    {
        return ChatId::fromString($this->aggregateId);
    }

    public function offerId(): OfferId
    {
        return OfferId::fromString($this->payload['offerId']);
    }

    public function messageId(): MessageId
    {
        return MessageId::fromString($this->payload['messageId']);
    }

    /**
     * @return array<array-key, UserId>
     */
    public function userIds(): array
    {
        return array_map(
            static fn (string $userId): UserId => UserId::fromString($userId),
            (array)$this->payload['userIds']
        );
    }

    public function status(): ChatStatus
    {
        return ChatStatus::fromString($this->payload['status']);
    }

    public function subject(): string
    {
        return $this->payload['subject'];
    }

    public function createdAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['createdAt']);
    }
}
