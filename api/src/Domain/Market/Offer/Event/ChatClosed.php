<?php declare(strict_types=1);

namespace App\Domain\Market\Offer\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\ChatStatus;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class ChatClosed extends AggregateChanged
{
    public static function raise(
        ChatId $id,
        ChatStatus $status,
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'status' => $status->toString(),
                'closedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function chatId(): ChatId
    {
        return ChatId::fromString($this->aggregateId);
    }

    public function status(): ChatStatus
    {
        return ChatStatus::fromString($this->payload['status']);
    }

    public function closedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['closedAt']);
    }
}
