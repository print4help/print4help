<?php declare(strict_types=1);

namespace App\Domain\Market\Offer\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\MessageId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class MessageAdded extends AggregateChanged
{
    public static function raise(
        ChatId $id,
        MessageId $messageId,
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'messageId' => $messageId->toString(),
                'updatedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function chatId(): ChatId
    {
        return ChatId::fromString($this->aggregateId);
    }

    public function messageId(): MessageId
    {
        return MessageId::fromString($this->payload['messageId']);
    }

    public function updatedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['updatedAt']);
    }
}
