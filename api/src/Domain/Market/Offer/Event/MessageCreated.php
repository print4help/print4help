<?php declare(strict_types=1);

namespace App\Domain\Market\Offer\Event;

use App\Domain\Account\UserId;
use App\Domain\DateTimeHelper;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\MessageId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class MessageCreated extends AggregateChanged
{
    public static function raise(
        MessageId $id,
        ChatId $chatId,
        UserId $authorId,
        string $body,
        ?string $subject
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'authorId' => $authorId->toString(),
                'chatId' => $chatId->toString(),
                'body' => $body,
                'subject' => $subject,
                'createdAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function messageId(): MessageId
    {
        return MessageId::fromString($this->aggregateId);
    }

    public function authorId(): UserId
    {
        return UserId::fromString($this->payload['authorId']);
    }

    public function chatId(): ChatId
    {
        return ChatId::fromString($this->payload['chatId']);
    }

    public function body(): string
    {
        return $this->payload['body'];
    }

    public function subject(): ?string
    {
        return $this->payload['subject'];
    }

    public function createdAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['createdAt']);
    }
}
