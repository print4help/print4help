<?php declare(strict_types=1);

namespace App\Domain\Market\Offer\Event;

use App\Domain\Account\UserId;
use App\Domain\DateTimeHelper;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Offer\OfferStatus;
use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class OfferCreated extends AggregateChanged
{
    public static function raise(
        UserId $userId,
        OfferId $id,
        InquiryId $inquiryId,
        PieceId $pieceId,
        OfferStatus $status,
        string $message,
        int $quantity
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'userId' => $userId->toString(),
                'inquiryId' => $inquiryId->toString(),
                'pieceId' => $pieceId->toString(),
                'status' => $status->toString(),
                'quantity' => $quantity,
                'message' => $message,
                'createdAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->payload['userId']);
    }

    public function offerId(): OfferId
    {
        return OfferId::fromString($this->aggregateId);
    }

    public function inquiryId(): InquiryId
    {
        return InquiryId::fromString($this->payload['inquiryId']);
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->payload['pieceId']);
    }

    public function status(): OfferStatus
    {
        return OfferStatus::fromString($this->payload['status']);
    }

    public function quantity(): int
    {
        return $this->payload['quantity'];
    }

    public function message(): string
    {
        return $this->payload['message'];
    }

    public function createdAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['createdAt']);
    }
}
