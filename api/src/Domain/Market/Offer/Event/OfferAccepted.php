<?php declare(strict_types=1);

namespace App\Domain\Market\Offer\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Offer\OfferStatus;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class OfferAccepted extends AggregateChanged
{
    public static function raise(
        OfferId $id,
        OfferStatus $status,
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'status' => $status->toString(),
                'acceptedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function offerId(): OfferId
    {
        return OfferId::fromString($this->aggregateId);
    }

    public function status(): OfferStatus
    {
        return OfferStatus::fromString($this->payload['status']);
    }

    public function acceptedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['acceptedAt']);
    }
}
