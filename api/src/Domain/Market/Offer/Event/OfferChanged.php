<?php declare(strict_types=1);

namespace App\Domain\Market\Offer\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Offer\OfferId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class OfferChanged extends AggregateChanged
{
    public static function raise(
        OfferId $id,
        string $message,
        int $quantity
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'quantity' => $quantity,
                'message' => $message,
                'updatedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function offerId(): OfferId
    {
        return OfferId::fromString($this->aggregateId);
    }

    public function quantity(): int
    {
        return $this->payload['quantity'];
    }

    public function message(): string
    {
        return $this->payload['message'];
    }

    public function updatedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['updatedAt']);
    }
}
