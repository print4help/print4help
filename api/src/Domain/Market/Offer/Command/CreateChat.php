<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Command;

use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\OfferId;

class CreateChat
{
    private string $chatId;
    private string $offerId;
    private string $subject;
    private string $messageBody;

    public function __construct(
        ChatId $chatId,
        OfferId $offerId,
        string $subject,
        string $messageBody
    ) {
        $this->chatId = $chatId->toString();
        $this->offerId = $offerId->toString();
        $this->subject = $subject;
        $this->messageBody = $messageBody;
    }

    public function chatId(): ChatId
    {
        return ChatId::fromString($this->chatId);
    }

    public function offerId(): OfferId
    {
        return OfferId::fromString($this->offerId);
    }

    public function subject(): string
    {
        return $this->subject;
    }

    public function messageBody(): string
    {
        return $this->messageBody;
    }
}
