<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Command;

use App\Domain\Market\Offer\ChatId;

class CloseChat
{
    private string $chatId;

    public function __construct(ChatId $chatId)
    {
        $this->chatId = $chatId->toString();
    }

    public function chatId(): ChatId
    {
        return ChatId::fromString($this->chatId);
    }
}
