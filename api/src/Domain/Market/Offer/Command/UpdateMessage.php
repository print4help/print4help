<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Command;

use App\Domain\Account\UserId;
use App\Domain\Market\Offer\MessageId;

class UpdateMessage
{
    private string $messageId;
    private string $authorId;
    private string $body;
    private ?string $subject;

    public function __construct(
        MessageId $messageId,
        UserId $authorId,
        string $body,
        ?string $subject
    ) {
        $this->messageId = $messageId->toString();
        $this->authorId = $authorId->toString();
        $this->body = $body;
        $this->subject = $subject;
    }

    public function messageId(): MessageId
    {
        return MessageId::fromString($this->messageId);
    }

    public function authorId(): UserId
    {
        return UserId::fromString($this->authorId);
    }

    public function body(): string
    {
        return $this->body;
    }

    public function subject(): ?string
    {
        return $this->subject;
    }
}
