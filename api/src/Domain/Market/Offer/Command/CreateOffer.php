<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Command;

use App\Domain\Account\UserId;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\OfferId;

class CreateOffer
{
    private string $offerId;
    private string $userId;
    private string $inquiryId;
    private string $message;
    private int $quantity;

    public function __construct(
        OfferId $offerId,
        UserId $userId,
        InquiryId $inquiryId,
        string $message,
        int $quantity
    ) {
        $this->offerId = $offerId->toString();
        $this->userId = $userId->toString();
        $this->inquiryId = $inquiryId->toString();
        $this->message = $message;
        $this->quantity = $quantity;
    }

    public function offerId(): OfferId
    {
        return OfferId::fromString($this->offerId);
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->userId);
    }

    public function inquiryId(): InquiryId
    {
        return InquiryId::fromString($this->inquiryId);
    }

    public function message(): string
    {
        return $this->message;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }
}
