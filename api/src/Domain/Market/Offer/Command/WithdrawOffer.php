<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Command;

use App\Domain\Market\Offer\OfferId;

class WithdrawOffer
{
    private string $offerId;

    public function __construct(OfferId $offerId)
    {
        $this->offerId = $offerId->toString();
    }

    public function offerId(): OfferId
    {
        return OfferId::fromString($this->offerId);
    }
}
