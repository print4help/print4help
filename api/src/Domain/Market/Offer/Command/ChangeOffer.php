<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Command;

use App\Domain\Market\Offer\OfferId;

class ChangeOffer
{
    private string $offerId;
    private string $message;
    private int $quantity;

    public function __construct(
        OfferId $offerId,
        string $message,
        int $quantity
    ) {
        $this->offerId = $offerId->toString();
        $this->message = $message;
        $this->quantity = $quantity;
    }

    public function offerId(): OfferId
    {
        return OfferId::fromString($this->offerId);
    }

    public function message(): string
    {
        return $this->message;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }
}
