<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Command;

use App\Domain\Account\UserId;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\MessageId;

class SendMessage
{
    private string $chatId;
    private string $messageId;
    private string $authorId;
    private string $body;
    private ?string $subject;

    public function __construct(
        ChatId $chatId,
        MessageId $messageId,
        UserId $authorId,
        string $body,
        ?string $subject
    ) {
        $this->chatId = $chatId->toString();
        $this->messageId = $messageId->toString();
        $this->authorId = $authorId->toString();
        $this->body = $body;
        $this->subject = $subject;
    }

    public function chatId(): ChatId
    {
        return ChatId::fromString($this->chatId);
    }

    public function messageId(): MessageId
    {
        return MessageId::fromString($this->messageId);
    }

    public function authorId(): UserId
    {
        return UserId::fromString($this->authorId);
    }

    public function body(): string
    {
        return $this->body;
    }

    public function subject(): ?string
    {
        return $this->subject;
    }
}
