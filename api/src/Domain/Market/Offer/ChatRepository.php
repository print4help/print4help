<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer;

use App\Domain\Market\Offer\Exception\ChatNotFound;

interface ChatRepository
{
    /**
     * @throws ChatNotFound
     */
    public function get(ChatId $chatId): Chat;

    public function save(Chat $chat): void;
}
