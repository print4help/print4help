<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Listener;

use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Offer\Event\OfferRejected;
use App\Domain\Market\Offer\OfferRepository;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class RemoveOfferIfRejected implements Listener
{
    public function __construct(
        private OfferRepository $offerRepository,
        private InquiryRepository $inquiryRepository
    ) {
    }

    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof OfferRejected) {
            return;
        }

        $rejectedOffer = $this->offerRepository->get($event->offerId());
        $inquiry = $this->inquiryRepository->get($rejectedOffer->inquiryId());

        $inquiry->removeOffer($rejectedOffer->id());
        $this->inquiryRepository->save($inquiry);
    }
}
