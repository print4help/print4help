<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Listener;

use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\Command\CreateChat;
use App\Domain\Market\Offer\Event\OfferCreated;
use App\Domain\Market\Piece\PieceRepository;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CreateChatIfOfferIsCreated implements Listener
{
    public function __construct(
        private MessageBusInterface $bus,
        private TranslatorInterface $translator,
        private PieceRepository $pieceRepository,
        private InquiryRepository $inquiryRepository
    ) {
    }

    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof OfferCreated) {
            return;
        }

        $createChatCommand = new CreateChat(
            ChatId::create(),
            $event->offerId(),
            $this->generateSubject($event),
            $event->message()
        );

        $this->bus->dispatch($createChatCommand);
    }

    private function generateSubject(OfferCreated $event): string
    {
        $piece = $this->pieceRepository->get($event->pieceId());
        $inquiry = $this->inquiryRepository->get($event->inquiryId());

        return $this->translator->trans(
            'message.subject',
            [
                '%piece.name%' => $piece->name(),
                '%piece.articleNumber%' => $piece->articleNumber()->toString(),
                '%inquiry.inquiryNumber%' => $inquiry->id()->toString(),
            ],
            'offer'
        );
    }
}
