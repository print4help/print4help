<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer\Listener;

use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Offer\Event\OfferAccepted;
use App\Domain\Market\Offer\OfferRepository;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class RejectOtherIfOfferAccepted implements Listener
{
    public function __construct(
        private OfferRepository $offerRepository,
        private InquiryRepository $inquiryRepository
    ) {
    }

    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof OfferAccepted) {
            return;
        }

        $acceptedOffer = $this->offerRepository->get($event->offerId());
        $inquiry = $this->inquiryRepository->get($acceptedOffer->inquiryId());

        foreach ($inquiry->offerIds() as $offerId) {
            if ($offerId->equals($event->offerId())) {
                continue;
            }

            $offer = $this->offerRepository->get($offerId);
            $offer->reject();
            // todo: send notification to other makers who made an offer
            $this->offerRepository->save($offer);
        }
    }
}
