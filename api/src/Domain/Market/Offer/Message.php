<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer;

use App\Domain\Account\UserId;
use App\Domain\Market\Offer\Event\MessageCreated;
use App\Domain\Market\Offer\Event\MessageUpdated;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateRoot;
use Webmozart\Assert\Assert;

class Message extends AggregateRoot
{
    private MessageId $id;
    private UserId $authorId;
    private ChatId $chatId;
    private ?string $subject;
    private string $body;
    private ?DateTimeImmutable $createdAt;
    private ?DateTimeImmutable $updatedAt;

    public function aggregateRootId(): string
    {
        return $this->id->toString();
    }

    public function id(): MessageId
    {
        return $this->id;
    }

    public function chatId(): ChatId
    {
        return $this->chatId;
    }

    public function authorId(): UserId
    {
        return $this->authorId;
    }

    public static function create(
        MessageId $id,
        ChatId $chatId,
        UserId $authorId,
        string $body,
        ?string $subject = null
    ): self {
        Assert::maxLength($body, 1000);

        if (is_string($subject)) {
            Assert::maxLength($subject, 100);
        }

        $self = new self();
        $self->apply(MessageCreated::raise(
            $id,
            $chatId,
            $authorId,
            $body,
            $subject
        ));

        return $self;
    }

    public function applyMessageCreated(MessageCreated $event): void
    {
        $this->id = $event->messageId();
        $this->chatId = $event->chatId();
        $this->authorId = $event->authorId();
        $this->body = $event->body();
        $this->subject = $event->subject();
        $this->createdAt = $event->createdAt();
    }

    public function update(string $body, ?string $subject): void
    {
        Assert::maxLength($body, 1000);

        if (is_string($subject)) {
            Assert::maxLength($subject, 100);
        }

        $this->apply(MessageUpdated::raise(
            $this->id,
            $body,
            $subject
        ));
    }

    public function applyMessageUpdated(MessageUpdated $event): void
    {
        $this->body = $event->body();
        $this->subject = $event->subject();
        $this->updatedAt = $event->updatedAt();
    }
}
