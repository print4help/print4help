<?php

declare(strict_types=1);

namespace App\Domain\Market\Offer;

use App\Domain\Market\Offer\Exception\OfferNotFound;

interface OfferRepository
{
    /**
     * @throws OfferNotFound
     */
    public function get(OfferId $offerId): Offer;

    public function save(Offer $offer): void;
}
