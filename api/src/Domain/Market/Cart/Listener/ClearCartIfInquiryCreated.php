<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Listener;

use App\Domain\Account\Repository\UserRepository;
use App\Domain\Market\Cart\CartId;
use App\Domain\Market\Cart\CartRepository;
use App\Domain\Market\Cart\Exception\CartNotFoundInUser;
use App\Domain\Market\Inquiry\Event\InquiryCreated;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class ClearCartIfInquiryCreated implements Listener
{
    public function __construct(
        private UserRepository $userRepository,
        private CartRepository $cartRepository
    ) {
    }

    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof InquiryCreated) {
            return;
        }

        $user = $this->userRepository->get($event->userId());

        $cartId = $user->getCartId();
        if (!$cartId instanceof CartId) {
            throw new CartNotFoundInUser($user->getId());
        }

        $cart = $this->cartRepository->get($cartId);

        $cart->removePiece($event->pieceId());
        $this->cartRepository->save($cart);
    }
}
