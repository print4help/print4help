<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Listener;

use App\Domain\Account\Repository\UserRepository;
use App\Domain\Market\Cart\Event\CartCreated;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class AddCartToUserIfCartCreated implements Listener
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof CartCreated) {
            return;
        }

        $user = $this->userRepository->get($event->userId());
        $user->setCartId($event->cartId());
        $this->userRepository->save($user);
    }
}
