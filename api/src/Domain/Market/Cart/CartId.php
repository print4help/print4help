<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart;

use App\Domain\UuidBehaviour;

class CartId
{
    use UuidBehaviour;
}
