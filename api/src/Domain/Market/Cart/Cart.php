<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart;

use App\Domain\Account\UserId;
use App\Domain\Market\Cart\Event\CartCreated;
use App\Domain\Market\Cart\Event\CartItemMetadataUpdated;
use App\Domain\Market\Cart\Event\CartSubmitted;
use App\Domain\Market\Cart\Event\PieceAdded;
use App\Domain\Market\Cart\Event\PieceQuantityChanged;
use App\Domain\Market\Cart\Event\PieceRemoved;
use App\Domain\Market\Cart\Exception\PieceNotInCart;
use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateRoot;
use Webmozart\Assert\Assert;

class Cart extends AggregateRoot
{
    private CartId $id;
    private UserId $userId;
    /**
     * @var array<string, CartItem>
     */
    private array $cartItems;
    private DateTimeImmutable $createdAt;
    private DateTimeImmutable $updatedAt;
    private DateTimeImmutable $submittedAt;

    public function aggregateRootId(): string
    {
        return $this->id->toString();
    }

    public function id(): CartId
    {
        return $this->id;
    }

    public function userId(): UserId
    {
        return $this->userId;
    }

    /**
     * @return CartItem[]
     */
    public function cartItems(): array
    {
        return $this->cartItems;
    }

    public static function create(UserId $userId, CartId $cartId): self
    {
        $self = new self();
        $self->apply(CartCreated::raise($userId, $cartId));
        return $self;
    }

    protected function applyCartCreated(CartCreated $event): void
    {
        $this->id = $event->cartId();
        $this->userId = $event->userId();
        $this->createdAt = $event->createdAt();
        $this->cartItems = [];
    }

    public function hasPiece(PieceId $pieceId): bool
    {
        return array_key_exists($pieceId->toString(), $this->cartItems);
    }

    public function addPiece(PieceId $pieceId, int $quantity): void
    {
        $this->apply(PieceAdded::raise(
            $this->id,
            $pieceId,
            $quantity
        ));
    }

    protected function applyPieceAdded(PieceAdded $event): void
    {
        $this->cartItems[$event->pieceId()->toString()] = new CartItem(
            $event->pieceId(),
            $event->quantity(),
            $event->addedAt()
        );
    }

    private function getCartItemByPieceId(PieceId $pieceId): CartItem
    {
        if (array_key_exists($pieceId->toString(), $this->cartItems) === false) {
            throw new PieceNotInCart($pieceId);
        }

        return $this->cartItems[$pieceId->toString()];
    }

    public function removePiece(PieceId $pieceId): void
    {
        $this->apply(PieceRemoved::raise(
            $this->id(),
            $pieceId,
        ));
    }

    protected function applyPieceRemoved(PieceRemoved $event): void
    {
        $pieceId = $event->pieceId()->toString();
        unset($this->cartItems[$pieceId]);
    }

    public function changePieceQuantity(PieceId $pieceId, int $quantity): void
    {
        if (!$this->hasPiece($pieceId)) {
            $this->apply(PieceAdded::raise($this->id(), $pieceId, $quantity));
            return;
        }

        if ($quantity === 0) {
            $this->apply(PieceRemoved::raise($this->id(), $pieceId));
            return;
        }

        $this->apply(PieceQuantityChanged::raise(
            $this->id(),
            $pieceId,
            $quantity
        ));
    }

    protected function applyPieceQuantityChanged(PieceQuantityChanged $event): void
    {
        $cartItem = $this->getCartItemByPieceId($event->pieceId());
        $cartItem->changeQuantity($event->quantity());
    }

    /**
     * @param array<array-key, mixed> $requirements
     */
    public function updateCartItemMetadata(PieceId $pieceId, string $purpose, array $requirements): void
    {
        if (!$this->hasPiece($pieceId)) {
            throw new PieceNotInCart($pieceId);
        }

        Assert::maxLength($purpose, 1000);
        Assert::notEmpty($requirements);
        Assert::allString($requirements);
        Assert::allValidArrayKey($requirements);
        Assert::allMaxLength($requirements, 1000);

        $this->apply(CartItemMetadataUpdated::raise(
            $this->id(),
            $pieceId,
            $purpose,
            $requirements
        ));
    }

    public function applyCartItemMetadataUpdated(CartItemMetadataUpdated $event): void
    {
        $cartItem = $this->getCartItemByPieceId($event->pieceId());
        $cartItem->updateMetadata($event->purpose(), $event->requirements());
    }

    public function submit(): void
    {
        $this->apply(
            CartSubmitted::raise($this->id)
        );
    }

    protected function applyCartSubmitted(CartSubmitted $event): void
    {
        $this->submittedAt = $event->submittedAt();
    }
}
