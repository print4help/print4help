<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Piece\PieceId;
use Throwable;

final class PieceNotInCart extends DomainException
{
    public function __construct(PieceId $pieceId, Throwable $previous = null)
    {
        parent::__construct(sprintf('Piece with ID [%s] is not in Cart', $pieceId->toString()), 0, $previous);
    }
}
