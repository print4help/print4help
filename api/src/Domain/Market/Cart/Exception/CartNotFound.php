<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Cart\CartId;
use Throwable;

final class CartNotFound extends DomainException
{
    public function __construct(CartId $cartId, Throwable $previous = null)
    {
        parent::__construct(sprintf('Cart with ID [%s] not found', $cartId->toString()), 0, $previous);
    }
}
