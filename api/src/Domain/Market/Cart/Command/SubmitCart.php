<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Command;

use App\Domain\Account\UserId;

class SubmitCart
{
    private string $userId;

    public function __construct(UserId $userId)
    {
        $this->userId = $userId->toString();
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->userId);
    }
}
