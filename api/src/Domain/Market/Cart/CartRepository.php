<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart;

use App\Domain\Market\Cart\Exception\CartNotFound;

interface CartRepository
{
    /**
     * @throws CartNotFound
     */
    public function get(CartId $cartId): Cart;
    public function save(Cart $cart): void;
}
