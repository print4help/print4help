<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Cart\CartId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class CartSubmitted extends AggregateChanged
{
    public static function raise(
        CartId $id
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'submittedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function cartId(): CartId
    {
        return CartId::fromString($this->aggregateId);
    }

    public function submittedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['submittedAt']);
    }
}
