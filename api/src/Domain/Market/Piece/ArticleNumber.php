<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

use App\Domain\Market\Piece\Exception\ArticleNumberNotValid;

class ArticleNumber
{
    private string $value;

    private function __construct(string $value)
    {
        if (ArticleNumberGenerator::isValid($value) === false) {
            throw new ArticleNumberNotValid($value);
        }
        $this->value = $value;
    }

    public static function create(): self
    {
        return new self(ArticleNumberGenerator::generate());
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function equals(self $other): bool
    {
        return $this->value === $other->toString();
    }

    // needs to be here since UnitOfWork assumes that Types as Identifiers has the magic method see: https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/cookbook/custom-mapping-types.html
    public function __toString(): string
    {
        return $this->value;
    }
}
