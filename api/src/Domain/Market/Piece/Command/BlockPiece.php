<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece\Command;

use App\Domain\Market\Piece\PieceId;

class BlockPiece
{
    private string $pieceId;
    private ?string $message;

    public function __construct(PieceId $pieceId, ?string $message)
    {
        $this->pieceId = $pieceId->toString();
        $this->message = $message;
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->pieceId);
    }

    public function message(): ?string
    {
        return $this->message;
    }
}
