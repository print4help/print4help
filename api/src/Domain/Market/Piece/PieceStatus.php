<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

use Patchlevel\Enum\ExtendedEnum;

/**
 * @psalm-immutable
 */
final class PieceStatus extends ExtendedEnum
{
    private const DRAFT = 'draft';
    private const IN_REVIEW = 'in-review';
    private const APPROVED = 'approved';
    private const REJECTED = 'rejected';
    private const BLOCKED = 'blocked';

    public static function draft(): self
    {
        return self::get(self::DRAFT);
    }

    public static function inReview(): self
    {
        return self::get(self::IN_REVIEW);
    }

    public static function approved(): self
    {
        return self::get(self::APPROVED);
    }

    public static function rejected(): self
    {
        return self::get(self::REJECTED);
    }

    public static function blocked(): self
    {
        return self::get(self::BLOCKED);
    }
}
