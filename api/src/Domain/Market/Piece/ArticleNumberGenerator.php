<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

use InvalidArgumentException;
use Webmozart\Assert\Assert;

class ArticleNumberGenerator
{
    private static int $counter = 0;
    private static bool $dummy = false;

    public static function generate(): string
    {
        if (!self::$dummy) {
            return self::generateRandom();
        }

        return self::generateDummy();
    }

    private static function generateRandom(): string
    {
        $alphabet = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
        /** @psalm-suppress ArgumentTypeCoercion */
        $startKeys = array_rand($alphabet, 3);
        /** @psalm-suppress ArgumentTypeCoercion */
        $endKeys = array_rand($alphabet, 3);

        $start = array_map(static fn (int $key): string => $alphabet[$key], $startKeys);
        $end = array_map(static fn (int $key): string => $alphabet[$key], $endKeys);

        return sprintf(
            '%s-%s',
            implode('', $start),
            implode('', $end)
        );
    }

    private static function generateDummy(): string
    {
        self::$counter++;

        $start = 'ZZZZZ';
        for ($i = 0; $i < self::$counter; $i++) {
            /** @psalm-suppress StringIncrement */
            $start++;
        }

        return sprintf(
            '%s-%s',
            substr($start, 0, 3),
            substr($start, 3, 3)
        );
    }

    public static function isValid(string $value): bool
    {
        try {
            Assert::length($value, 7);
            Assert::regex($value, '/[A-Z0-9]{3}\-[A-Z0-9]{3}/');
        } catch (InvalidArgumentException $exception) {
            return false;
        }

        return true;
    }

    public static function dummy(): void
    {
        self::$dummy = true;
        self::$counter = 0;
    }

    public static function deactivateDummy(): void
    {
        self::$dummy = false;
    }
}
