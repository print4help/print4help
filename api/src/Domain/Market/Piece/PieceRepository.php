<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

use App\Domain\Market\Piece\Exception\PieceNotFound;

interface PieceRepository
{
    /**
     * @throws PieceNotFound
     */
    public function get(PieceId $pieceId): Piece;
    public function save(Piece $piece): void;
}
