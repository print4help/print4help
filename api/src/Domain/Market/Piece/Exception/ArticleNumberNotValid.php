<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece\Exception;

use App\Domain\DomainException;
use Throwable;

final class ArticleNumberNotValid extends DomainException
{
    public function __construct(string $articleNumber, Throwable $previous = null)
    {
        parent::__construct(sprintf('ArticleNumber [%s] is not a valid ArticleNumber', $articleNumber), 0, $previous);
    }
}
