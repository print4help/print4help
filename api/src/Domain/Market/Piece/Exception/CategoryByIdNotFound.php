<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece\Exception;

use App\Domain\DomainException;

class CategoryByIdNotFound extends DomainException
{
    public function __construct(string $categoryId)
    {
        parent::__construct(sprintf('Category with id [%s] not found', $categoryId));
    }
}
