<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece\Exception;

use App\Domain\DomainException;
use App\Domain\Market\Piece\PieceId;
use Throwable;

final class PieceNotFound extends DomainException
{
    public function __construct(PieceId $pieceId, Throwable $previous = null)
    {
        parent::__construct(
            sprintf('Piece with ID [%s] not found', $pieceId->toString()),
            0,
            $previous
        );
    }
}
