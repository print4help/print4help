<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

use Patchlevel\Enum\Enum;

/**
 * @psalm-immutable
 */
class CategoryId extends Enum
{
    public const ART_DESIGN = 'art-design';
    public const ART_DESIGN_SCUPLTURES = 'art-design.sculptures';
    public const ART_DESIGN_WALL_MOUNTED = 'art-design.wall-mounted';
    public const COSTUMES_ACCESSORIES = 'costumes-accessories';
    public const COSTUMES_ACCESSORIES_MASKS = 'costumes-accessories.masks';
    public const COSTUMES_ACCESSORIES_PROPS = 'costumes-accessories.props';
    public const COSTUMES_ACCESSORIES_OTHER = 'costumes-accessories.other';
    public const FASHION = 'fashion';
    public const FASHION_MEN = 'fashion.men';
    public const FASHION_WOMEN = 'fashion.women';
    public const FASHION_ACCESSORIES = 'fashion.accessories';
    public const FASHION_BRACELETS = 'fashion.bracelets';
    public const FASHION_JEWELERY = 'fashion.jewelery';
    public const FASHION_RINGS = 'fashion.rings';
    public const FASHION_OTHER = 'fashion.other';
    public const GADGET = 'gadget';
    public const GADGET_AUDIO = 'gadget.audio';
    public const GADGET_COMPUTER = 'gadget.computer';
    public const GADGET_PHOTO_VIDEO = 'gadget.photo-video';
    public const GADGET_PORTABLES = 'gadget.portables';
    public const GADGET_MOBILE = 'gadget.mobile';
    public const GADGET_OTHER = 'gadget.other';
    public const HEALTHCARE = 'healthcare';
    public const HEALTHCARE_HOME_MEDICAL_TOOLS = 'healthcare.home-medical-tools';
    public const HEALTHCARE_MEDICAL_TOOLS = 'healthcare.medical-tools';
    public const HOBBY_MAKERS = 'hobby-makers';
    public const HOBBY_MAKERS_AUTOMOTIVE = 'hobby-makers.automotive';
    public const HOBBY_MAKERS_ELECTRONICS = 'hobby-makers.electronics';
    public const HOBBY_MAKERS_MECHANICAL_PARTS = 'hobby-makers.mechanical-parts';
    public const HOBBY_MAKERS_MUSIC = 'hobby-makers.music';
    public const HOBBY_MAKERS_ORGANIZERS = 'hobby-makers.organizers';
    public const HOBBY_MAKERS_RC_ROBOTICS = 'hobby-makers.rc-robotics';
    public const HOBBY_MAKERS_TOOLS = 'hobby-makers.tools';
    public const HOBBY_MAKERS_OTHER = 'hobby-makers.other';
    public const HOUSEHOLD = 'household';
    public const HOUSEHOLD_BATHROOM = 'household.bathroom';
    public const HOUSEHOLD_BEDROOM = 'household.bedroom';
    public const HOUSEHOLD_HOME_DECOR = 'household.home-decor';
    public const HOUSEHOLD_KITCHEN = 'household.kitchen';
    public const HOUSEHOLD_LIVING_ROOM = 'household.living-room';
    public const HOUSEHOLD_OFFICE = 'household.office';
    public const HOUSEHOLD_OUTDOOR_GARDEN = 'household.outdoor-garden';
    public const HOUSEHOLD_OTHER = 'household.other';
    public const HOUSEHOLD_PETS = 'household.pets';
    public const LEARNING = 'learning';
    public const LEARNING_CHEMISTRY = 'learning.chemistry';
    public const LEARNING_BIOLOGY = 'learning.biology';
    public const LEARNING_PHYSICS = 'learning.physics';
    public const LEARNING_ASTRONOMY = 'learning.astronomy';
    public const LEARNING_MATHS = 'learning.maths';
    public const LEARNING_ENGINEERING = 'learning.engineering';
    public const LEARNING_HAPTIC_MODELS = 'learning.haptic-models';
    public const LEARNING_OTHER = 'learning.other';
    public const MODELS = 'models';
    public const MODELS_ANIMALS = 'models.animals';
    public const MODELS_BUILDINGS_STRUCTURES = 'models.buildings-structures';
    public const MODELS_CREATURES = 'models.creatures';
    public const MODELS_FOOD_DRINK = 'models.food-drink';
    public const MODELS_FURNITURE = 'models.furniture';
    public const MODELS_ROBOTS = 'models.robots';
    public const MODELS_PEOPLE = 'models.people';
    public const MODELS_PROPS = 'models.props';
    public const MODELS_VEHICLES = 'models.vehicles';
    public const SEASONAL = 'seasonal';
    public const SEASONAL_AUTUMN_HALLOWEEN = 'seasonal.autumn-halloween';
    public const SEASONAL_SPRING = 'seasonal.spring-easter';
    public const SEASONAL_SUMMER = 'seasonal.summer';
    public const SEASONAL_WINTER_CHRISTMAS = 'seasonal.winter-christmas';
    public const SPORTS_OUTDOOR = 'sports-outdoor';
    public const SPORTS_OUTDOOR_INDOOR_SPORTS = 'sports-outdoor.indoor-sports';
    public const SPORTS_OUTDOOR_OUTDOOR_SPORTS = 'sports-outdoor.outdoor-sports';
    public const SPORTS_OUTDOOR_OTHER = 'sports-outdoor.other';
    public const SPORTS_OUTDOOR_WINTER = 'sports-outdoor.winter';
    public const SPORTS_OUTDOOR_CAMPING = 'sports-outdoor.camping';
    public const TOOLS = 'tools';
    public const TOOLS_HAND = 'tools.hand';
    public const TOOLS_MACHINE = 'tools.machine';
    public const TOOLS_HOLDERS_BOXES = 'tools.holders-boxes';
    public const TOYS_GAMES = 'toys-games';
    public const TOYS_GAMES_ACTION_FIGURES = 'toys-games.action-figures';
    public const TOYS_GAMES_STATUES = 'toys-games.statues';
    public const TOYS_GAMES_BOARD_GAMES = 'toys-games.board-games';
    public const TOYS_GAMES_BUILDING_TOYS = 'toys-games.building-toys';
    public const TOYS_GAMES_OUTDOOR_TOYS = 'toys-games.outdoor-toys';
    public const TOYS_GAMES_PUZZLES = 'toys-games.puzzles';
    public const TOYS_GAMES_RPG_FIGURES = 'toys-games.rpg-figures';
    public const TOYS_GAMES_VEHICLES = 'toys-games.vehicles';
    public const TOYS_GAMES_OTHER = 'toys-games.other';
    public const WORLD_SCANS = 'world-scans';
    public const WORLD_SCANS_ARCHITECTURE = 'world-scans.architecture';
    public const WORLD_SCANS_HISTORICAL = 'world-scans.historical';
    public const WORLD_SCANS_PEOPLE = 'world-scans.people';

    public static function artDesign(): self
    {
        return self::get(self::ART_DESIGN);
    }

    public static function artDesign_Sculptures(): self
    {
        return self::get(self::ART_DESIGN_SCUPLTURES);
    }

    public static function artDesign_Wall_Mounted(): self
    {
        return self::get(self::ART_DESIGN_WALL_MOUNTED);
    }

    public static function costumesAccessories(): self
    {
        return self::get(self::COSTUMES_ACCESSORIES);
    }

    public static function costumesAccessories_Masks(): self
    {
        return self::get(self::COSTUMES_ACCESSORIES_MASKS);
    }

    public static function costumesAccessories_Props(): self
    {
        return self::get(self::COSTUMES_ACCESSORIES_PROPS);
    }

    public static function costumesAccessories_Other(): self
    {
        return self::get(self::COSTUMES_ACCESSORIES_OTHER);
    }

    public static function fashion(): self
    {
        return self::get(self::FASHION);
    }

    public static function fashion_Men(): self
    {
        return self::get(self::FASHION_MEN);
    }

    public static function fashion_Women(): self
    {
        return self::get(self::FASHION_WOMEN);
    }

    public static function fashion_Other(): self
    {
        return self::get(self::FASHION_OTHER);
    }

    public static function fashion_Accessories(): self
    {
        return self::get(self::FASHION_ACCESSORIES);
    }

    public static function fashion_Bracelets(): self
    {
        return self::get(self::FASHION_BRACELETS);
    }

    public static function fashion_Jewelery(): self
    {
        return self::get(self::FASHION_JEWELERY);
    }

    public static function fashion_Rings(): self
    {
        return self::get(self::FASHION_RINGS);
    }

    public static function gadget(): self
    {
        return self::get(self::GADGET);
    }

    public static function gadget_Audio(): self
    {
        return self::get(self::GADGET_AUDIO);
    }

    public static function gadget_Computer(): self
    {
        return self::get(self::GADGET_COMPUTER);
    }

    public static function gadget_PhotoVideo(): self
    {
        return self::get(self::GADGET_PHOTO_VIDEO);
    }

    public static function gadget_Portables(): self
    {
        return self::get(self::GADGET_PORTABLES);
    }

    public static function gadget_Mobile(): self
    {
        return self::get(self::GADGET_MOBILE);
    }

    public static function gadget_Other(): self
    {
        return self::get(self::GADGET_OTHER);
    }

    public static function healthcare(): self
    {
        return self::get(self::HEALTHCARE);
    }

    public static function healthcare_HomeMedicalTools(): self
    {
        return self::get(self::HEALTHCARE_HOME_MEDICAL_TOOLS);
    }

    public static function healthcare_MedicalTools(): self
    {
        return self::get(self::HEALTHCARE_MEDICAL_TOOLS);
    }

    public static function hobbyMakers(): self
    {
        return self::get(self::HOBBY_MAKERS);
    }

    public static function hobbyMakers_Automotive(): self
    {
        return self::get(self::HOBBY_MAKERS_AUTOMOTIVE);
    }

    public static function hobbyMakers_Electronics(): self
    {
        return self::get(self::HOBBY_MAKERS_ELECTRONICS);
    }

    public static function hobbyMakers_MechanicalParts(): self
    {
        return self::get(self::HOBBY_MAKERS_MECHANICAL_PARTS);
    }

    public static function hobbyMakers_Music(): self
    {
        return self::get(self::HOBBY_MAKERS_MUSIC);
    }

    public static function hobbyMakers_Organizers(): self
    {
        return self::get(self::HOBBY_MAKERS_ORGANIZERS);
    }

    public static function hobbyMakers_RcRobotics(): self
    {
        return self::get(self::HOBBY_MAKERS_RC_ROBOTICS);
    }

    public static function hobbyMakers_Tools(): self
    {
        return self::get(self::HOBBY_MAKERS_TOOLS);
    }

    public static function hobbyMakers_Other(): self
    {
        return self::get(self::HOBBY_MAKERS_OTHER);
    }

    public static function household(): self
    {
        return self::get(self::HOUSEHOLD);
    }

    public static function household_Bathroom(): self
    {
        return self::get(self::HOUSEHOLD_BATHROOM);
    }

    public static function household_Bedroom(): self
    {
        return self::get(self::HOUSEHOLD_BEDROOM);
    }

    public static function household_HomeDecor(): self
    {
        return self::get(self::HOUSEHOLD_HOME_DECOR);
    }

    public static function household_Kitchen(): self
    {
        return self::get(self::HOUSEHOLD_KITCHEN);
    }

    public static function household_LivingRoom(): self
    {
        return self::get(self::HOUSEHOLD_LIVING_ROOM);
    }

    public static function household_Office(): self
    {
        return self::get(self::HOUSEHOLD_OFFICE);
    }

    public static function household_OutdoorGarden(): self
    {
        return self::get(self::HOUSEHOLD_OUTDOOR_GARDEN);
    }

    public static function household_Other(): self
    {
        return self::get(self::HOUSEHOLD_OTHER);
    }

    public static function household_Pets(): self
    {
        return self::get(self::HOUSEHOLD_PETS);
    }

    public static function learning(): self
    {
        return self::get(self::LEARNING);
    }

    public static function learning_Chemistry(): self
    {
        return self::get(self::LEARNING_CHEMISTRY);
    }

    public static function learning_Physics(): self
    {
        return self::get(self::LEARNING_PHYSICS);
    }

    public static function learning_Biology(): self
    {
        return self::get(self::LEARNING_BIOLOGY);
    }

    public static function learning_Astronomy(): self
    {
        return self::get(self::LEARNING_ASTRONOMY);
    }

    public static function learning_Maths(): self
    {
        return self::get(self::LEARNING_MATHS);
    }

    public static function learning_Engineering(): self
    {
        return self::get(self::LEARNING_ENGINEERING);
    }

    public static function learning_HapticModels(): self
    {
        return self::get(self::LEARNING_HAPTIC_MODELS);
    }

    public static function learning_Other(): self
    {
        return self::get(self::LEARNING_OTHER);
    }

    public static function models(): self
    {
        return self::get(self::MODELS);
    }

    public static function models_Animals(): self
    {
        return self::get(self::MODELS_ANIMALS);
    }

    public static function models_BuildingStructures(): self
    {
        return self::get(self::MODELS_BUILDINGS_STRUCTURES);
    }

    public static function models_Creatures(): self
    {
        return self::get(self::MODELS_CREATURES);
    }

    public static function models_FoodDrink(): self
    {
        return self::get(self::MODELS_FOOD_DRINK);
    }

    public static function models_Furniture(): self
    {
        return self::get(self::MODELS_FURNITURE);
    }

    public static function models_Robots(): self
    {
        return self::get(self::MODELS_ROBOTS);
    }

    public static function models_People(): self
    {
        return self::get(self::MODELS_PEOPLE);
    }

    public static function models_Props(): self
    {
        return self::get(self::MODELS_PROPS);
    }

    public static function models_Vehicles(): self
    {
        return self::get(self::MODELS_VEHICLES);
    }

    public static function seasonal(): self
    {
        return self::get(self::SEASONAL);
    }

    public static function seasonal_AutumnHalloween(): self
    {
        return self::get(self::SEASONAL_AUTUMN_HALLOWEEN);
    }

    public static function seasonal_Spring(): self
    {
        return self::get(self::SEASONAL_SPRING);
    }

    public static function seasonal_Summer(): self
    {
        return self::get(self::SEASONAL_SUMMER);
    }

    public static function seasonal_WinterChristmas(): self
    {
        return self::get(self::SEASONAL_WINTER_CHRISTMAS);
    }

    public static function sportsOutdoor(): self
    {
        return self::get(self::SPORTS_OUTDOOR);
    }

    public static function sportsOutdoor_IndoorSports(): self
    {
        return self::get(self::SPORTS_OUTDOOR_INDOOR_SPORTS);
    }

    public static function sportsOutdoor_OutdoorSports(): self
    {
        return self::get(self::SPORTS_OUTDOOR_OUTDOOR_SPORTS);
    }

    public static function sportsOutdoor_Other(): self
    {
        return self::get(self::SPORTS_OUTDOOR_OTHER);
    }

    public static function sportsOutdoor_Winter(): self
    {
        return self::get(self::SPORTS_OUTDOOR_WINTER);
    }

    public static function sportsOutdoor_Camping(): self
    {
        return self::get(self::SPORTS_OUTDOOR_CAMPING);
    }

    public static function tools(): self
    {
        return self::get(self::TOOLS);
    }

    public static function tools_Hand(): self
    {
        return self::get(self::TOOLS_HAND);
    }

    public static function tools_Machine(): self
    {
        return self::get(self::TOOLS_MACHINE);
    }

    public static function tools_HoldersBoxes(): self
    {
        return self::get(self::TOOLS_HOLDERS_BOXES);
    }

    public static function toysGames(): self
    {
        return self::get(self::TOYS_GAMES);
    }

    public static function toysGames_ActionFigures(): self
    {
        return self::get(self::TOYS_GAMES_ACTION_FIGURES);
    }

    public static function toysGames_Statues(): self
    {
        return self::get(self::TOYS_GAMES_STATUES);
    }

    public static function toysGames_BoardGames(): self
    {
        return self::get(self::TOYS_GAMES_BOARD_GAMES);
    }

    public static function toysGames_BuildingToys(): self
    {
        return self::get(self::TOYS_GAMES_BUILDING_TOYS);
    }

    public static function toysGames_OutdoorToys(): self
    {
        return self::get(self::TOYS_GAMES_OUTDOOR_TOYS);
    }

    public static function toysGames_Puzzles(): self
    {
        return self::get(self::TOYS_GAMES_PUZZLES);
    }

    public static function toysGames_RgpFigures(): self
    {
        return self::get(self::TOYS_GAMES_RPG_FIGURES);
    }

    public static function toysGames_Vehicles(): self
    {
        return self::get(self::TOYS_GAMES_VEHICLES);
    }

    public static function toysGames_Other(): self
    {
        return self::get(self::TOYS_GAMES_OTHER);
    }

    public static function worldScans(): self
    {
        return self::get(self::WORLD_SCANS);
    }

    public static function worldScans_Architecture(): self
    {
        return self::get(self::WORLD_SCANS_ARCHITECTURE);
    }

    public static function worldScans_Historical(): self
    {
        return self::get(self::WORLD_SCANS_HISTORICAL);
    }

    public static function worldScans_People(): self
    {
        return self::get(self::WORLD_SCANS_PEOPLE);
    }
}
