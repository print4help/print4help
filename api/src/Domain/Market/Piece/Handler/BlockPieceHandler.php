<?php declare(strict_types=1);

namespace App\Domain\Market\Piece\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Piece\Command\BlockPiece;
use App\Domain\Market\Piece\PieceRepository;

class BlockPieceHandler implements CommandHandlerInterface
{
    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function __invoke(BlockPiece $command): void
    {
        $piece = $this->pieceRepository->get($command->pieceId());
        $piece->block($command->message());

        $this->pieceRepository->save($piece);
    }
}
