<?php declare(strict_types=1);

namespace App\Domain\Market\Piece\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Piece\Command\ApprovePiece;
use App\Domain\Market\Piece\PieceRepository;

class ApprovePieceHandler implements CommandHandlerInterface
{
    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function __invoke(ApprovePiece $command): void
    {
        $piece = $this->pieceRepository->get($command->pieceId());
        $piece->approve($command->message());

        $this->pieceRepository->save($piece);
    }
}
