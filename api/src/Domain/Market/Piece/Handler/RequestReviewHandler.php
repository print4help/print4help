<?php declare(strict_types=1);

namespace App\Domain\Market\Piece\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Piece\Command\RequestReview;
use App\Domain\Market\Piece\PieceRepository;

class RequestReviewHandler implements CommandHandlerInterface
{
    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function __invoke(RequestReview $command): void
    {
        $piece = $this->pieceRepository->get($command->pieceId());
        $piece->requestReview($command->message());

        $this->pieceRepository->save($piece);
    }
}
