<?php declare(strict_types=1);

namespace App\Domain\Market\Piece\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Piece\Command\RejectPiece;
use App\Domain\Market\Piece\PieceRepository;

class RejectPieceHandler implements CommandHandlerInterface
{
    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function __invoke(RejectPiece $command): void
    {
        $piece = $this->pieceRepository->get($command->pieceId());
        $piece->reject($command->message());

        $this->pieceRepository->save($piece);
    }
}
