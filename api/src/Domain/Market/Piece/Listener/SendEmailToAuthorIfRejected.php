<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece\Listener;

use App\Domain\Market\Piece\Event\PieceRejected;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class SendEmailToAuthorIfRejected implements Listener
{
    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof PieceRejected) {
            return;
        }

        // todo: send email to maker
    }
}
