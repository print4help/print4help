<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece\Listener;

use App\Domain\Market\Piece\Event\PieceReviewRequested;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class SendEmailToAdminIfReviewRequested implements Listener
{
    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof PieceReviewRequested) {
            return;
        }

        // todo: send email to admin
    }
}
