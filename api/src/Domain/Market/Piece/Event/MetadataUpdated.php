<?php declare(strict_types=1);

namespace App\Domain\Market\Piece\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Piece\CategoryId;
use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class MetadataUpdated extends AggregateChanged
{
    /**
     * @param array<int, CategoryId> $categoryIds
     */
    public static function raise(
        PieceId $id,
        string $name,
        ?string $summary,
        ?string $description,
        ?string $manufacturingNotes,
        ?string $licence,
        ?string $sourceUrl,
        array $categoryIds = []
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'name' => $name,
                'summary' => $summary,
                'description' => $description,
                'manufacturingNotes' => $manufacturingNotes,
                'licence' => $licence,
                'sourceUrl' => $sourceUrl,
                'categoryIds' => array_map(fn (CategoryId $categoryId) => $categoryId->toString(), $categoryIds),
                'updatedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->aggregateId);
    }

    public function name(): string
    {
        return $this->payload['name'];
    }

    public function summary(): ?string
    {
        return $this->payload['summary'];
    }

    public function description(): ?string
    {
        return $this->payload['description'];
    }

    public function manufacturingNotes(): ?string
    {
        return $this->payload['manufacturingNotes'];
    }

    public function licence(): ?string
    {
        return $this->payload['licence'];
    }

    public function sourceUrl(): ?string
    {
        return $this->payload['sourceUrl'];
    }

    /**
     * @return array<int, CategoryId>
     */
    public function categoryIds(): array
    {
        return array_map(fn (string $categoryId) => CategoryId::fromString($categoryId), $this->payload['categoryIds']);
    }

    public function updatedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['updatedAt']);
    }
}
