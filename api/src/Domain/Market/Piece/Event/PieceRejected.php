<?php declare(strict_types=1);

namespace App\Domain\Market\Piece\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Piece\PieceId;
use App\Domain\Market\Piece\PieceStatus;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class PieceRejected extends AggregateChanged
{
    public static function raise(PieceId $id, ?string $message): AggregateChanged
    {
        return self::occur(
            $id->toString(),
            [
                'status' => PieceStatus::rejected()->toString(),
                'statusMessage' => $message,
                'updatedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->aggregateId);
    }

    public function status(): PieceStatus
    {
        return PieceStatus::fromString($this->payload['status']);
    }

    public function statusMessage(): ?string
    {
        return $this->payload['statusMessage'];
    }

    public function updatedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['updatedAt']);
    }
}
