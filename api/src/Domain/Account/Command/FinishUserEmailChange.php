<?php

declare(strict_types=1);

namespace App\Domain\Account\Command;

use App\Domain\Email;

final class FinishUserEmailChange
{
    private string $emailChangeToken;
    private string $email;

    public function __construct(string $emailChangeToken, Email $email)
    {
        $this->emailChangeToken = $emailChangeToken;
        $this->email = $email->toString();
    }

    public function getEmailChangeToken(): string
    {
        return $this->emailChangeToken;
    }

    public function getEmail(): Email
    {
        return Email::fromString($this->email);
    }
}
