<?php

declare(strict_types=1);

namespace App\Domain\Account\Command;

final class FinishUserRegistration
{
    public function __construct(
        private string $registrationToken,
        private string $password
    ) {
    }

    public function getRegistrationToken(): string
    {
        return $this->registrationToken;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
