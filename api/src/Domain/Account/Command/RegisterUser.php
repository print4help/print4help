<?php

declare(strict_types=1);

namespace App\Domain\Account\Command;

use App\Domain\Account\UserId;
use App\Domain\Email;

class RegisterUser
{
    private string $userId;
    private string $email;
    private string $firstName;
    private string $lastName;

    public function __construct(UserId $userId, Email $email, string $firstName, string $lastName)
    {
        $this->userId = $userId->toString();
        $this->email = $email->toString();
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getUserId(): UserId
    {
        return UserId::fromString($this->userId);
    }

    public function getEmail(): Email
    {
        return Email::fromString($this->email);
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}
