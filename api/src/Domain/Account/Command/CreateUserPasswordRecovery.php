<?php

declare(strict_types=1);

namespace App\Domain\Account\Command;

use App\Domain\Email;

final class CreateUserPasswordRecovery
{
    private string $email;

    public function __construct(Email $email)
    {
        $this->email = $email->toString();
    }

    public function getEmail(): Email
    {
        return Email::fromString($this->email);
    }
}
