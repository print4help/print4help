<?php

declare(strict_types=1);

namespace App\Domain\Account\Command;

final class FinishUserPasswordRecovery
{
    public function __construct(
        private string $passwordRecoveryToken,
        private string $password
    ) {
    }

    public function getPasswordRecoveryToken(): string
    {
        return $this->passwordRecoveryToken;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
