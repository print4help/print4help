<?php

declare(strict_types=1);

namespace App\Domain\Account\Handler;

use App\Domain\Account\Command\RegisterUser;
use App\Domain\Account\Exception\UserByEmailAlreadyExists;
use App\Domain\Account\Exception\UserByEmailDoesNotExist;
use App\Domain\Account\Model\User;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;

class RegisterUserHandler implements CommandHandlerInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(RegisterUser $command): void
    {
        $email = $command->getEmail();

        try {
            $this->userRepository->getByEmail($email);

            throw new UserByEmailAlreadyExists($email);
        } catch (UserByEmailDoesNotExist $exception) {
            // happy path, no user should exist with this email
        }

        $user = User::create(
            $command->getUserId(),
            $command->getEmail(),
            $command->getFirstName(),
            $command->getLastName()
        );

        $user->createRegistration();

        $this->userRepository->save($user);

        // todo send email to user
    }
}
