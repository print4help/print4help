<?php

declare(strict_types=1);

namespace App\Domain\Account\Handler;

use App\Domain\Account\Command\CreateUserPasswordRecovery;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;

final class CreateUserPasswordRecoveryHandler implements CommandHandlerInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(CreateUserPasswordRecovery $command): void
    {
        $user = $this->userRepository->getByEmail($command->getEmail());

        $user->createPasswordRecovery();

        $this->userRepository->save($user);
    }
}
