<?php

declare(strict_types=1);

namespace App\Domain\Account\Handler;

use App\Domain\Account\Command\FinishUserRegistration;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;

final class FinishUserRegistrationHandler implements CommandHandlerInterface
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function __invoke(FinishUserRegistration $command): void
    {
        $userRegistration = $this->userRepository->getRegistrationByToken($command->getRegistrationToken());

        $user = $userRegistration->getUser();
        $user->finishRegistration($command->getPassword());

        $this->userRepository->removeRegistration($userRegistration);
        $this->userRepository->save($user);
    }
}
