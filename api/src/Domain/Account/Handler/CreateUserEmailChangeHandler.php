<?php

declare(strict_types=1);

namespace App\Domain\Account\Handler;

use App\Domain\Account\Command\CreateUserEmailChange;
use App\Domain\Account\Exception\UserByEmailAlreadyExists;
use App\Domain\Account\Exception\UserByEmailDoesNotExist;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;

final class CreateUserEmailChangeHandler implements CommandHandlerInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(CreateUserEmailChange $command): void
    {
        try {
            $this->userRepository->getByEmail($command->getEmail());

            throw new UserByEmailAlreadyExists($command->getEmail());
        } catch (UserByEmailDoesNotExist $userWithEmailDoesNotExist) {
            // happy path, no user should exist with this email
        }

        $user = $this->userRepository->get($command->getUserId());

        $user->createEmailChange($command->getEmail());

        $this->userRepository->save($user);

        // todo send email to new email address so user can confirm it
    }
}
