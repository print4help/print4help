<?php

declare(strict_types=1);

namespace App\Domain\Account\Handler;

use App\Domain\Account\Command\FinishUserPasswordRecovery;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;

final class FinishUserPasswordRecoveryHandler implements CommandHandlerInterface
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function __invoke(FinishUserPasswordRecovery $command): void
    {
        $userPasswordRecovery = $this->userRepository->getPasswordRecoveryByToken($command->getPasswordRecoveryToken());

        $user = $userPasswordRecovery->getUser();
        $user->finishPasswordRecovery($command->getPassword());

        $this->userRepository->removePasswordRecovery($userPasswordRecovery);
        $this->userRepository->save($user);
    }
}
