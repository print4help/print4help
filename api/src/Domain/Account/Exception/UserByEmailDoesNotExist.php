<?php

declare(strict_types=1);

namespace App\Domain\Account\Exception;

use App\Domain\DomainException;
use App\Domain\Email;
use Throwable;

final class UserByEmailDoesNotExist extends DomainException
{
    public function __construct(Email $email, Throwable $previous = null)
    {
        parent::__construct(sprintf('A User with Email [%s] does not exist', $email->toString()), 0, $previous);
    }
}
