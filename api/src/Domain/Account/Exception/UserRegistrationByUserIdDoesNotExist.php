<?php

declare(strict_types=1);

namespace App\Domain\Account\Exception;

use App\Domain\Account\UserId;
use App\Domain\DomainException;
use Throwable;

final class UserRegistrationByUserIdDoesNotExist extends DomainException
{
    public function __construct(UserId $id, Throwable $previous = null)
    {
        parent::__construct(sprintf('A UserRegistration with UserId [%s] does not exist', $id->toString()), 0, $previous);
    }
}
