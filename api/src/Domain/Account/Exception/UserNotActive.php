<?php

declare(strict_types=1);

namespace App\Domain\Account\Exception;

use App\Domain\Account\UserId;
use App\Domain\DomainException;
use Throwable;

final class UserNotActive extends DomainException
{
    public function __construct(UserId $userId, Throwable $previous = null)
    {
        parent::__construct(sprintf('User [%s] is active', $userId->toString()), 0, $previous);
    }
}
