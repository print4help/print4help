<?php

declare(strict_types=1);

namespace App\Domain\Account\Exception;

use App\Domain\DomainException;
use Throwable;

final class WrongUserPassword extends DomainException
{
    public function __construct(string $password, Throwable $previous = null)
    {
        parent::__construct(sprintf('The password [%s] is wrong.', $password), 0, $previous);
    }
}
