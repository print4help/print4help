<?php

declare(strict_types=1);

namespace App\Domain\Account\Repository;

use App\Domain\Account\Exception\UserByEmailDoesNotExist;
use App\Domain\Account\Exception\UserByUserIdDoesNotExist;
use App\Domain\Account\Exception\UserEmailChangeByEmailChangeTokenDoesNotExist;
use App\Domain\Account\Exception\UserEmailChangeByUserIdDoesNotExist;
use App\Domain\Account\Exception\UserPasswordRecoveryByPasswordRecoveryTokenDoesNotExist;
use App\Domain\Account\Exception\UserPasswordRecoveryByUserIdDoesNotExist;
use App\Domain\Account\Exception\UserRegistrationByRegistrationTokenDoesNotExist;
use App\Domain\Account\Exception\UserRegistrationByUserIdDoesNotExist;
use App\Domain\Account\Model\User;
use App\Domain\Account\Model\UserEmailChange;
use App\Domain\Account\Model\UserPasswordRecovery;
use App\Domain\Account\Model\UserRegistration;
use App\Domain\Account\UserId;
use App\Domain\Email;

interface UserRepository
{
    /**
     * @throws UserByUserIdDoesNotExist
     */
    public function get(UserId $userId): User;

    /**
     * @throws UserByEmailDoesNotExist
     */
    public function getByEmail(Email $email): User;

    /**
     * @throws UserRegistrationByRegistrationTokenDoesNotExist
     */
    public function getRegistrationByToken(string $registrationToken): UserRegistration;

    /**
     * @throws UserRegistrationByUserIdDoesNotExist
     */
    public function getRegistrationByUserId(UserId $userId): UserRegistration;

    /**
     * @throws UserPasswordRecoveryByPasswordRecoveryTokenDoesNotExist
     */
    public function getPasswordRecoveryByToken(string $passwordRecoveryToken): UserPasswordRecovery;

    /**
     * @throws UserPasswordRecoveryByUserIdDoesNotExist
     */
    public function getPasswordRecoveryByUserId(UserId $userId): UserPasswordRecovery;

    /**
     * @throws UserEmailChangeByEmailChangeTokenDoesNotExist
     */
    public function getEmailChangeByToken(string $emailChangeToken): UserEmailChange;

    /**
     * @throws UserEmailChangeByUserIdDoesNotExist
     */
    public function getEmailChangeByUserId(UserId $userId): UserEmailChange;

    /** @return array<int, User> */
    public function getAll(): array;

    public function save(User $user): void;

    public function remove(User $user): void;

    public function removeRegistration(UserRegistration $userRegistration): void;

    public function removePasswordRecovery(UserPasswordRecovery $userPasswordRecovery): void;

    public function removeEmailChange(UserEmailChange $userEmailChange): void;
}
