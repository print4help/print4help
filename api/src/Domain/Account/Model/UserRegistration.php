<?php

declare(strict_types=1);

namespace App\Domain\Account\Model;

use App\Domain\DateTimeHelper;
use App\Domain\UuidGenerator;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @final
 * @ORM\Entity()
 */
class UserRegistration
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="registration")
     */
    private User $user;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private string $registrationToken;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $registrationTokenGenerationDate;

    private function __construct(
        User $user,
        string $registrationToken,
        DateTimeImmutable $registrationTokenGenerationDate
    ) {
        $this->id = UuidGenerator::generate();
        $this->user = $user;
        $this->registrationToken = $registrationToken;
        $this->registrationTokenGenerationDate = $registrationTokenGenerationDate;
    }

    public static function create(User $user): self
    {
        $registrationTokenGenerationDate = DateTimeHelper::create();
        $registrationToken = self::generateRegistrationToken($user, $registrationTokenGenerationDate);

        return new self($user, $registrationToken, $registrationTokenGenerationDate);
    }

    private static function generateRegistrationToken(User $user, DateTimeImmutable $dateTimeImmutable): string
    {
        return substr(
            hash(
                'sha1',
                sprintf(
                    '%s-%s',
                    $user->getId()->toString(),
                    $dateTimeImmutable->format(DateTimeImmutable::ATOM)
                )
            ),
            0,
            10
        );
    }

    public function getRegistrationToken(): string
    {
        return $this->registrationToken;
    }

    public function isRegistrationTokenValid(): bool
    {
        return DateTimeHelper::create()->modify('-24 hours') < $this->registrationTokenGenerationDate;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
