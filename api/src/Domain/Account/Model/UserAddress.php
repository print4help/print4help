<?php
declare(strict_types=1);

namespace App\Domain\Account\Model;

use App\Domain\Account\UserAddressId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @final
 * @ORM\Entity()
 */
class UserAddress
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    private string $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Account\Model\User", inversedBy="addresses")
     */
    private User $user;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     */
    private string $street;

    /**
     * @ORM\Column(type="string")
     */
    private string $streetNumber;

    /**
     * @ORM\Column(type="string")
     */
    private string $zip;

    /**
     * @ORM\Column(type="string")
     */
    private string $city;

    /**
     * @ORM\Column(type="string")
     */
    private string $country;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $addition;

    private function __construct(
        UserAddressId $id,
        User $user,
        string $name,
        string $street,
        string $streetNumber,
        string $zip,
        string $city,
        string $country,
        ?string $addition
    ) {
        $this->id = $id->toString();
        $this->user = $user;
        $this->name = $name;
        $this->street = $street;
        $this->streetNumber = $streetNumber;
        $this->zip = $zip;
        $this->city = $city;
        $this->country = $country;
        $this->addition = $addition;
    }

    public static function create(
        UserAddressId $id,
        User $user,
        string $name,
        string $street,
        string $streetNumber,
        string $zip,
        string $city,
        string $country,
        ?string $addition
    ): self {
        return new self($id, $user, $name, $street, $streetNumber, $zip, $city, $country, $addition);
    }

    public function edit(
        string $name,
        string $street,
        string $streetNumber,
        string $zip,
        string $city,
        string $country,
        ?string $addition
    ): void {
        $this->name = $name;
        $this->street = $street;
        $this->streetNumber = $streetNumber;
        $this->zip = $zip;
        $this->city = $city;
        $this->country = $country;
        $this->addition = $addition;
    }

    public function getId(): UserAddressId
    {
        return UserAddressId::fromString($this->id);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getStreetNumber(): string
    {
        return $this->streetNumber;
    }

    public function getZip(): string
    {
        return $this->zip;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getAddition(): ?string
    {
        return $this->addition;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
