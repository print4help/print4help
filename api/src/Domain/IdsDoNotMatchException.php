<?php

declare(strict_types=1);

namespace App\Domain;

use function sprintf;

final class IdsDoNotMatchException extends DomainException
{
    public function __construct(mixed $idInUrl, mixed $idInRequestBody)
    {
        $message = sprintf(
            'Given Ids do not match, expecting [%s] and [%s] to be equal',
            json_encode($idInUrl, JSON_THROW_ON_ERROR),
            json_encode($idInRequestBody, JSON_THROW_ON_ERROR)
        );

        parent::__construct($message);
    }
}
