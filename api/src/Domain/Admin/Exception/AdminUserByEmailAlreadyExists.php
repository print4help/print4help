<?php

declare(strict_types=1);

namespace App\Domain\Admin\Exception;

use App\Domain\DomainException;
use App\Domain\Email;
use Throwable;

final class AdminUserByEmailAlreadyExists extends DomainException
{
    public function __construct(Email $email, Throwable $previous = null)
    {
        parent::__construct(sprintf('AdminUser with Email [%s] already exists', $email->toString()), 0, $previous);
    }
}
