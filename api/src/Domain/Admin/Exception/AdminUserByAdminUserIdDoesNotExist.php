<?php

declare(strict_types=1);

namespace App\Domain\Admin\Exception;

use App\Domain\Admin\AdminUserId;
use App\Domain\DomainException;
use Throwable;

final class AdminUserByAdminUserIdDoesNotExist extends DomainException
{
    public function __construct(AdminUserId $adminUserId, Throwable $previous = null)
    {
        parent::__construct(sprintf('AdminUser with AdminId [%s] does not exist', $adminUserId->toString()), 0, $previous);
    }
}
