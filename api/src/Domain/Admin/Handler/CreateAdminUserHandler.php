<?php
declare(strict_types=1);

namespace App\Domain\Admin\Handler;

use App\Domain\Admin\Command\CreateAdminUser;
use App\Domain\Admin\Exception\AdminUserByEmailAlreadyExists;
use App\Domain\Admin\Exception\AdminUserByEmailDoesNotExist;
use App\Domain\Admin\Model\AdminUser;
use App\Domain\Admin\Repository\AdminUserRepository;
use App\Domain\CommandHandlerInterface;

final class CreateAdminUserHandler implements CommandHandlerInterface
{
    public function __construct(
        private AdminUserRepository $adminUserRepository,
    ) {
    }

    public function __invoke(CreateAdminUser $command): void
    {
        try {
            $this->adminUserRepository->getByEmail($command->getEmail());

            throw new AdminUserByEmailAlreadyExists($command->getEmail());
        } catch (AdminUserByEmailDoesNotExist $exception) {
            // happy path
        }

        $adminUser = AdminUser::create($command->getAdminUserId(), $command->getEmail());
        $adminUser->updatePassword($command->getPassword());

        $this->adminUserRepository->save($adminUser);
    }
}
