<?php

declare(strict_types=1);

namespace App\Domain;

use DateTimeImmutable;
use function is_array;
use function is_bool;
use function is_float;
use function is_int;
use function is_numeric;
use function is_string;

final class InputHelper
{
    public static function string(mixed $value): string
    {
        if (!is_string($value)) {
            throw new InvalidArgumentException($value, 'string');
        }

        return $value;
    }

    public static function int(mixed $value): int
    {
        if (!is_int($value)) {
            throw new InvalidArgumentException($value, 'integer');
        }

        return $value;
    }

    /**
     * @return array<mixed, mixed>
     */
    public static function array(mixed $value): array
    {
        if (!is_array($value)) {
            throw new InvalidArgumentException($value, 'array');
        }

        return $value;
    }

    /**
     * @return array<array-key, string>
     */
    public static function arrayOfStrings(mixed $value): array
    {
        return array_map(
            static fn (mixed $entry): string => self::string($entry),
            self::array($value)
        );
    }

    public static function bool(mixed $value): bool
    {
        if (!is_bool($value)) {
            throw new InvalidArgumentException($value, 'boolean');
        }

        return $value;
    }

    public static function float(mixed $value): float
    {
        if (!is_float($value)) {
            throw new InvalidArgumentException($value, 'float');
        }

        return $value;
    }

    public static function numeric(mixed $value): float
    {
        if (!is_numeric($value)) {
            throw new InvalidArgumentException($value, 'float');
        }

        return (float)$value;
    }

    public static function nullableString(mixed $value): ?string
    {
        if ($value === null) {
            return null;
        }

        return self::string($value);
    }

    public static function nullableInt(mixed $value): ?int
    {
        if ($value === null) {
            return null;
        }

        return self::int($value);
    }

    /**
     * @return array<mixed, mixed>|null
     */
    public static function nullableArray(mixed $value): ?array
    {
        if ($value === null) {
            return null;
        }

        return self::array($value);
    }

    public static function nullableFloat(mixed $value): ?float
    {
        if ($value === null) {
            return null;
        }

        return self::float($value);
    }

    public static function dateTimeImmutable(mixed $value): DateTimeImmutable
    {
        if ($value instanceof DateTimeImmutable === false) {
            throw new InvalidArgumentException($value, DateTimeImmutable::class);
        }

        return $value;
    }

    public static function nullableDateTimeImmutable(mixed $value): ?DateTimeImmutable
    {
        if ($value === null) {
            return null;
        }

        return self::dateTimeImmutable($value);
    }

    /**
     * @template T
     * @param class-string<T> $type
     * @return T
     */
    public static function type(mixed $value, string $type)
    {
        if ($value instanceof $type === false) {
            throw new InvalidArgumentException($value, $type);
        }
        return $value;
    }
}
