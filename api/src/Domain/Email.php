<?php

declare(strict_types=1);

namespace App\Domain;

use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\NoRFCWarningsValidation;
use InvalidArgumentException;
use function strtolower;
use function trim;

final class Email
{
    private string $value;

    public function __construct(string $value)
    {
        if (self::isValid($value) === false) {
            throw new InvalidArgumentException(sprintf('Email [%s] is not RFC compatible', $value));
        }

        $this->value = strtolower(trim($value));
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    public static function isValid(string $value): bool
    {
        $validator = new EmailValidator();

        return $validator->isValid($value, new NoRFCWarningsValidation());
    }

    public function toString(): string
    {
        return $this->value;
    }
}
