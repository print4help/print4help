<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\ORM\Repository;

use App\Domain\Account\Exception\UserByEmailDoesNotExist;
use App\Domain\Account\Exception\UserByUserIdDoesNotExist;
use App\Domain\Account\Exception\UserEmailChangeByEmailChangeTokenDoesNotExist;
use App\Domain\Account\Exception\UserEmailChangeByUserIdDoesNotExist;
use App\Domain\Account\Exception\UserPasswordRecoveryByPasswordRecoveryTokenDoesNotExist;
use App\Domain\Account\Exception\UserPasswordRecoveryByUserIdDoesNotExist;
use App\Domain\Account\Exception\UserRegistrationByRegistrationTokenDoesNotExist;
use App\Domain\Account\Exception\UserRegistrationByUserIdDoesNotExist;
use App\Domain\Account\Model\User;
use App\Domain\Account\Model\UserEmailChange;
use App\Domain\Account\Model\UserPasswordRecovery;
use App\Domain\Account\Model\UserRegistration;
use App\Domain\Account\Repository\UserRepository as UserRepositoryInterface;
use App\Domain\Account\UserId;
use App\Domain\Email;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use RuntimeException;

final class UserRepository implements UserRepositoryInterface
{
    public function __construct(private ManagerRegistry $registry)
    {
    }

    public function get(UserId $userId): User
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('u');
        $queryBuilder->where('u.id = :id');
        $queryBuilder->setParameter('id', $userId->toString());

        $user = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$user instanceof User) {
            throw new UserByUserIdDoesNotExist($userId);
        }

        return $user;
    }

    public function getByEmail(Email $email): User
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('u');
        $queryBuilder->where('u.email = :email');
        $queryBuilder->setParameter('email', $email->toString());

        $user = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$user instanceof User) {
            throw new UserByEmailDoesNotExist($email);
        }

        return $user;
    }

    public function getRegistrationByToken(string $registrationToken): UserRegistration
    {
        $queryBuilder = $this->getManager()->createQueryBuilder();
        $queryBuilder->select('ur');
        $queryBuilder->from(UserRegistration::class, 'ur');
        $queryBuilder->join('ur.user', 'u');
        $queryBuilder->where('ur.registrationToken = :registrationToken');
        $queryBuilder->setParameter('registrationToken', $registrationToken);
        $queryBuilder->getQuery();

        $userRegistration = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$userRegistration instanceof UserRegistration) {
            throw new UserRegistrationByRegistrationTokenDoesNotExist($registrationToken);
        }

        return $userRegistration;
    }

    public function getRegistrationByUserId(UserId $userId): UserRegistration
    {
        $queryBuilder = $this->getManager()->createQueryBuilder();
        $queryBuilder->select('ur');
        $queryBuilder->from(UserRegistration::class, 'ur');
        $queryBuilder->join('ur.user', 'u');
        $queryBuilder->where('ur.user = :user');
        $queryBuilder->setParameter('user', $userId->toString());
        $queryBuilder->getQuery();

        $userRegistration = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$userRegistration instanceof UserRegistration) {
            throw new UserRegistrationByUserIdDoesNotExist($userId);
        }

        return $userRegistration;
    }

    public function getPasswordRecoveryByToken(string $passwordRecoveryToken): UserPasswordRecovery
    {
        $queryBuilder = $this->getManager()->createQueryBuilder();
        $queryBuilder->select('ur');
        $queryBuilder->from(UserPasswordRecovery::class, 'ur');
        $queryBuilder->join('ur.user', 'u');
        $queryBuilder->where('ur.passwordRecoveryToken = :passwordRecoveryToken');
        $queryBuilder->setParameter('passwordRecoveryToken', $passwordRecoveryToken);
        $queryBuilder->getQuery();

        $userPasswordRecovery = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$userPasswordRecovery instanceof UserPasswordRecovery) {
            throw new UserPasswordRecoveryByPasswordRecoveryTokenDoesNotExist($passwordRecoveryToken);
        }

        return $userPasswordRecovery;
    }

    public function getPasswordRecoveryByUserId(UserId $userId): UserPasswordRecovery
    {
        $queryBuilder = $this->getManager()->createQueryBuilder();
        $queryBuilder->select('ur');
        $queryBuilder->from(UserPasswordRecovery::class, 'ur');
        $queryBuilder->join('ur.user', 'u');
        $queryBuilder->where('ur.user = :user');
        $queryBuilder->setParameter('user', $userId->toString());
        $queryBuilder->getQuery();

        $userPasswordRecovery = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$userPasswordRecovery instanceof UserPasswordRecovery) {
            throw new UserPasswordRecoveryByUserIdDoesNotExist($userId);
        }

        return $userPasswordRecovery;
    }

    public function getEmailChangeByToken(string $emailChangeToken): UserEmailChange
    {
        $queryBuilder = $this->getManager()->createQueryBuilder();
        $queryBuilder->select('ur');
        $queryBuilder->from(UserEmailChange::class, 'ur');
        $queryBuilder->join('ur.user', 'u');
        $queryBuilder->where('ur.emailChangeToken = :emailChangeToken');
        $queryBuilder->setParameter('emailChangeToken', $emailChangeToken);
        $queryBuilder->getQuery();

        $userEmailChange = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$userEmailChange instanceof UserEmailChange) {
            throw new UserEmailChangeByEmailChangeTokenDoesNotExist($emailChangeToken);
        }

        return $userEmailChange;
    }

    public function getEmailChangeByUserId(UserId $userId): UserEmailChange
    {
        $queryBuilder = $this->getManager()->createQueryBuilder();
        $queryBuilder->select('ur');
        $queryBuilder->from(UserEmailChange::class, 'ur');
        $queryBuilder->join('ur.user', 'u');
        $queryBuilder->where('ur.user = :user');
        $queryBuilder->setParameter('user', $userId->toString());
        $queryBuilder->getQuery();

        $userEmailChange = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$userEmailChange instanceof UserEmailChange) {
            throw new UserEmailChangeByUserIdDoesNotExist($userId);
        }

        return $userEmailChange;
    }

    /**
     * @return array<int, User>
     */
    public function getAll(): array
    {
        /** @var array<int, User> $users */
        $users = $this->getRepository()->createQueryBuilder('u')
            ->orderBy('u.email', 'ASC')
            ->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();

        return $users;
    }

    public function save(User $user): void
    {
        $this->getManager()->persist($user);
        $this->getManager()->flush();
    }

    public function remove(User $user): void
    {
        $this->getManager()->remove($user);
        $this->getManager()->flush();
    }

    public function removeRegistration(UserRegistration $userRegistration): void
    {
        $this->getRegistrationManager()->remove($userRegistration);
        $this->getRegistrationManager()->flush();
    }

    public function removePasswordRecovery(UserPasswordRecovery $userPasswordRecovery): void
    {
        $this->getPasswordRecoveryManager()->remove($userPasswordRecovery);
        $this->getPasswordRecoveryManager()->flush();
    }

    public function removeEmailChange(UserEmailChange $userEmailChange): void
    {
        $this->getEmailChangeManager()->remove($userEmailChange);
        $this->getEmailChangeManager()->flush();
    }

    private function getManager(): EntityManager
    {
        $manager = $this->registry->getManagerForClass(User::class);

        if (!$manager instanceof EntityManager) {
            throw new RuntimeException();
        }

        if ($manager->isOpen() === false) {
            $manager = $this->registry->resetManager();
        }

        if (!$manager instanceof EntityManager) {
            throw new RuntimeException();
        }

        return $manager;
    }

    private function getRegistrationManager(): EntityManager
    {
        $manager = $this->registry->getManagerForClass(UserRegistration::class);

        if (!$manager instanceof EntityManager) {
            throw new RuntimeException();
        }

        if ($manager->isOpen() === false) {
            $manager = $this->registry->resetManager();
        }

        if (!$manager instanceof EntityManager) {
            throw new RuntimeException();
        }

        return $manager;
    }

    private function getPasswordRecoveryManager(): EntityManager
    {
        $manager = $this->registry->getManagerForClass(UserPasswordRecovery::class);

        if (!$manager instanceof EntityManager) {
            throw new RuntimeException();
        }

        if ($manager->isOpen() === false) {
            $manager = $this->registry->resetManager();
        }

        if (!$manager instanceof EntityManager) {
            throw new RuntimeException();
        }

        return $manager;
    }

    private function getEmailChangeManager(): EntityManager
    {
        $manager = $this->registry->getManagerForClass(UserEmailChange::class);

        if (!$manager instanceof EntityManager) {
            throw new RuntimeException();
        }

        if ($manager->isOpen() === false) {
            $manager = $this->registry->resetManager();
        }

        if (!$manager instanceof EntityManager) {
            throw new RuntimeException();
        }

        return $manager;
    }

    /**
     * @return EntityRepository<User>
     */
    private function getRepository(): EntityRepository
    {
        $repository = $this->registry->getRepository(User::class);

        if (!$repository instanceof EntityRepository) {
            throw new RuntimeException();
        }

        return $repository;
    }
}
