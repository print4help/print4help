<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Normalizer;

use App\Domain\Market\Piece\Category;
use App\Infrastructure\Symfony\Translation\CategoryTranslator;
use InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CategoryNormalizer implements NormalizerInterface
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    /**
     * @param mixed $object
     * @param array<mixed, mixed> $context
     * @return array{id: string, name: string, description: string}
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        if (!$object instanceof Category) {
            throw new InvalidArgumentException();
        }

        return $this->normalizeCategory($object);
    }

    /**
     * @psalm-type children list<array{id: string, name: string, description: string}>
     * @phpstan-ignore-next-line
     * @return array{id: string, name: string, description: string, children: children}
     */
    private function normalizeCategory(Category $category): array
    {
        /** @phpstan-ignore-next-line */
        return [
            'id' => $category->id()->toString(),
            'name' => $this->translator->trans(
                CategoryTranslator::buildName($category->id()),
                [],
                'category'
            ),
            'description' => $this->translator->trans(
                CategoryTranslator::buildDescription($category->id()),
                [],
                'category'
            ),
            'children' => $this->normalizeChildren($category->children()),
        ];
    }

    /**
     * @param Category[] $children
     * @return list<array{id: string, name: string, description: string}>
     */
    private function normalizeChildren(array $children): array
    {
        $responseChildren = [];
        foreach ($children as $child) {
            $responseChildren[] = $this->normalizeCategory($child);
        }
        return $responseChildren;
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof Category;
    }
}
