<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Offer;

use App\Infrastructure\Symfony\Validator\Inquiry\InquiryBelongsToCurrentUser;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use App\Infrastructure\Symfony\Validator\Offer\OfferBelongsNotToCurrentUser;
use App\Infrastructure\Symfony\Validator\Offer\OfferBelongsToInquiry;
use App\Infrastructure\Symfony\Validator\Offer\OfferExists;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

#[OfferBelongsToInquiry(message: "offer.accept_offer.offer_does_not_belong_to_inquiry")]
class AcceptOfferRequest implements HasOfferAndInquiry
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message:"offer.accept_offer.inquiry_id.not_blank")]
    #[Assert\Type(type:"string", message:"offer.accept_offer.inquiry_id.inquiry_id.type")]
    #[Assert\Uuid(message: "offer.accept_offer.inquiry_id.invalid_uuid", strict: false)]
    #[InquiryExists(message: "offer.accept_offer.inquiry_id.not_found")]
    #[InquiryIsStatus(
        message: "offer.accept_offer.inquiry_id.status_not_open",
        status: ["open"]
    )]
    #[InquiryBelongsToCurrentUser(message: "offer.accept_offer.inquiry_id.belongs_to_current_user")]
    public mixed $inquiryId;

    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message:"offer.accept_offer.offer_id.not_blank")]
    #[Assert\Type(type:"string", message:"offer.accept_offer.offer_id.type")]
    #[Assert\Uuid(message: "offer.accept_offer.offer_id.invalid_uuid", strict: false)]
    #[OfferExists(message: "offer.accept_offer.offer_id.does_not_exist")]
    #[OfferBelongsNotToCurrentUser(message: "offer.accept_offer.offer_id.belongs_not_to_current_user")]
    public mixed $offerId;

    public function offerId(): mixed
    {
        return $this->offerId ?? null;
    }

    public function inquiryId(): mixed
    {
        return $this->inquiryId ?? null;
    }
}
