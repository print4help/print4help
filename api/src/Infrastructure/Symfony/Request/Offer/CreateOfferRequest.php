<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Offer;

use App\Infrastructure\Symfony\Validator\Inquiry\InquiryBelongsNotToCurrentUser;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use App\Infrastructure\Symfony\Validator\Offer\UserHasNoOfferForThisInquiry;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class CreateOfferRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message:"offer.create_offer.inquiry_id.not_blank")]
    #[Assert\Type(type:"string", message:"offer.create_offer.inquiry_id.type")]
    #[Assert\Uuid(message: "offer.create_offer.inquiry_id.invalid_uuid", strict: false)]
    #[InquiryExists(message: "offer.create_offer.inquiry_id.not_found")]
    #[InquiryIsStatus(
        message: "offer.create_offer.inquiry_id.status_not_open",
        status: ["open"]
    )]
    #[UserHasNoOfferForThisInquiry(message: "offer.create_offer.inquiry_id.already_exists")]
    #[InquiryBelongsNotToCurrentUser(message: "offer.create_offer.inquiry_id.belongs_to_yourself")]
    public mixed $inquiryId;

    /**
     * @OA\Property(type="string")
     */
    #[Assert\NotBlank(message:"offer.create_offer.message.not_blank")]
    #[Assert\Type(type:"string", message:"offer.create_offer.message.type")]
    #[Assert\Length(
        max: 1000,
        maxMessage: "offer.create_offer.message.length.max"
    )]
    public mixed $message;

    /**
     * @OA\Property(type="int")
     */
    #[Assert\NotBlank(message:"offer.create_offer.quantity.not_blank")]
    #[Assert\Type(type:"int", message:"offer.create_offer.quantity.type")]
    #[Assert\LessThanOrEqual(value: 100, message: "offer.create_offer.quantity.less_than_or_equal")]
    #[Assert\GreaterThanOrEqual(value: 1, message: "offer.create_offer.quantity.greater_than_or_equal")]
    public mixed $quantity;
}
