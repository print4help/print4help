<?php
declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Offer;

interface HasOfferAndInquiry
{
    public function offerId(): mixed;
    public function inquiryId(): mixed;
}
