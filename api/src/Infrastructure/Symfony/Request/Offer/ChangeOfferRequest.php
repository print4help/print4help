<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Offer;

use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use App\Infrastructure\Symfony\Validator\Offer\OfferBelongsToCurrentUser;
use App\Infrastructure\Symfony\Validator\Offer\OfferBelongsToInquiry;
use App\Infrastructure\Symfony\Validator\Offer\OfferExists;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

#[OfferBelongsToInquiry(message: "offer.change_offer.offer_does_not_belong_to_inquiry")]
class ChangeOfferRequest implements HasOfferAndInquiry
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message:"offer.change_offer.inquiry_id.not_blank")]
    #[Assert\Type(type:"string", message:"offer.change_offer.inquiry_id.inquiry_id.type")]
    #[Assert\Uuid(message: "offer.change_offer.inquiry_id.invalid_uuid", strict: false)]
    #[InquiryExists(message: "offer.change_offer.inquiry_id.not_found")]
    #[InquiryIsStatus(
        message: "offer.change_offer.inquiry_id.status_not_open",
        status: ["open"]
    )]
    public mixed $inquiryId;

    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message:"offer.change_offer.offer_id.not_blank")]
    #[Assert\Type(type:"string", message:"offer.change_offer.offer_id.inquiry_id.type")]
    #[Assert\Uuid(message: "offer.change_offer.offer_id.invalid_uuid", strict: false)]
    #[OfferExists(message: "offer.change_offer.offer_id.does_not_exist")]
    #[OfferBelongsToCurrentUser(message: "offer.change_offer.offer_id.belongs_to_current_user")]
    public mixed $offerId;

    /**
     * @OA\Property(type="string")
     */
    #[Assert\NotBlank(message:"offer.change_offer.message.not_blank")]
    #[Assert\Type(type:"string", message:"offer.change_offer.message.type")]
    #[Assert\Length(
        max: 1000,
        maxMessage: "offer.change_offer.message.length.max"
    )]
    public mixed $message;

    /**
     * @OA\Property(type="int")
     */
    #[Assert\NotBlank(message:"offer.change_offer.quantity.not_blank")]
    #[Assert\Type(type:"int", message:"offer.change_offer.quantity.type")]
    #[Assert\LessThanOrEqual(value: 100, message: "offer.change_offer.quantity.less_than_or_equal")]
    #[Assert\GreaterThanOrEqual(value: 1, message: "offer.change_offer.quantity.greater_than_or_equal")]
    public mixed $quantity;

    public function offerId(): mixed
    {
        if (!isset($this->offerId)) {
            return null;
        }
        return $this->offerId;
    }

    public function inquiryId(): mixed
    {
        return $this->inquiryId ?? null;
    }
}
