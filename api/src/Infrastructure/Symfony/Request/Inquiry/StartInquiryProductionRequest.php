<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Inquiry;

use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryHasAcceptedOfferFromCurrentUser;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class StartInquiryProductionRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "inquiry.start_production.id.not_blank")]
    #[Assert\Type(type: "string", message: "inquiry.start_production.id.type")]
    #[Assert\Uuid(message: "inquiry.start_production.id.invalid_uuid", strict: false)]
    #[InquiryExists(message: "inquiry.start_production.id.not_found")]
    #[InquiryHasAcceptedOfferFromCurrentUser(message: "inquiry.start_production.id.no_accepted_offer_from_current_user")]
    #[InquiryIsStatus(
        message: "inquiry.start_production.id.status_not_in_execution",
        status: ["in_execution"]
    )]
    public mixed $inquiryId;
}
