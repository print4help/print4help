<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Inquiry;

use App\Infrastructure\Symfony\Validator\Inquiry\InquiryBelongsToCurrentUser;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use App\Infrastructure\Symfony\Validator\Inquiry\Requirements;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateInquiryMetadataRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "inquiry.update_inquiry_metadata.id.not_blank")]
    #[Assert\Type(type: "string", message: "inquiry.update_inquiry_metadata.id.type")]
    #[Assert\Uuid(message: "inquiry.update_inquiry_metadata.id.invalid_uuid", strict: false)]
    #[InquiryExists(message: "inquiry.update_inquiry_metadata.id.not_found")]
    #[InquiryBelongsToCurrentUser(message: "inquiry.update_inquiry_metadata.id.access_denied")]
    #[InquiryIsStatus(
        message: "inquiry.update_inquiry_metadata.id.status_not_open",
        status: ["open"]
    )]
    public mixed $inquiryId;

    /**
     * @OA\Property(type="int", example="5")
     */
    #[Assert\NotBlank(message: "cart.change_piece_quantity.quantity.not_blank")]
    #[Assert\Type(type: "integer", message: "cart.change_piece_quantity.quantity.type")]
    #[Assert\GreaterThanOrEqual(value: 0, message: "cart.change_piece_quantity.quantity.min")]
    #[Assert\LessThanOrEqual(value:100, message:"cart.change_piece_quantity.quantity.max")]
    public mixed $quantity;

    /**
     * @OA\Property(type="string", example="My old ones broke")
     */
    #[Assert\Type(type: "string", message: "inquiry.update_inquiry_metadata.purpose.type")]
    #[Assert\Length(max: 1000, maxMessage: "inquiry.update_inquiry_metadata.purpose.max_length")]
    public mixed $purpose;

    /**
     * @OA\Property(
     *     type="array",
     *     @OA\Items(type="string", maxLength=1000),
     *     example="['Color: Red', 'Material: any']"
     * )
     */
    #[Requirements(
        message: "inquiry.update_cart_item_metadata.requirements.type",
        messageKeyIsNotInt: "inquiry.update_cart_item_metadata.requirements.key.type",
        messageValueIsNotString: "inquiry.update_cart_item_metadata.requirements.value.type",
        messageValueMaxLength: "inquiry.update_cart_item_metadata.requirements.value.max_length",
        maxLength: 1000
    )]
    public mixed $requirements;
}
