<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Inquiry;

use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryHasAcceptedOfferFromCurrentUser;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class AbortInquiryProductionRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "inquiry.abort_production.id.not_blank")]
    #[Assert\Type(type: "string", message: "inquiry.abort_production.id.type")]
    #[Assert\Uuid(message: "inquiry.abort_production.id.invalid_uuid", strict: false)]
    #[InquiryExists(message: "inquiry.abort_production.id.not_found")]
    #[InquiryHasAcceptedOfferFromCurrentUser(message: "inquiry.abort_production.id.no_accepted_offer_from_current_user")]
    #[InquiryIsStatus(
        message: "inquiry.abort_production.id.status_not_in_execution_production_started",
        status: ["in_execution", "production_started"]
    )]
    public mixed $inquiryId;
}
