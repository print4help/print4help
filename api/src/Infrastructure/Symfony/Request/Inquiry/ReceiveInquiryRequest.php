<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Inquiry;

use App\Infrastructure\Symfony\Validator\Inquiry\InquiryBelongsToCurrentUser;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class ReceiveInquiryRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "inquiry.receive.id.not_blank")]
    #[Assert\Type(type: "string", message: "inquiry.receive.id.type")]
    #[Assert\Uuid(message: "inquiry.receive.id.invalid_uuid", strict: false)]
    #[InquiryExists(message: "inquiry.receive.id.not_found")]
    #[InquiryBelongsToCurrentUser(message: "inquiry.receive.id.access_denied")]
    #[InquiryIsStatus(
        message: "inquiry.receive.id.status_not_in_execution_production_started_production_finished_shipped",
        status: ["in_execution", "production_started", "production_finished", "shipped"]
    )]
    public mixed $inquiryId;
}
