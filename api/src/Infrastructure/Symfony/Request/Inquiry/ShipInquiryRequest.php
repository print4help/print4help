<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Inquiry;

use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryHasAcceptedOfferFromCurrentUser;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class ShipInquiryRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "inquiry.ship.id.not_blank")]
    #[Assert\Type(type: "string", message: "inquiry.ship.id.type")]
    #[Assert\Uuid(message: "inquiry.ship.id.invalid_uuid", strict: false)]
    #[InquiryExists(message: "inquiry.ship.id.not_found")]
    #[InquiryHasAcceptedOfferFromCurrentUser(message: "inquiry.ship.id.no_accepted_offer_from_current_user")]
    #[InquiryIsStatus(
        message: "inquiry.ship.id.status_not_in_execution_production_started_production_finished",
        status: ["in_execution", "production_started", "production_finished"]
    )]
    public mixed $inquiryId;
}
