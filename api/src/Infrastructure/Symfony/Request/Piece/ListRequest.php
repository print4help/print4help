<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Piece;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ListRequest
{
    /**
     * @Assert\All({
     *     @Assert\Type(type="string"),
     * })
     */
    #[Assert\Type('array')]
    public mixed $categoryIds;

    public static function createFromRequest(Request $request): self
    {
        $self = new self();
        $self->categoryIds = $request->query->all('categoryIds');

        return $self;
    }
}
