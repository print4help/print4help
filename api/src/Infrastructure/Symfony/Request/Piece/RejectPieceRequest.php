<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Piece;

use App\Infrastructure\Symfony\Validator\Piece\PieceExists;
use App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySet;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class RejectPieceRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "piece.reject_piece.id.not_blank")]
    #[Assert\Type(type: "string", message: "piece.reject_piece.id.type")]
    #[Assert\Uuid(message: "piece.reject_piece.id.invalid_uuid", strict: false)]
    #[PieceExists(message: "piece.approve_piece.id.not_found")]
    #[PieceStatusAlreadySet(
        message: "piece.reject_piece.id.status_already_set",
        targetStatus: "rejected"
    )]
    public mixed $id;

    /**
     * @OA\Property(type="string", maxLength=2000)
     */
    #[Assert\NotBlank(message: "piece.reject_piece.message.not_blank")]
    #[Assert\Type(type: "string", message: "piece.reject_piece.message.type")]
    #[Assert\Length(
        max: 2000,
        maxMessage: "piece.reject_piece.message.max_length"
    )]
    public mixed $message;
}
