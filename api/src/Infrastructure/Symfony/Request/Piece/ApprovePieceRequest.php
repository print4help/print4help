<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Piece;

use App\Infrastructure\Symfony\Validator\Piece\PieceExists;
use App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySet;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class ApprovePieceRequest
{
    /**
     * @var mixed
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "piece.approve_piece.id.not_blank")]
    #[Assert\Type(type: "string", message: "piece.approve_piece.id.type")]
    #[Assert\Uuid(message: "piece.approve_piece.id.invalid_uuid", strict: false)]
    #[PieceExists(message: "piece.approve_piece.id.not_found")]
    #[PieceStatusAlreadySet(
        message: "piece.approve_piece.id.status_already_set",
        targetStatus: "approved"
    )]
    public mixed $id;

    /**
     * @OA\Property(type="string", maxLength=2000)
     */
    #[Assert\Type(type: "string", message: "piece.approve_piece.message.type")]
    #[Assert\Length(
        max: 2000,
        maxMessage: "piece.approve_piece.message.max_length"
    )]
    public mixed $message = null;
}
