<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Piece;

use App\Infrastructure\Symfony\Validator\Piece\PieceBelongsToCurrentUser;
use App\Infrastructure\Symfony\Validator\Piece\PieceExists;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateMetadataRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "piece.update_metadata.id.not_blank")]
    #[Assert\Type(type: "string", message: "piece.update_metadata.id.type")]
    #[Assert\Uuid(message: "piece.update_metadata.id.invalid_uuid", strict: false)]
    #[PieceExists(message: "piece.update_metadata.id.not_found")]
    #[PieceBelongsToCurrentUser(message: "piece.update_metadata.id.access_denied")]
    public mixed $id;

    /**
     * @OA\Property(type="string", minLength=3, maxLength=255)
     */
    #[Assert\NotBlank(message: "piece.update_metadata.name.not_blank")]
    #[Assert\Type(type: "string", message: "piece.update_metadata.name.type")]
    #[Assert\Length(
        min: 3,
        max: 255,
        minMessage: "piece.update_metadata.name.min_length",
        maxMessage: "piece.update_metadata.name.max_length"
    )]
    public mixed $name;

    /**
     * @OA\Property(type="string", maxLength=2000)
     */
    #[Assert\NotBlank(message: "piece.update_metadata.summary.not_blank")]
    #[Assert\Length(
        min: 25,
        max: 2000,
        minMessage: "piece.update_metadata.summary.min_length",
        maxMessage: "piece.update_metadata.summary.max_length"
    )]
    public mixed $summary;

    /**
     * @var mixed
     * @OA\Property(type="string", maxLength=10000)
     */
    #[Assert\Length(
        max: 10000,
        maxMessage: "piece.update_metadata.description.max_length"
    )]
    public mixed $description;

    /**
     * @OA\Property(type="string", maxLength=10000)
     */
    #[Assert\Length(
        max: 10000,
        maxMessage: "piece.update_metadata.manufacturingNotes.max_length"
    )]
    public mixed $manufacturingNotes;

    /**
     * @OA\Property(type="string", maxLength=10000)
     */
    #[Assert\Length(
        max: 10000,
        maxMessage: "piece.update_metadata.licence.max_length"
    )]
    public mixed $licence;

    /**
     * @OA\Property(type="string")
     */
    #[Assert\Url(message: "piece.update_metadata.sourceUrl.invalid")]
    public mixed $sourceUrl;
}
