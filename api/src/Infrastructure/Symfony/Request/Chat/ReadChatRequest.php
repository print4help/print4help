<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Chat;

use App\Infrastructure\Symfony\Validator\Chat\ChatExists;
use App\Infrastructure\Symfony\Validator\Chat\CurrentUserIsPartOfChat;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class ReadChatRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message:"chat.read.chat_id.not_blank")]
    #[Assert\Type(type:"string", message:"chat.read.chat_id.type")]
    #[Assert\Uuid(message: "chat.read.chat_id.invalid_uuid", strict: false)]
    #[ChatExists(message: "chat.read.chat_id.not_found", throwNotFoundException: true)]
    #[CurrentUserIsPartOfChat(message: "chat.read.chat_id.not_part_of_chat", throwAccessDeniedException: true)]
    public mixed $chatId;
}
