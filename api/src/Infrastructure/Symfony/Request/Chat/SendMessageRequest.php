<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Chat;

use App\Infrastructure\Symfony\Validator\Chat\ChatExists;
use App\Infrastructure\Symfony\Validator\Chat\ChatIsStatus;
use App\Infrastructure\Symfony\Validator\Chat\CurrentUserIsPartOfChat;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class SendMessageRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message:"chat.send_message.chat_id.not_blank")]
    #[Assert\Type(type:"string", message:"chat.send_message.chat_id.type")]
    #[Assert\Uuid(message: "chat.send_message.chat_id.invalid_uuid", strict: false)]
    #[ChatExists(message: "chat.send_message.chat_id.not_found")]
    #[ChatIsStatus(
        message: "chat.send_message.chat_id.status_not_open",
        status: ["open"]
    )]
    #[CurrentUserIsPartOfChat(message: "chat.send_message.chat_id.not_part_of_chat")]
    public mixed $chatId;

    /**
     * @OA\Property(type="string")
     */
    #[Assert\NotBlank(message:"chat.send_message.message.not_blank")]
    #[Assert\Type(type:"string", message:"chat.send_message.message.type")]
    #[Assert\Length(
        max: 1000,
        maxMessage: "chat.send_message.message.length.max"
    )]
    public mixed $message;

    /**
     * @OA\Property(type="string")
     */
    #[Assert\Type(type:"string", message:"chat.send_message.subject.type")]
    #[Assert\Length(
        max: 1000,
        maxMessage: "chat.send_message.message.subject.max"
    )]
    public mixed $subject;
}
