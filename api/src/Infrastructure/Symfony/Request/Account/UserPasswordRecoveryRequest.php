<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Account;

use App\Infrastructure\Symfony\Validator\Account\UserExists;
use App\Infrastructure\Symfony\Validator\Account\UserPasswordRecoveryDoesNotExistForUserWithEmail;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class UserPasswordRecoveryRequest
{
    /**
     * @OA\Property(type="string", maxLength=255, example="john.doe@example.com")
     */
    #[Assert\NotBlank(message: "user_password_recovery.email.not_blank")]
    #[Assert\Type(type: "string", message: "user_password_recovery.email.type")]
    #[Assert\Email(message: "user_password_recovery.email.invalid", mode: "strict")]
    #[UserExists(message: "user_password_recovery.email.user_does_not_exist")]
    #[UserPasswordRecoveryDoesNotExistForUserWithEmail(message: "user_password_recovery.email.token_does_not_exist")]
    public mixed $email;
}
