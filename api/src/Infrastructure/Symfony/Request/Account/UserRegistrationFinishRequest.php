<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Account;

use App\Infrastructure\Symfony\Validator\Account\UserRegistrationExistsForRegistrationToken;
use App\Infrastructure\Symfony\Validator\Account\ValidUserRegistrationToken;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class UserRegistrationFinishRequest
{
    /**
     * @OA\Property(type="string", maxLength=255, example="32cd11ba8c")
     */
    #[Assert\NotBlank(message: "user_registration_finish.token.not_blank")]
    #[Assert\Type(type: "string", message: "user_registration_finish.token.type")]
    #[UserRegistrationExistsForRegistrationToken(message: "user_registration_finish.token.does_not_exist")]
    #[ValidUserRegistrationToken(message: "user_registration_finish.token.invalid")]
    public mixed $registrationToken;

    /**
     * regex is from https://regexr.com/3bfsi
     * - 8-chars
     * - min 1 Uppercase
     * - min 1 Number
     * - min 1 special-char
     * -> "Abcdef1!"
     *
     * @OA\Property(
     *     type="string",
     *     maxLength=255,
     *     example="Abcdef1!",
     *     description="The Password has to be 8 characters long, at least 1 Uppercase, 1 Number and 1 special-char"
     * )
     */
    #[Assert\NotBlank(message: "user_registration_finish.password.not_blank")]
    #[Assert\Type(type: "string", message: "user_registration_finish.password.type")]
    #[Assert\Regex(
        pattern: "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/",
        message: "user_registration_finish.password.regex",
    )]
    #[Assert\Length(
        min: 8,
        max: 4096,
        minMessage: "user_registration_finish.password.min_length",
        maxMessage: "user_registration_finish.password.max_length"
    )]
    public mixed $password;

    /**
     * @OA\Property(
     *     type="string",
     *     maxLength=255,
     *     example="Abcdef1!",
     *     description="The exact same value as for [newPassword]"
     * )
     */
    #[Assert\NotBlank(message: "user_registration_finish.password_repeat.not_blank")]
    #[Assert\Type(type: "string", message: "user_registration_finish.password_repeat.type")]
    #[Assert\EqualTo(propertyPath: "password", message: "user_registration_finish.password_repeat.do_not_match")]
    public mixed $passwordRepeat;
}
