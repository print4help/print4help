<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Account;

use App\Infrastructure\Symfony\Validator\Account\UniqueUserEmail;
use App\Infrastructure\Symfony\Validator\Account\UserEmailChangeExistsForEmailChangeToken;
use App\Infrastructure\Symfony\Validator\Account\ValidUserEmailChangeToken;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class UserEmailChangeFinishRequest
{
    /**
     * @OA\Property(type="string", maxLength=255, example="john.doe@example.com")
     */
    #[UniqueUserEmail(message: "user_email_change_finish.email.user_already_exists")]
    #[Assert\NotBlank(message: "user_email_change_finish.email.not_blank")]
    #[Assert\Type(type: "string", message: "user_email_change_finish.email.type")]
    #[Assert\Email(message: "user_email_change_finish.email.invalid", mode: "strict")]
    public mixed $email;

    /**
     * @OA\Property(type="string", maxLength=255, example="32cd11ba8c")
     */
    #[Assert\NotBlank(message: "user_email_change_finish.email_change_token.not_blank")]
    #[Assert\Type(type: "string", message: "user_email_change_finish.email_change_token.type")]
    #[UserEmailChangeExistsForEmailChangeToken(message: "user_email_change_finish.email_change_token.does_not_exist")]
    #[ValidUserEmailChangeToken(message: "user_email_change_finish.email_change_token.invalid")]
    public mixed $emailChangeToken;
}
