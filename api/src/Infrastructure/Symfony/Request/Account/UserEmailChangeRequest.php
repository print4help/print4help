<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Account;

use App\Infrastructure\Symfony\Validator\Account\UniqueUserEmail;
use App\Infrastructure\Symfony\Validator\Account\UserEmailChangeDoesNotExistForUser;
use App\Infrastructure\Symfony\Validator\Account\UserExists;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class UserEmailChangeRequest
{
    /**
     * @OA\Property(type="string", maxLength=255, example="john.doe@example.com")
     */
    #[UniqueUserEmail(message: "user_email_change.email.email_already_exists")]
    #[Assert\NotBlank(message: "user_email_change.email.not_blank")]
    #[Assert\Type(type: "string", message: "user_email_change.email.type")]
    #[Assert\Email(message: "user_email_change.email.valid", mode: "strict")]
    public mixed $email;

    /**
     * @OA\Property(type="string", maxLength=255, example="d882da02-76e1-11eb-9439-0242ac130002")
     */
    #[UserExists(message: "user_email_change.user_id.does_not_exist")]
    #[UserEmailChangeDoesNotExistForUser(message: "user_email_change.user_id.email_change_already_exists")]
    #[Assert\NotBlank(message: "user_email_change.user_id.not_blank")]
    #[Assert\Type(type: "string", message: "user_email_change.user_id.type")]
    public mixed $userId;
}
