<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Chat;

use App\Domain\InputHelper;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\Command\SendMessage;
use App\Domain\Market\Offer\MessageId;
use App\Infrastructure\ReadModel\Repository\MessageRepository;
use App\Infrastructure\Symfony\Request\Chat\SendMessageRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SendMessageAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Chat")
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Chat\SendMessageRequest::class)
     * )
     * @OA\Response(
     *     response=201,
     *     description="The sent message",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Message::class)
     *          )
     *     )
     * )
     */
    #[Route('/chat/send-message', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        MessageRepository $messageRepository
    ): JsonResponse {
        $sendMessageRequest = $requestHandler->handle($request, SendMessageRequest::class);

        $messageId = MessageId::create();
        $sendMessage = new SendMessage(
            ChatId::fromString(InputHelper::string($sendMessageRequest->chatId)),
            $messageId,
            $this->userId(),
            InputHelper::string($sendMessageRequest->message),
            InputHelper::nullableString($sendMessageRequest->subject)
        );
        $bus->dispatch($sendMessage);

        $message = $messageRepository->get($messageId->toString());

        return new JsonResponse(['data' => $message], Response::HTTP_CREATED, []);
    }
}
