<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Chat;

use App\Domain\IdsDoNotMatchException;
use App\Domain\InputHelper;
use App\Domain\Market\Offer\Command\UpdateMessage;
use App\Domain\Market\Offer\MessageId;
use App\Infrastructure\ReadModel\Repository\MessageRepository;
use App\Infrastructure\Symfony\Request\Chat\UpdateMessageRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UpdateMessageAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Chat")
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Chat\UpdateMessageRequest::class)
     * )
     * @OA\Response(
     *     response=200,
     *     description="The updated message",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Message::class)
     *          )
     *     )
     * )
     */
    #[Route('/chat/update-message/{uuid}', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        MessageRepository $messageRepository
    ): JsonResponse {
        $updateMessageRequest = $requestHandler->handle(
            $request,
            UpdateMessageRequest::class,
            function (UpdateMessageRequest $updateMessageRequest, Request $request): void {
                if ($request->get('uuid') !== $updateMessageRequest->messageId) {
                    throw new IdsDoNotMatchException($request->get('uuid'), $updateMessageRequest->messageId);
                }

                if (isset($updateMessageRequest->subject) === false) {
                    $updateMessageRequest->subject = null;
                }
            }
        );

        $messageId = MessageId::fromString(InputHelper::string($updateMessageRequest->messageId));
        $sendMessage = new UpdateMessage(
            $messageId,
            $this->userId(),
            InputHelper::string($updateMessageRequest->message),
            InputHelper::nullableString($updateMessageRequest->subject)
        );
        $bus->dispatch($sendMessage);

        $message = $messageRepository->get($messageId->toString());

        return new JsonResponse(['data' => $message], Response::HTTP_OK, []);
    }
}
