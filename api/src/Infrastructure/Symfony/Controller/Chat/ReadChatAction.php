<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Chat;

use App\Domain\InputHelper;
use App\Infrastructure\ReadModel\Repository\ChatRepository;
use App\Infrastructure\Symfony\Request\Chat\ReadChatRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReadChatAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Chat")
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="the uuid of the chat",
     * )
     * @OA\Response(
     *     response=200,
     *     description="The Chat with all messages",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Chat::class)
     *          )
     *     )
     * )
     * @OA\Response(
     *     response=404,
     *     description="If the chat is not found"
     * )
     * @OA\Response(
     *     response=403,
     *     description="Access Denied if the chat does not belong to any of the participants"
     * )
     */
    #[Route('/chat/read/{uuid}', methods: ['GET'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        ChatRepository $chatRepository
    ): JsonResponse {
        $readChatRequest = $requestHandler->handleGETRequest(
            $request,
            ReadChatRequest::class,
            function (ReadChatRequest $readChatRequest, Request $request): void {
                $readChatRequest->chatId = $request->get('uuid');
            }
        );

        $chat = $chatRepository->getWithMessages(
            InputHelper::string($readChatRequest->chatId)
        );

        return new JsonResponse(['data' => $chat], Response::HTTP_CREATED, []);
    }
}
