<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Chat;

use App\Infrastructure\ReadModel\Repository\ChatRepository;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListMyChatsAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Chat")
     * @OA\Response(
     *     response=201,
     *     description="The sent message",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              type="array",
     *              @OA\Items(
     *                  ref=@Model(type=App\Infrastructure\ReadModel\Message::class)
     *              )
     *          )
     *     )
     * )
     */
    #[Route('/chat/list-my', methods: ['GET'])]
    public function __invoke(
        Request $request,
        ChatRepository $chatRepository
    ): JsonResponse {
        $chats = $chatRepository->findByUser($this->userId()->toString());

        return new JsonResponse(['data' => $chats], Response::HTTP_OK, []);
    }
}
