<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Security;

use App\Infrastructure\Symfony\Response\DefaultResponse;
use App\Infrastructure\Symfony\Security\LoginRequest;
use App\Infrastructure\Symfony\Security\LoginResponse;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class LoginAction
{
    /**
     * @OA\Tag(name="Security")
     * @OA\Response(
     *     response=200,
     *     description="Login Response with Token",
     *     @Model(type=LoginResponse::class)
     * )
     * @OA\Response(
     *     response=401,
     *     description="Unauthorized failure",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\Response(
     *     response=403,
     *     description="Authenticaiton failure",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=LoginRequest::class)
     * )
     */
    #[Route('/login', methods: ['POST'])]
    public function __invoke(
        Request $request
    ): JsonResponse {
        // proxy method just for OpenAPI Documentation
        return new JsonResponse();
    }
}
