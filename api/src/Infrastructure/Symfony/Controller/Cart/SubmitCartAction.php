<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Cart;

use App\Domain\Market\Cart\Command\SubmitCart;
use App\Infrastructure\ReadModel\Repository\CartRepository;
use App\Infrastructure\Symfony\Request\Cart\SubmitCartRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\Response\DefaultResponse;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SubmitCartAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Cart")
     * @OA\Response(
     *     response=201,
     *     description="Submit the Cart and open an Inquiry",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\Symfony\Response\DefaultResponse::class)
     *          )
     *     )
     * )
     */
    #[Route('/user/cart/submit', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        CartRepository $cartRepository
    ): JsonResponse {
        $requestHandler->handle($request, SubmitCartRequest::class);
        $submitCart = new SubmitCart($this->userId());
        $bus->dispatch($submitCart);

        return new JsonResponse(new DefaultResponse(), Response::HTTP_CREATED, []);
    }
}
