<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Offer;

use App\Domain\InputHelper;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Offer\Command\CreateOffer;
use App\Domain\Market\Offer\OfferId;
use App\Infrastructure\ReadModel\Repository\OfferRepository;
use App\Infrastructure\Symfony\Request\Offer\CreateOfferRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CreateOfferAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Offer")
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Offer\CreateOfferRequest::class)
     * )
     * @OA\Response(
     *     response=201,
     *     description="Submit an Offer for an Inquiry",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Offer::class)
     *          )
     *     )
     * )
     */
    #[Route('/offer/create', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        OfferRepository $offerRepository
    ): JsonResponse {
        $createOfferRequest = $requestHandler->handle($request, CreateOfferRequest::class);

        $offerId = OfferId::create();
        $createOffer = new CreateOffer(
            $offerId,
            $this->userId(),
            InquiryId::fromString(
                InputHelper::string($createOfferRequest->inquiryId)
            ),
            InputHelper::string($createOfferRequest->message),
            InputHelper::int($createOfferRequest->quantity)
        );
        $bus->dispatch($createOffer);

        $offer = $offerRepository->get($offerId->toString());

        return new JsonResponse(['data' => $offer], Response::HTTP_CREATED, []);
    }
}
