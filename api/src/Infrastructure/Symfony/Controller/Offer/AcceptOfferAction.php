<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Offer;

use App\Domain\IdsDoNotMatchException;
use App\Domain\InputHelper;
use App\Domain\Market\Offer\Command\AcceptOffer;
use App\Domain\Market\Offer\OfferId;
use App\Infrastructure\ReadModel\Repository\OfferRepository;
use App\Infrastructure\Symfony\Request\Offer\AcceptOfferRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AcceptOfferAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Offer")
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Offer\ChangeOfferRequest::class)
     * )
     * @OA\Response(
     *     response=200,
     *     description="Accept one of the offers from your Inquiry",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Offer::class)
     *          )
     *     )
     * )
     */
    #[Route('/offer/{uuid}/accept', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        OfferRepository $offerRepository
    ): JsonResponse {
        $acceptOfferRequest = $requestHandler->handle(
            $request,
            AcceptOfferRequest::class,
            function (AcceptOfferRequest $acceptOfferRequest, Request $request): void {
                if ($request->get('uuid') !== $acceptOfferRequest->offerId()) {
                    throw new IdsDoNotMatchException($request->get('uuid'), $acceptOfferRequest->offerId());
                }
            }
        );

        $changeOffer = new AcceptOffer(
            OfferId::fromString(InputHelper::string($acceptOfferRequest->offerId)),
        );
        $bus->dispatch($changeOffer);

        $offer = $offerRepository->get($changeOffer->offerId()->toString());

        return new JsonResponse(['data' => $offer], Response::HTTP_OK, []);
    }
}
