<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Offer;

use App\Infrastructure\ReadModel\Repository\OfferRepository;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ListMyOffersAction
{
    use UserTrait;
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Offer")
     * @OA\Response(
     *     response=200,
     *     description="List of all my offers",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              type="array",
     *              @OA\Items(
     *                  ref=@Model(type=App\Infrastructure\ReadModel\Offer::class)
     *              )
     *          )
     *     )
     * )
     */
    #[Route('/offer/list-my', methods: ['GET'])]
    public function __invoke(
        Request $request,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        OfferRepository $offerRepository
    ): JsonResponse {
        $user = $this->getUser();

        $offers = $offerRepository->getByUser($user->getId()->toString());

        return new JsonResponse(
            $serializer->serialize(['data' => $offers], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
