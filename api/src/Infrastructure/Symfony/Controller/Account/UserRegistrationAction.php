<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Account;

use App\Domain\Account\Command\RegisterUser;
use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\InputHelper;
use App\Infrastructure\Symfony\Request\Account\UserRegistrationRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\Response\DefaultResponse;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

final class UserRegistrationAction
{
    /**
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=201,
     *     description="Register a new User",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\Response(
     *     response=422,
     *     description="Validation Errors",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=UserRegistrationRequest::class)
     * )
     */
    #[Route('/registration', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus
    ): JsonResponse {
        $userRegistrationRequest = $requestHandler->handle($request, UserRegistrationRequest::class);
        $registerUser = new RegisterUser(
            UserId::create(),
            Email::fromString(
                InputHelper::string($userRegistrationRequest->email)
            ),
            InputHelper::string($userRegistrationRequest->firstName),
            InputHelper::string($userRegistrationRequest->lastName)
        );

        $bus->dispatch($registerUser);

        return new JsonResponse(new DefaultResponse(), Response::HTTP_CREATED);
    }
}
