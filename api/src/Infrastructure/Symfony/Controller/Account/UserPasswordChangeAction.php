<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Account;

use App\Domain\Account\Command\ChangeUserPassword;
use App\Domain\Account\UserId;
use App\Domain\InputHelper;
use App\Infrastructure\Symfony\Request\Account\UserPasswordChange;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\Response\DefaultResponse;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

final class UserPasswordChangeAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=200,
     *     description="Change a User password",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=UserPasswordChange::class)
     * )
     */
    #[Route('/user/settings/change-password', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus
    ): JsonResponse {
        $userPasswordChange = $requestHandler->handle($request, UserPasswordChange::class);
        $registerUser = new ChangeUserPassword(
            UserId::fromString(
                InputHelper::string($userPasswordChange->userId)
            ),
            InputHelper::string($userPasswordChange->newPassword)
        );

        $bus->dispatch($registerUser);

        return new JsonResponse(new DefaultResponse(), Response::HTTP_OK);
    }
}
