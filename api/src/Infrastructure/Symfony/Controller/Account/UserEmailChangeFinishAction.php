<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Account;

use App\Domain\Account\Command\FinishUserEmailChange;
use App\Domain\Email;
use App\Domain\InputHelper;
use App\Infrastructure\Symfony\Request\Account\UserEmailChangeFinishRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\Response\DefaultResponse;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

final class UserEmailChangeFinishAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=200,
     *     description="Finish the User E-Mail Address with the Token",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=UserEmailChangeFinishRequest::class)
     * )
     */
    #[Route('/user/settings/change-email-finish', methods: ["POST"])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus
    ): JsonResponse {
        $userEmailChangeFinishRequest = $requestHandler->handle($request, UserEmailChangeFinishRequest::class);
        $passwordRecovery = new FinishUserEmailChange(
            InputHelper::string($userEmailChangeFinishRequest->emailChangeToken),
            Email::fromString(
                InputHelper::string($userEmailChangeFinishRequest->email)
            ),
        );

        $bus->dispatch($passwordRecovery);

        return new JsonResponse(new DefaultResponse(), Response::HTTP_OK);
    }
}
