<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Category;

use App\Domain\Market\Piece\CategoryRepository;
use App\Infrastructure\Symfony\Request\RequestHandler;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ListAction
{
    /**
     * @OA\Tag(name="Category")
     * @OA\Response(
     *     response=200,
     *     description="List of all Categories",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="categories",
     *              type="array",
     *              @OA\Items(
     *                  @OA\Property(property="id", type="string", example="art-design"),
     *                  @OA\Property(property="name", type="string", example="Art & Design"),
     *                  @OA\Property(property="description", type="string", example="A nice description of the category"),
     *                  @OA\Property(
     *                      property="children",
     *                      type="array",
     *                          @OA\Items(
     *                              @OA\Property(property="id", type="string", example="art-designn.sculptures"),
     *                              @OA\Property(property="name", type="string", example="Scultptures"),
     *                              @OA\Property(property="description", type="string", example="A nice description of the category")
     *                          )
     *                      )
     *                  )
     *              )
     *          )
     *     )
     * )
     */
    #[Route('/category/list', methods: ['GET'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        SerializerInterface $serializer,
        CategoryRepository $categoryRepository
    ): JsonResponse {
        $categories = $categoryRepository->getAll();

        return new JsonResponse(
            $serializer->serialize(['data' => $categories], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
