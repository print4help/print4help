<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Inquiry;

use App\Domain\IdsDoNotMatchException;
use App\Domain\InputHelper;
use App\Domain\Market\Inquiry\Command\Receive;
use App\Domain\Market\Inquiry\InquiryId;
use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use App\Infrastructure\Symfony\Request\Inquiry\ReceiveInquiryRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ReceiveInquiryAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Inquiry")
     * @OA\Response(
     *     response=200,
     *     description="Receive the shipped & produced inquiry.",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Inquiry::class)
     *          )
     *     )
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Inquiry\ReceiveInquiryRequest::class)
     * )
     */
    #[Route('/inquiry/{uuid}/receive', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        InquiryRepository $inquiryRepository
    ): JsonResponse {
        $receiveInquiryRequest = $requestHandler->handle(
            $request,
            ReceiveInquiryRequest::class,
            function (ReceiveInquiryRequest $receiveInquiryRequest, Request $request): void {
                if ($request->get('uuid') !== $receiveInquiryRequest->inquiryId) {
                    throw new IdsDoNotMatchException($request->get('uuid'), $receiveInquiryRequest->inquiryId);
                }
            }
        );

        $inquiryId = InquiryId::fromString(InputHelper::string($receiveInquiryRequest->inquiryId));
        $startProduction = new Receive($inquiryId);

        $bus->dispatch($startProduction);

        $updatedInquiry = $inquiryRepository->get($inquiryId->toString());
        return new JsonResponse(['data' => $updatedInquiry], Response::HTTP_OK);
    }
}
