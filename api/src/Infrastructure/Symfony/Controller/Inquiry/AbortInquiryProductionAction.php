<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Inquiry;

use App\Domain\IdsDoNotMatchException;
use App\Domain\InputHelper;
use App\Domain\Market\Inquiry\Command\AbortProduction;
use App\Domain\Market\Inquiry\InquiryId;
use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use App\Infrastructure\Symfony\Request\Inquiry\AbortInquiryProductionRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AbortInquiryProductionAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Inquiry")
     * @OA\Response(
     *     response=200,
     *     description="Abort the production of the inquiry.",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Inquiry::class)
     *          )
     *     )
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Inquiry\AbortInquiryProductionRequest::class)
     * )
     */
    #[Route('/inquiry/{uuid}/abort-production', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        InquiryRepository $inquiryRepository
    ): JsonResponse {
        $closeInquiryRequest = $requestHandler->handle(
            $request,
            AbortInquiryProductionRequest::class,
            function (AbortInquiryProductionRequest $abortInquiryProductionRequest, Request $request): void {
                if ($request->get('uuid') !== $abortInquiryProductionRequest->inquiryId) {
                    throw new IdsDoNotMatchException($request->get('uuid'), $abortInquiryProductionRequest->inquiryId);
                }
            }
        );

        $inquiryId = InquiryId::fromString(InputHelper::string($closeInquiryRequest->inquiryId));
        $startProduction = new AbortProduction($inquiryId);

        $bus->dispatch($startProduction);

        $updatedInquiry = $inquiryRepository->get($inquiryId->toString());
        return new JsonResponse(['data' => $updatedInquiry], Response::HTTP_OK);
    }
}
