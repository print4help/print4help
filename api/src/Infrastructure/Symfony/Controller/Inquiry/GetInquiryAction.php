<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Inquiry;

use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use App\Infrastructure\ReadModel\Repository\OfferRepository;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetInquiryAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Inquiry")
     * @OA\Response(
     *     response=200,
     *     description="Get details of an inquiry",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Inquiry::class)
     *          )
     *     )
     * )
     */
    #[Route('/inquiry/{uuid}/detail', methods: ['GET'])]
    public function __invoke(
        Request $request,
        InquiryRepository $inquiryRepository,
        OfferRepository $offerRepository
    ): JsonResponse {
        $uuid = (string)$request->get('uuid');

        $inquiry = $inquiryRepository->get($uuid);

        if ($this->authenticated() && $inquiry->userId === $this->userId()->toString()) {
            $inquiry = $inquiryRepository->getWithOffers($uuid);
        }

        return new JsonResponse(['data' => $inquiry], Response::HTTP_OK, []);
    }
}
