<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Piece;

use App\Domain\IdsDoNotMatchException;
use App\Domain\InputHelper;
use App\Domain\Market\Piece\Command\ApprovePiece;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use App\Infrastructure\Symfony\Request\Piece\ApprovePieceRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ApprovePieceAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Piece")
     * @OA\Response(
     *     response=200,
     *     description="Approve a Piece that was requested for review.",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Piece::class)
     *          )
     *     )
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Piece\ApprovePieceRequest::class)
     * )
     */
    #[Route('/piece/{uuid}/approve', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        PieceRepository $pieceRepository
    ): JsonResponse {
        $approvePieceRequest = $requestHandler->handle(
            $request,
            ApprovePieceRequest::class,
            function (ApprovePieceRequest $approvePieceRequest, Request $request): void {
                if ($request->get('uuid') !== $approvePieceRequest->id) {
                    throw new IdsDoNotMatchException($request->get('uuid'), $approvePieceRequest->id);
                }
            }
        );

        $pieceId = PieceId::fromString(InputHelper::string($approvePieceRequest->id));
        $requestReview = new ApprovePiece(
            $pieceId,
            InputHelper::nullableString($approvePieceRequest->message)
        );

        $bus->dispatch($requestReview);
        $updatedPiece = $pieceRepository->get($pieceId->toString());

        return new JsonResponse(['data' => $updatedPiece], Response::HTTP_OK);
    }
}
