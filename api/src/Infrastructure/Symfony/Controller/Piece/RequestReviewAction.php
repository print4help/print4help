<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Piece;

use App\Domain\IdsDoNotMatchException;
use App\Domain\InputHelper;
use App\Domain\Market\Piece\Command\RequestReview;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use App\Infrastructure\Symfony\Request\Piece\RequestReviewRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class RequestReviewAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Piece")
     * @OA\Response(
     *     response=200,
     *     description="Request a review from Admins for a Piece",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Piece::class)
     *          )
     *     )
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Piece\RequestReviewRequest::class)
     * )
     */
    #[Route('/piece/{uuid}/request-review', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        PieceRepository $pieceRepository
    ): JsonResponse {
        $requestReviewRequest = $requestHandler->handle(
            $request,
            RequestReviewRequest::class,
            function (RequestReviewRequest $requestReviewRequest, Request $request): void {
                if ($request->get('uuid') !== $requestReviewRequest->id) {
                    throw new IdsDoNotMatchException($request->get('uuid'), $requestReviewRequest->id);
                }
            }
        );

        $pieceId = PieceId::fromString(InputHelper::string($requestReviewRequest->id));
        $requestReview = new RequestReview(
            $pieceId,
            InputHelper::nullableString($requestReviewRequest->message)
        );
        $bus->dispatch($requestReview);

        $updatedPiece = $pieceRepository->get($pieceId->toString());
        return new JsonResponse(['data' => $updatedPiece], Response::HTTP_OK);
    }
}
