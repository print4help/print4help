<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Piece;

use App\Domain\IdsDoNotMatchException;
use App\Domain\InputHelper;
use App\Domain\Market\Piece\Command\UpdatePieceMetadata;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use App\Infrastructure\Symfony\Request\Piece\UpdateMetadataRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UpdateMetadataAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Piece")
     * @OA\Response(
     *     response=200,
     *     description="The updated Piece with its new metadata",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Piece::class)
     *          )
     *     )
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=UpdateMetadataRequest::class)
     * )
     */
    #[Route('/piece/{uuid}/update', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        PieceRepository $pieceRepository
    ): JsonResponse {
        $updateMetadataRequest = $requestHandler->handle(
            $request,
            UpdateMetadataRequest::class,
            function (UpdateMetadataRequest $updateMetadataRequest, Request $request): void {
                if ($request->get('uuid') !== $updateMetadataRequest->id) {
                    throw new IdsDoNotMatchException($request->get('uuid'), $updateMetadataRequest->id);
                }
            }
        );

        $pieceId = PieceId::fromString(InputHelper::string($updateMetadataRequest->id));
        $createPiece = new UpdatePieceMetadata(
            $pieceId,
            InputHelper::string($updateMetadataRequest->name),
            InputHelper::nullableString($updateMetadataRequest->summary),
            InputHelper::nullableString($updateMetadataRequest->description),
            InputHelper::nullableString($updateMetadataRequest->manufacturingNotes),
            InputHelper::nullableString($updateMetadataRequest->licence),
            InputHelper::nullableString($updateMetadataRequest->sourceUrl)
        );

        $bus->dispatch($createPiece);

        $updatedPiece = $pieceRepository->get($pieceId->toString());
        return new JsonResponse(['data' => $updatedPiece], Response::HTTP_OK);
    }
}
