<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Piece;

use App\Domain\InputHelper;
use App\Infrastructure\ReadModel\Exception\PieceStatusHistoryByPieceIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\PieceStatusHistoryRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PieceStatusHistoryAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Piece")
     * @OA\Response(
     *     response=200,
     *     description="Receive the status history of a Piece.",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\PieceStatusHistory::class)
     *          )
     *     )
     * )
     */
    #[Route('/piece/{uuid}/status-history', methods:['GET'])]
    public function __invoke(
        Request $request,
        PieceStatusHistoryRepository $pieceStatusHistoryRepository
    ): JsonResponse {
        $pieceId = InputHelper::string($request->get('uuid'));

        // todo check for admin if piece is blocked?

        try {
            $pieceStatusHistory = $pieceStatusHistoryRepository->get($pieceId);
        } catch (PieceStatusHistoryByPieceIdNotFoundException $exception) {
            throw new NotFoundHttpException(
                sprintf('PieceStatusHistory for Id [%s] was not found', $pieceId),
                $exception
            );
        }

        return new JsonResponse(['data' => $pieceStatusHistory], Response::HTTP_OK);
    }
}
