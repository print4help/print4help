<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Piece;

use App\Domain\InputHelper;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\Command\CreatePiece;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use App\Infrastructure\Symfony\Request\Piece\CreatePieceRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CreatePieceAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Piece")
     * @OA\Response(
     *     response=201,
     *     description="The created Piece",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Piece::class)
     *          )
     *     )
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Piece\CreatePieceRequest::class)
     * )
     */
    #[Route('/piece/create', methods:['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        PieceRepository $pieceRepository
    ): JsonResponse {
        $createPieceRequest = $requestHandler->handle($request, CreatePieceRequest::class, );

        $createPiece = new CreatePiece(
            $this->userId(),
            PieceId::create(),
            ArticleNumber::create(),
            InputHelper::string($createPieceRequest->name)
        );

        $bus->dispatch($createPiece);
        $createdPiece = $pieceRepository->get($createPiece->pieceId()->toString());

        return new JsonResponse(['data' => $createdPiece], Response::HTTP_CREATED);
    }
}
