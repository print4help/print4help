<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Listener;

use App\Domain\DomainException;
use App\Domain\IdsDoNotMatchException;
use App\Infrastructure\ReadModel\ReadModelException;
use App\Infrastructure\ReadModel\ReadModelNotFoundException;
use App\Infrastructure\Symfony\ValidationException;
use Psr\Log\LoggerInterface;
use Sentry\SentrySdk;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

final class JsonExceptionListener
{
    public function __construct(
        private LoggerInterface $logger,
        private TranslatorInterface $translator
    ) {
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $response = $this->handleException($event->getThrowable());

        $event->setResponse($response);
    }

    private function handleException(Throwable $exception): JsonResponse
    {
        if ($exception instanceof ReadModelNotFoundException) {
            $data = [
                'code' => 404,
                'message' => $exception->getMessage(),
                'data' => null,
            ];

            return new JsonResponse($data, Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof UnexpectedValueException) {
            $data = [
                'code' => 400,
                'message' => 'Wrong request: json expected.',
                'data' => null,
            ];
            $this->logger->notice('An UnexpectedValueException reached the user', $data);

            return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
        }

        if ($exception instanceof UnauthorizedHttpException) {
            $data = [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
                'data' => null,
            ];

            return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof AccessDeniedException) {
            $data = [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
                'data' => null,
            ];
            $this->logger->notice('A Symfony AccessDeniedException reached the user', $data);

            return new JsonResponse($data, Response::HTTP_FORBIDDEN);
        }

        if ($exception instanceof HttpException) {
            $data = [
                'code' => $exception->getStatusCode(),
                'message' => $exception->getMessage(),
                'data' => null,
            ];
            $this->logger->notice('A HttpException reached the user', $data);

            return new JsonResponse($data, $exception->getStatusCode());
        }

        if ($exception instanceof ValidationException) {
            $data = [
                'code' => 422,
                'message' => 'Validation errors.',
                'data' => $this->transformViolationList($exception->getViolationList()),
            ];
            $this->logger->notice('A ValidationException reached the user', $data);

            return new JsonResponse($data, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($exception instanceof IdsDoNotMatchException) {
            $data = [
                'code' => 400,
                'message' => $exception->getMessage(),
                'data' => null,
            ];
            $this->logger->notice('A IdsDoNotMatchException reached the user', $data);

            return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
        }

        return $this->handleHardException($exception);
    }

    private function handleHardException(Throwable $exception): JsonResponse
    {
        // send sentry the exception, this should stay as the first measure!
        SentrySdk::getCurrentHub()->captureException($exception);
        $this->logger->critical('exception', ['message' => $exception->getMessage()]);

        if ($exception instanceof DomainException || $exception instanceof ReadModelException) {
            $data = [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
                'data' => null,
            ];
            $this->logger->error('A DomainException reached the user', $data);

            return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(
            [
                'code' => 500,
                'message' => 'Internal Server Error.',
                'data' => $this->transformException($exception),
            ],
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

    /**
     * @return array<string, mixed>
     */
    private function transformException(Throwable $exception): array
    {
        $previousException = $exception->getPrevious();

        return [
            'class' => get_class($exception),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => $exception->getTrace(),
            'previous' => $previousException ? $this->transformException($previousException) : null,
        ];
    }

    /**
     * @return array<int, array>
     */
    private function transformViolationList(ConstraintViolationListInterface $violationList): array
    {
        $errors = [];
        /** @var ConstraintViolation $violation */
        foreach ($violationList as $violation) {
            $errors[] = [
                'property' => $violation->getPropertyPath(),
                'message' => $this->translator->trans(
                    (string)$violation->getMessage(),
                    $violation->getParameters(),
                    'validators'
                ),
            ];
        }

        return $errors;
    }
}
