<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Listener;

use App\Infrastructure\Collector\ExceptionCollector;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

final class ExceptionCollectorListener
{
    public function __construct(private ExceptionCollector $collector)
    {
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $this->collector->collect($event->getThrowable());
    }
}
