<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Cart;

use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[Attribute]
final class UserHasCart extends Constraint
{
    public string $message = 'user.does_not_have_cart';
    public string $translationDomain = 'cart.validator';

    /**
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
        $this->message = $message;
    }

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
