<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Cart;

use App\Domain\Account\Repository\UserRepository;
use App\Domain\Email;
use App\Infrastructure\MongoDb\Repository\CartRepository;
use App\Infrastructure\ReadModel\Exception\CartByCartIdNotFoundException;
use App\Infrastructure\Symfony\UserTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class PieceExistsInCartValidator extends ConstraintValidator
{
    use UserTrait;

    public function __construct(
        private UserRepository $userRepository,
        private CartRepository $cartRepository,
    ) {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PieceExistsInCart) {
            throw new UnexpectedTypeException($constraint, PieceExistsInCart::class);
        }

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        $currentUser = $this->getUser();

        $user = $this->userRepository->getByEmail(
            Email::fromString($currentUser->getUsername())
        );

        $cartId = $user->getCartId();
        if ($cartId === null) {
            return;
        }

        try {
            $cart = $this->cartRepository->get($cartId->toString());
        } catch (CartByCartIdNotFoundException $exception) {
            return;
        }

        foreach ($cart->cartItems as $cartItem) {
            if ($cartItem->pieceId === $value) {
                return;
            }
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
