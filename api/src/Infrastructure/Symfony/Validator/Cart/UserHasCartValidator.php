<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Cart;

use App\Domain\Account\Repository\UserRepository;
use App\Domain\Email;
use App\Infrastructure\MongoDb\Repository\CartRepository;
use App\Infrastructure\ReadModel\Exception\CartByCartIdNotFoundException;
use App\Infrastructure\Symfony\UserTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

final class UserHasCartValidator extends ConstraintValidator
{
    use UserTrait;

    public function __construct(
        private UserRepository $userRepository,
        private CartRepository $cartRepository,
    ) {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UserHasCart) {
            throw new UnexpectedTypeException($constraint, UserHasCart::class);
        }

        $currentUser = $this->getUser();

        $user = $this->userRepository->getByEmail(
            Email::fromString($currentUser->getUsername())
        );

        $cartId = $user->getCartId();
        if ($cartId === null) {
            $this->context->buildViolation($constraint->message)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
            return;
        }

        try {
            $this->cartRepository->get($cartId->toString());
        } catch (CartByCartIdNotFoundException $exception) {
            $this->context->buildViolation($constraint->message)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
        }
    }
}
