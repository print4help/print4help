<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Account;

use App\Domain\Account\Exception\UserRegistrationByRegistrationTokenDoesNotExist;
use App\Domain\Account\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class ValidUserRegistrationTokenValidator extends ConstraintValidator
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidUserRegistrationToken) {
            throw new UnexpectedTypeException($constraint, ValidUserRegistrationToken::class);
        }

        if ($value === null) {
            return;
        }

        if (is_string($value) === false) {
            return;
        }

        try {
            $userRegistration = $this->userRepository->getRegistrationByToken($value);
        } catch (UserRegistrationByRegistrationTokenDoesNotExist $exception) {
            return;
        }

        if ($userRegistration->isRegistrationTokenValid() === true) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setTranslationDomain($constraint->translationDomain)
            ->setInvalidValue($value)
            ->addViolation();
    }
}
