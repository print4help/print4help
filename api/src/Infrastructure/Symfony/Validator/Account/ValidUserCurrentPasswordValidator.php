<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Account;

use App\Domain\Account\Exception\UserByUserIdDoesNotExist;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\Account\UserId;
use App\Infrastructure\Symfony\Request\Account\UserPasswordChange;
use App\Infrastructure\Symfony\Security\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

final class ValidUserCurrentPasswordValidator extends ConstraintValidator
{
    public function __construct(
        private UserRepository $userRepository,
        private UserPasswordHasherInterface $hasher
    ) {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidUserCurrentPassword) {
            throw new UnexpectedTypeException($constraint, ValidUserCurrentPassword::class);
        }

        if ($value === null) {
            return;
        }

        if ($value instanceof UserPasswordChange === false) {
            throw new UnexpectedTypeException($value, UserPasswordChange::class);
        }

        if ($value->userId === null || $value->currentPassword === null) {
            return;
        }

        if (is_string($value->userId) === false || is_string($value->currentPassword) === false) {
            return;
        }
        // todo use UserProvider(?) to compare logged in User with this userId ?

        try {
            $user = $this->userRepository->get(UserId::fromString($value->userId));
        } catch (UserByUserIdDoesNotExist $exception) {
            return;
        }

        if ($this->hasher->isPasswordValid(User::fromAccount($user), $value->currentPassword) === true) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->atPath('currentPassword')
            ->setTranslationDomain($constraint->translationDomain)
            ->setInvalidValue($value)
            ->addViolation();
    }
}
