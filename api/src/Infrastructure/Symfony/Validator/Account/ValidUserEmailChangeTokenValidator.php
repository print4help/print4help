<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Account;

use App\Domain\Account\Exception\UserEmailChangeByEmailChangeTokenDoesNotExist;
use App\Domain\Account\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class ValidUserEmailChangeTokenValidator extends ConstraintValidator
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidUserEmailChangeToken) {
            throw new UnexpectedTypeException($constraint, ValidUserEmailChangeToken::class);
        }

        if ($value === null) {
            return;
        }

        if (is_string($value) === false) {
            return;
        }

        try {
            $userEmailChange = $this->userRepository->getEmailChangeByToken($value);
        } catch (UserEmailChangeByEmailChangeTokenDoesNotExist $exception) {
            return;
        }

        if ($userEmailChange->isEmailChangeTokenValid() === true) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setTranslationDomain($constraint->translationDomain)
            ->setInvalidValue($value)
            ->addViolation();
    }
}
