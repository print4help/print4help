<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Piece;

use App\Infrastructure\ReadModel\Exception\PieceByPieceIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use App\Infrastructure\Symfony\UserTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class PieceBelongsToCurrentUserValidator extends ConstraintValidator
{
    use UserTrait;

    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PieceBelongsToCurrentUser) {
            throw new UnexpectedTypeException($constraint, PieceBelongsToCurrentUser::class);
        }

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        try {
            $piece = $this->pieceRepository->get($value);
        } catch (PieceByPieceIdNotFoundException $exception) {
            return;
        }

        if ($this->userIsAdmin() || $piece->userId === $this->userId()->toString()) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
