<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Piece;

use App\Domain\Market\Piece\PieceStatus;
use App\Infrastructure\ReadModel\Exception\PieceByPieceIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use function is_string;

final class PieceStatusAlreadySetValidator extends ConstraintValidator
{
    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PieceStatusAlreadySet) {
            throw new UnexpectedTypeException($constraint, PieceStatusAlreadySet::class);
        }

        if (PieceStatus::isValid($constraint->targetStatus) === false) {
            throw new UnexpectedValueException(
                $constraint->targetStatus,
                implode('|', PieceStatus::values())
            );
        }

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        try {
            $piece = $this->pieceRepository->get($value);
        } catch (PieceByPieceIdNotFoundException $exception) {
            return;
        }

        if ($piece->status !== $constraint->targetStatus) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
