<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Piece;

use App\Infrastructure\ReadModel\Exception\PieceByPieceIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class PieceExistsValidator extends ConstraintValidator
{
    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PieceExists) {
            throw new UnexpectedTypeException($constraint, PieceExists::class);
        }

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        try {
            $this->pieceRepository->get($value);
            return;
        } catch (PieceByPieceIdNotFoundException $exception) {
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
