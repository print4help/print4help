<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Piece;

use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[Attribute]
final class PieceIsStatus extends Constraint
{
    public string $status;
    public string $message = 'piece.is_not_expected_status';
    public string $translationDomain = 'inquiry.validator';

    /**
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        string $status,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
        $this->message = $message;
        $this->status = $status;
    }

    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
