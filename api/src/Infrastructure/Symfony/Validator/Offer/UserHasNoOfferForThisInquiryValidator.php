<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Offer;

use App\Infrastructure\ReadModel\Exception\OfferByInquiryIdAndUserIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use App\Infrastructure\ReadModel\Repository\OfferRepository;
use App\Infrastructure\Symfony\UserTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class UserHasNoOfferForThisInquiryValidator extends ConstraintValidator
{
    use UserTrait;

    public function __construct(
        private InquiryRepository $inquiryRepository,
        private OfferRepository $offerRepository
    ) {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UserHasNoOfferForThisInquiry) {
            throw new UnexpectedTypeException($constraint, UserHasNoOfferForThisInquiry::class);
        }

        if (is_string($value) === false || $value === '') {
            return;
        }

        try {
            $offer = $this->offerRepository->getByInquiryAndUser($value, $this->userId()->toString());
        } catch (OfferByInquiryIdAndUserIdNotFoundException $exception) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setParameter('{{ offerId }}', $offer->id)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
