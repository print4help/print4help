<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Offer;

use App\Infrastructure\ReadModel\Exception\OfferByOfferIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\OfferRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class OfferExistsValidator extends ConstraintValidator
{
    public function __construct(private OfferRepository $offerRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof OfferExists) {
            throw new UnexpectedTypeException($constraint, OfferExists::class);
        }

        if (is_string($value) === false || $value === '') {
            return;
        }

        try {
            $this->offerRepository->get($value);
        } catch (OfferByOfferIdNotFoundException $exception) {
            $this->context->buildViolation($constraint->message)
                ->setInvalidValue($value)
                ->setParameter('{{ offerId }}', $value)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
        }
    }
}
