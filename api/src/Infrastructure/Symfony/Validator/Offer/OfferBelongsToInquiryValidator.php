<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Offer;

use App\Infrastructure\ReadModel\Exception\OfferByOfferIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\OfferRepository;
use App\Infrastructure\Symfony\Request\Offer\HasOfferAndInquiry;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class OfferBelongsToInquiryValidator extends ConstraintValidator
{
    public function __construct(private OfferRepository $offerRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof OfferBelongsToInquiry) {
            throw new UnexpectedTypeException($constraint, OfferBelongsToInquiry::class);
        }

        if (!$value instanceof HasOfferAndInquiry) {
            throw new UnexpectedTypeException($value, HasOfferAndInquiry::class);
        }

        $offerId = $value->offerId();
        $inquiryId = $value->inquiryId();

        if (
            is_string($offerId) === false || $offerId === '' ||
            is_string($inquiryId) === false || $inquiryId === ''
        ) {
            return;
        }

        try {
            $offer = $this->offerRepository->get($offerId);
        } catch (OfferByOfferIdNotFoundException $exception) {
            return;
        }

        if ($offer->inquiryId === $inquiryId) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($inquiryId)
            ->setParameter('{{ inquiryId }}', $inquiryId)
            ->atPath('inquiryId')
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
