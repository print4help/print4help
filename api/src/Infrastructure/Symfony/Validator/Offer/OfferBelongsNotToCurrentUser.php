<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Offer;

use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[Attribute]
final class OfferBelongsNotToCurrentUser extends Constraint
{
    public string $message = 'offer.does_not_belong_to_current_user';
    public string $translationDomain = 'inquiry.validator';

    /**
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
        $this->message = $message;
    }

    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
