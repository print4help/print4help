<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Chat;

use App\Infrastructure\ReadModel\Exception\MessageByMessageIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\MessageRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class MessageExistsValidator extends ConstraintValidator
{
    public function __construct(private MessageRepository $messageRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof MessageExists) {
            throw new UnexpectedTypeException($constraint, MessageExists::class);
        }

        if (is_string($value) === false) {
            return;
        }

        if ($value === '') {
            return;
        }

        try {
            $this->messageRepository->get($value);
        } catch (MessageByMessageIdNotFoundException $exception) {
            if ($constraint->throwNotFoundException) {
                throw new NotFoundHttpException(sprintf('Message with id [%s] does not exist', $value));
            }

            $this->context->buildViolation($constraint->message)
                ->setInvalidValue($value)
                ->setParameter('{{ messageId }}', $value)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
        }
    }
}
