<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Chat;

use App\Infrastructure\ReadModel\Exception\ChatByChatIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\ChatRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class ChatExistsValidator extends ConstraintValidator
{
    public function __construct(private ChatRepository $chatRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ChatExists) {
            throw new UnexpectedTypeException($constraint, ChatExists::class);
        }

        if (is_string($value) === false || $value === '') {
            return;
        }

        try {
            $this->chatRepository->get($value);
        } catch (ChatByChatIdNotFoundException $exception) {
            if ($constraint->throwNotFoundException) {
                throw new NotFoundHttpException(sprintf('Chat with id [%s] does not exist', $value));
            }

            $this->context->buildViolation($constraint->message)
                ->setInvalidValue($value)
                ->setParameter('{{ chatId }}', $value)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
        }
    }
}
