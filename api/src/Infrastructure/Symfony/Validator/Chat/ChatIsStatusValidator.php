<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Chat;

use App\Domain\Market\Offer\ChatStatus;
use App\Infrastructure\ReadModel\Exception\ChatByChatIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\ChatRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use function is_string;

final class ChatIsStatusValidator extends ConstraintValidator
{
    public function __construct(private ChatRepository $chatRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ChatIsStatus) {
            throw new UnexpectedTypeException($constraint, ChatIsStatus::class);
        }

        self::assertAllStatusAreValid($constraint);

        if (is_string($value) === false || $value === '') {
            return;
        }

        try {
            $chat = $this->chatRepository->get($value);
        } catch (ChatByChatIdNotFoundException $exception) {
            return;
        }

        if (in_array($chat->status, $constraint->status, true)) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($chat->status)
            ->setParameter('{{ expected_status }}', implode('|', $constraint->status))
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }

    private static function assertAllStatusAreValid(ChatIsStatus $constraint): void
    {
        foreach ($constraint->status as $status) {
            if (ChatStatus::isValid($status) === false) {
                throw new UnexpectedValueException(
                    $status,
                    implode('|', ChatStatus::values())
                );
            }
        }
    }
}
