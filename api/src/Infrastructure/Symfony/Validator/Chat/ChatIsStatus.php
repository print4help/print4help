<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Chat;

use App\Domain\Market\Inquiry\InquiryStatus;
use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[Attribute]
final class ChatIsStatus extends Constraint
{
    /**
     * @var array<int, string>
     */
    public array $status;
    public string $message = 'chat.is_not_expected_status';
    public string $translationDomain = 'chat.validator';

    /**
     * @psalm-param array<int, InquiryStatus::*> $status
     *
     * @param array<int, string> $status
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        array $status,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
        $this->message = $message;
        $this->status = $status;
    }

    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
