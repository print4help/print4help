<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Chat;

use App\Infrastructure\ReadModel\Exception\ChatByChatIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\ChatRepository;
use App\Infrastructure\Symfony\UserTrait;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class CurrentUserIsPartOfChatValidator extends ConstraintValidator
{
    use UserTrait;

    public function __construct(private ChatRepository $chatRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof CurrentUserIsPartOfChat) {
            throw new UnexpectedTypeException($constraint, CurrentUserIsPartOfChat::class);
        }

        if (is_string($value) === false || $value === '') {
            return;
        }

        try {
            $chat = $this->chatRepository->get($value);
        } catch (ChatByChatIdNotFoundException $exception) {
            return;
        }

        if ($this->userIsAdmin()) {
            return;
        }

        if (in_array($this->userId()->toString(), $chat->userIds)) {
            return;
        }

        if ($constraint->throwAccessDeniedException) {
            throw new AccessDeniedException(sprintf('Access Denied to Chat [%s]', $value));
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setParameter('{{ chatId }}', $value)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
