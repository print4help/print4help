<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Chat;

use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[Attribute]
final class ChatExists extends Constraint
{
    public string $message = 'chat.does_not_exist';
    public string $translationDomain = 'chat.validator';
    public bool $throwNotFoundException;

    /**
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        bool $throwNotFoundException = false,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
        $this->message = $message;
        $this->throwNotFoundException = $throwNotFoundException;
    }

    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
