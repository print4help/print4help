<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Chat;

use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[Attribute]
final class CurrentUserIsPartOfChat extends Constraint
{
    public string $message = 'chat.does_not_belong_to_you';
    public string $translationDomain = 'chat.validator';
    public bool $throwAccessDeniedException;

    /**
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        bool $throwAccessDeniedException = false,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
        $this->message = $message;
        $this->throwAccessDeniedException = $throwAccessDeniedException;
    }

    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
