<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Chat;

use App\Infrastructure\ReadModel\Exception\MessageByMessageIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\MessageRepository;
use App\Infrastructure\Symfony\UserTrait;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class CurrentUserIsAuthorOfMessageValidator extends ConstraintValidator
{
    use UserTrait;

    public function __construct(private MessageRepository $messageRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof CurrentUserIsAuthorOfMessage) {
            throw new UnexpectedTypeException($constraint, CurrentUserIsAuthorOfMessage::class);
        }

        if (is_string($value) === false) {
            return;
        }

        if ($value === '') {
            return;
        }

        try {
            $message = $this->messageRepository->get($value);
        } catch (MessageByMessageIdNotFoundException $exception) {
            return;
        }

        if ($this->userIsAdmin()) {
            return;
        }

        if ($message->authorId === $this->userId()->toString()) {
            return;
        }

        if ($constraint->throwAccessDeniedException) {
            throw new AccessDeniedException(sprintf('Access Denied to Message [%s]', $value));
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setParameter('{{ messageId }}', $value)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
