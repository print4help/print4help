<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Inquiry;

use App\Domain\Market\Inquiry\InquiryStatus;
use App\Infrastructure\ReadModel\Exception\InquiryByInquiryIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use function is_string;

final class InquiryIsStatusValidator extends ConstraintValidator
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof InquiryIsStatus) {
            throw new UnexpectedTypeException($constraint, InquiryIsStatus::class);
        }

        self::assertAllStatusAreValid($constraint);

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        try {
            $inquiry = $this->inquiryRepository->get($value);
        } catch (InquiryByInquiryIdNotFoundException $exception) {
            return;
        }

        if (in_array($inquiry->status, $constraint->status, true)) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($inquiry->status)
            ->setParameter('{{ expected_status }}', implode('|', $constraint->status))
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }

    private static function assertAllStatusAreValid(InquiryIsStatus $constraint): void
    {
        foreach ($constraint->status as $status) {
            if (InquiryStatus::isValid($status) === false) {
                throw new UnexpectedValueException(
                    $status,
                    implode('|', InquiryStatus::values())
                );
            }
        }
    }
}
