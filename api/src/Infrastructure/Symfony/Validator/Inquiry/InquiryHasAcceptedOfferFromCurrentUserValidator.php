<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Inquiry;

use App\Infrastructure\ReadModel\Exception\InquiryByInquiryIdNotFoundException;
use App\Infrastructure\ReadModel\Exception\NoAcceptedOfferInInquiryException;
use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use App\Infrastructure\Symfony\UserTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class InquiryHasAcceptedOfferFromCurrentUserValidator extends ConstraintValidator
{
    use UserTrait;

    public function __construct(
        private InquiryRepository $inquiryRepository
    ) {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof InquiryHasAcceptedOfferFromCurrentUser) {
            throw new UnexpectedTypeException($constraint, InquiryHasAcceptedOfferFromCurrentUser::class);
        }

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        $currentUser = $this->getUser();

        try {
            $inquiry = $this->inquiryRepository->getWithOffers($value);
        } catch (InquiryByInquiryIdNotFoundException $exception) {
            return;
        }

        try {
            $acceptedOffer = $inquiry->acceptedOffer();
        } catch (NoAcceptedOfferInInquiryException $exception) {
            return;
        }

        if ($this->userIsAdmin()) {
            return;
        }

        if ($acceptedOffer->userId === $currentUser->getId()->toString()) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
