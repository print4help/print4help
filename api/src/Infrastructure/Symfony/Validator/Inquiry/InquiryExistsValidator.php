<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Inquiry;

use App\Infrastructure\ReadModel\Exception\InquiryByInquiryIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class InquiryExistsValidator extends ConstraintValidator
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof InquiryExists) {
            throw new UnexpectedTypeException($constraint, InquiryExists::class);
        }

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        try {
            $this->inquiryRepository->get($value);
        } catch (InquiryByInquiryIdNotFoundException $exception) {
            $this->context->buildViolation($constraint->message)
                ->setInvalidValue($value)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
        }
    }
}
