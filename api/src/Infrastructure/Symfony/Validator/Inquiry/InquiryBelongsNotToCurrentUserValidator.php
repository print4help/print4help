<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Inquiry;

use App\Domain\Account\Repository\UserRepository;
use App\Infrastructure\ReadModel\Exception\InquiryByInquiryIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use App\Infrastructure\Symfony\UserTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class InquiryBelongsNotToCurrentUserValidator extends ConstraintValidator
{
    use UserTrait;

    public function __construct(
        private InquiryRepository $inquiryRepository,
        private UserRepository $userRepository
    ) {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof InquiryBelongsNotToCurrentUser) {
            throw new UnexpectedTypeException($constraint, InquiryBelongsNotToCurrentUser::class);
        }

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        $currentUser = $this->getUser();

        try {
            $inquiry = $this->inquiryRepository->get($value);
        } catch (InquiryByInquiryIdNotFoundException $exception) {
            return;
        }

        if ($this->userIsAdmin() || $inquiry->userId !== $currentUser->getId()->toString()) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
