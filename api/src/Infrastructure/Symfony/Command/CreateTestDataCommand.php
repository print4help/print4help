<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Command;

use App\Domain\Account\Command\FinishUserRegistration;
use App\Domain\Account\Command\RegisterUser;
use App\Domain\Account\Model\User;
use App\Domain\Account\Model\UserRegistration;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\Account\UserId;
use App\Domain\Admin\AdminUserId;
use App\Domain\Admin\Command\CreateAdminUser;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\ChangePieceQuantity;
use App\Domain\Market\Cart\Command\RemovePieceFromCart;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\CategoryId;
use App\Domain\Market\Piece\Command\ApprovePiece;
use App\Domain\Market\Piece\Command\CreatePiece;
use App\Domain\Market\Piece\Command\RequestReview;
use App\Domain\Market\Piece\Command\UpdatePieceMetadata;
use App\Domain\Market\Piece\Piece;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\EventSourcing\Repository\PieceRepository;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class CreateTestDataCommand extends Command
{
    private const DEFAULT_USER_PASSWORD = 'p4h!';
    private MessageBusInterface $messageBus;
    private UserRepository $userRepository;
    private PieceRepository $pieceRepository;

    /**
     * @var string[]
     */
    private static $articleNumbers = [];

    public function __construct(
        MessageBusInterface $messageBus,
        UserRepository $userRepository,
        PieceRepository $pieceRepository
    ) {
        parent::__construct();
        $this->messageBus = $messageBus;
        $this->userRepository = $userRepository;
        $this->pieceRepository = $pieceRepository;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:test:data')
            ->setDescription('Create Test Data');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('');

        $this->createAdmin(
            AdminUserId::fromString('a136b19b-c71d-4214-b117-0022877565e4'),
            new Email('admin@print4help.org'),
            $output
        );

        $user = $this->createUser(
            UserId::fromString('439a1a14-e412-4d58-8d11-b443b30c9729'),
            new Email('user@print4help.org'),
            $output
        );

        $pieces = $this->createPieces($user, $output);

        $this->addPieceToCart($user, $pieces[0], 10, $output);
        $this->addPieceToCart($user, $pieces[1], 10, $output);

        $this->removePieceFromCart($user, $pieces[1], $output);

        $this->changePieceQuantityInCart($user, $pieces[0], 5, $output);
        $this->changePieceQuantityInCart($user, $pieces[0], 0, $output);

        $output->writeln('');
        $output->writeln('done.');

        return self::SUCCESS;
    }

    /**
     * @return Piece[]
     */
    private function createPieces(User $user, OutputInterface $output): array
    {
        $pieces = [];
        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('0e5f9c63-3f0e-4f61-955e-331a0bca1e9e'),
            'Surgical Mask Strap',
            ArticleNumber::fromString('BMX-M15'),
            [CategoryId::healthcare_HomeMedicalTools()],
            'A simple mask strap to hold up chirugal masks with the back of your head instead of the ears.',
            'Print as many as you like at once. Material can either be PLA, PETG, ASA or ABS.',
            null,
            'https://creativecommons.org/licenses/by/4.0/',
            'https://www.thingiverse.com/thing:4249113',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('579e472c-1f7f-4a19-9cd2-df5ebd0a4dd0'),
            'Door-Handle Attachment',
            ArticleNumber::fromString('SY4-KP2'),
            [CategoryId::household_Other(), CategoryId::healthcare_HomeMedicalTools()],
            'This Door-Opener can be attached to a door-handle so it can be opened with the forearm',
            'Measure your door handle and select the print according to your handle size.',
            'Depending on the model you need two or four M3x10 screws and M3 nuts',
            'By downloading, you are free to use and adapt the design as you would like for the purpose of 3D printing it yourself in any of the aforementioned technologies. The only requirement is that you attribute Materialise, by not removing the reference to Materialise on the design. This royalty-free license is solely intended for non-commercial use.',
            'https://www.materialise.com/en/hands-free-door-opener/technical-information',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('2b7498be-dcc7-45e4-9cd8-3c6cc3681b55'),
            'Ergonomic Door Opener',
            ArticleNumber::fromString('JK9-IW4'),
            [CategoryId::healthcare_HomeMedicalTools(), CategoryId::household_Other()],
            'For opening doors and pressing buttons without touching. Dual mode-direct touch or sideways for more leverage.',
            'Designed by 2C Solar (aka 3DPrintChCh) to be comfortable in the hand

Ergonomic design is helping the elderly to open doors hygienically.

Remember to wash regularly with hot soapy water or IPA',
            'Not needed',
            'https://creativecommons.org/licenses/by-nc/4.0/',
            'https://www.thingiverse.com/thing:4260659',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('0e639f36-11d6-4c61-9934-83df2586fca1'),
            'FaceShield (Prusa)',
            ArticleNumber::fromString('ATW-MY2'),
            [CategoryId::healthcare_MedicalTools()],
            'Helps to protect from direct aerosol splashes.',
            'The famous face shield from Prusa that can be printed with up to 4 pieces stacked and helps to avoid direct contact with aerosol splashes.',
            'To assemply this face shield, a transparent plastic shield is required. Please see the [source url](https://www.prusaprinters.org/prints/25857-prusa-face-shield) for details.',
            'We share these files under non-commercial licence. It would be great if you donated these shields to those in need for free. If you need to cover your production costs, we are ok with you selling the shields for production cost. However, we do not want to see these shields on eBay for $50.',
            'https://www.prusaprinters.org/prints/25857-prusa-face-shield',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('a9a25a73-98e6-407d-b2b7-6cc4985a38b6'),
            'Bottle Cap Opener',
            ArticleNumber::fromString('C37-UW4'),
            [CategoryId::household_Kitchen()],
            'Open or close bottles with ease',
            'With this bottle cap opener you don\'t need so much force for opening a bottle anymore',
            'Depending on the bottle sizes you might want to scale it up a bit (tested up to 125% for large bottles)',
            null,
            'https://www.prusaprinters.org/prints/35588-bottle-cap-opener-v2',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('94ded9ba-ed66-4492-88a0-eaa6f9c1270d'),
            'Bunk Bed Cup Holder 20mm',
            ArticleNumber::fromString('LS9-HQY'),
            [CategoryId::household_Bedroom(), CategoryId::healthcare_HomeMedicalTools()],
            'Place it on your bed and put a cup in it.',
            'This is a cup holder I designed for a bunkbed. It fits a small diameter bottle (fruit shoot size).',
            'No manufacturig required',
            '',
            'https://www.prusaprinters.org/prints/19773-bunk-bed-cup-holder-20mm',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('c291798a-f099-47d9-9ff2-1920b39bfdc0'),
            'Bedside Tray 260x110mm',
            ArticleNumber::fromString('AVR-MB9'),
            [CategoryId::household_Bedroom(), CategoryId::household_LivingRoom()],
            'Place something useful on the side of your bed',
            null,
            'The hook is made for 24mm thick material and it takes DIN912 M3 bolt and DIN934 M3 nuts.',
            'Creative Commons - Attribution - Non-Commercial - Share Alikelicense.',
            'https://www.thingiverse.com/thing:4457921',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('963c752b-fc30-436a-884e-0f2b60b68919'),
            'Stabilized Cup and Plate',
            ArticleNumber::fromString('FNS-JW8'),
            [CategoryId::healthcare_HomeMedicalTools()],
            'Self-stabilizing cup and plate holder for a crutch.',
            null,
            'No tools or skrews are required to assembly this one.',
            'Creative Commons - Attribution - Non-Commercial - Share Alikelicense.',
            'https://www.thingiverse.com/thing:2536014',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('aa50e519-a1ea-4e23-ae17-bb8a60758cbc'),
            'Coat Hanger Hook',
            ArticleNumber::fromString('Q4B-V93'),
            [CategoryId::household_Office(), CategoryId::household_Other(), CategoryId::tools()],
            'Simple hanger for a coat or a bag',
            null,
            null,
            'Creative Commons - Attribution - Non-Commercial - Share Alikelicense.',
            'https://www.thingiverse.com/thing:1569417',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('1b6e4e58-a8e6-4d95-a305-0496b36e1fbe'),
            'Customizable U-Hook',
            ArticleNumber::fromString('QC7-EB4'),
            [CategoryId::tools()],
            'This hook can be customized with a scad editor.',
            'Parametric hook, universal and very strong.

I’ve been looking a while for a parametric hook modeling, but I didn’t found any available model that ideally combines strength and customizing possibilities. So I tried to create the model I wanted. This is an attempt for a universal and very strong parametric hook, easily 3D printable ﻿as a "finished product".

I did a "stress test" with my parametric U-Hook in its default configuration. The hook had been printed in PLA, with 2 perimeters, 0.2 mm layer height, and 20% infill.
It supported a weight of 47,4 Kg.

Full documentation available as a PDF (in English and French) in the ﻿"Thing Files" section, or on my website :
[http://www.sergepayen.fr/en/parametric-u-hook](http://www.sergepayen.fr/en/parametric-u-hook)

To customize this object, you can use Thingiverse\'s Customizer, or OpenScad software (free and open-source).

    OpenScad is very easy to use if you just want to customize an existing SCAD file. I wrote a little tutorial about "how to use a parametric file", that explain how to use OpenScad to customize a parametric object :
[http://www.sergepayen.fr/en/how-to-use-a-parametric-file](http://www.sergepayen.fr/en/how-to-use-a-parametric-file)',
            null,
            'Creative Commons - Attribution - Non-Commercial - Share Alikelicense.',
            'https://www.thingiverse.com/thing:1367661/files',
            $output
        );

        $pieces[] = $this->createPiece(
            $user->getId(),
            PieceId::fromString('69addee9-2a1b-403b-88ac-8c6a1d5aba82'),
            'Cup holder self balancing for bike handlebar',
            ArticleNumber::fromString('G35-BIR'),
            [CategoryId::household(), CategoryId::healthcare_HomeMedicalTools(), CategoryId::models_FoodDrink()],
            "It's a nice self balancing cup holder, designed for my bicycle. It fit a medium cup, and the clamp is for 31.8 handlebar",
            "I just made my new self balancing cup holder. It's designed for a 66 mm medium cup diameter and for 31.8 bicycle handlebar",
            'Printer brand:
Creality


Printer:
Ender 5


Rafts:
No

Supports:
Yes

Resolution:
200

Infill:
40',
            'Creative Commons - Attribution Alikelicense.',
            'https://www.thingiverse.com/thing:4881012',
            $output
        );

//        $this->createPiece(
//            $user->getId(),
//            PieceId::fromString('0e639f36-11d6-4c61-9934-83df2586fca1'),
//            '',
//            ArticleNumber::fromString('ATW-MY2'),
//            '',
//            '',
//            '',
//            '',
//            '',
//            $output
//        );

        return $pieces;
    }

    /**
     * @param array<int, CategoryId> $categoryIds
     */
    private function createPiece(
        UserId $userId,
        PieceId $id,
        string $name,
        ArticleNumber $articleNumber,
        array $categoryIds,
        ?string $summary,
        ?string $description,
        ?string $manufacturingNotes,
        ?string $licence,
        ?string $sourceUrl,
        OutputInterface $output
    ): Piece {
        if (in_array($articleNumber->toString(), self::$articleNumbers, true)) {
            throw new RuntimeException(sprintf('ArticleNumber [%s] already exists', $articleNumber->toString()));
        }

        self::$articleNumbers[] = $articleNumber->toString();

        $this->messageBus->dispatch(new CreatePiece(
            $userId,
            $id,
            $articleNumber,
            $name
        ));
        $output->writeln(sprintf(
            'Created Piece [<info>%s</info>] "<info>%s</info>"  (%s)',
            $articleNumber->toString(),
            $name,
            $id->toString()
        ));

        $this->messageBus->dispatch(new UpdatePieceMetadata(
            $id,
            $name,
            $summary,
            $description,
            $manufacturingNotes,
            $licence,
            $sourceUrl,
            $categoryIds
        ));

        if ($output->isDebug()) {
            $output->writeln(sprintf('Updated Piece Metadata [<info>%s</info>]', $articleNumber->toString()));
        }

        $this->messageBus->dispatch(new RequestReview($id, ''));
        if ($output->isDebug()) {
            $output->writeln(sprintf('Review Requested for Piece [%s]', $articleNumber->toString()));
        }

        $this->messageBus->dispatch(new ApprovePiece($id, ''));
        if ($output->isDebug()) {
            $output->writeln(sprintf('Piece [%s] approved', $articleNumber->toString()));
        }

        return $this->pieceRepository->get($id);
    }

    private function createAdmin(AdminUserId $adminUserId, Email $email, OutputInterface $output): void
    {
        $createAdmin = new CreateAdminUser(
            $adminUserId,
            $email,
            self::DEFAULT_USER_PASSWORD
        );

        $this->messageBus->dispatch($createAdmin);

        $output->writeln(sprintf(
            'Created Admin: [<info>%s</info>] with password [<info>%s</info>] (%s)',
            $createAdmin->getEmail()->toString(),
            $createAdmin->getPassword(),
            $createAdmin->getAdminUserId()->toString()
        ));
    }

    private function createUser(UserId $userId, Email $email, OutputInterface $output): User
    {
        $createUser = new RegisterUser(
            $userId,
            $email,
            'John',
            'Doe'
        );

        $this->messageBus->dispatch($createUser);

        $user = $this->userRepository->get($createUser->getUserId());
        $registration = $user->getRegistration();
        if (!$registration instanceof UserRegistration) {
            throw new RuntimeException(
                sprintf(
                    'User [%s:%s] does not have a pending registration!',
                    $createUser->getUserId()->toString(),
                    $createUser->getEmail()->toString()
                )
            );
        }
        $finishRegistration = new FinishUserRegistration(
            $registration->getRegistrationToken(),
            self::DEFAULT_USER_PASSWORD
        );

        $this->messageBus->dispatch($finishRegistration);

        $output->writeln(sprintf(
            'Created User: [<info>%s</info>] with password [<info>%s</info>] (%s)',
            $user->getUsername(),
            $finishRegistration->getPassword(),
            $user->getId()->toString()
        ));

        return $user;
    }

    private function addPieceToCart(User $user, Piece $piece, int $quantity, OutputInterface $output): void
    {
        $addPieceToCart = new AddPieceToCart($user->getId(), $piece->id(), $quantity);
        $this->messageBus->dispatch($addPieceToCart);

        $output->writeln(sprintf(
            'User [<info>%s</info>] added Piece [<info>%s</info>] <info>%sx</info> to Cart',
            $user->getUsername(),
            $piece->articleNumber()->toString(),
            $quantity
        ));
    }

    private function removePieceFromCart(User $user, Piece $piece, OutputInterface $output): void
    {
        $removePieceFromCart = new RemovePieceFromCart($user->getId(), $piece->id());
        $this->messageBus->dispatch($removePieceFromCart);

        $output->writeln(sprintf(
            'User [<info>%s</info>] removed Piece [<info>%s</info>] from Cart',
            $user->getUsername(),
            $piece->articleNumber()->toString(),
        ));
    }

    private function changePieceQuantityInCart(User $user, Piece $piece, int $quantity, OutputInterface $output): void
    {
        $changePieceQuantity = new ChangePieceQuantity($user->getId(), $piece->id(), $quantity);
        $this->messageBus->dispatch($changePieceQuantity);

        $output->writeln(sprintf(
            'User [<info>%s</info>] changed Piece [<info>%s</info>] to <info>%sx</info> in Cart',
            $user->getUsername(),
            $piece->articleNumber()->toString(),
            $quantity
        ));
    }
}
