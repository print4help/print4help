<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Command;

use App\Domain\Market\Piece\ArticleNumberGenerator;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class RandomArticleNumbers extends Command
{
    protected function configure(): void
    {
        $this->setName('app:util:random:article-numbers')
            ->addArgument('limit', InputArgument::OPTIONAL, 'Amount to generate', '20')
            ->addOption('dummy', null, InputOption::VALUE_NONE, 'Use dummy generator or not');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $limit = $input->getArgument('limit');

        if (!is_numeric($limit)) {
            throw new InvalidArgumentException('limit needs to be an integer');
        }

        $limit = (int)$limit;

        if ($input->getOption('dummy')) {
            ArticleNumberGenerator::dummy();
        }

        for ($i = 0; $i < $limit; $i++) {
            $output->writeln(ArticleNumberGenerator::generate());
        }

        return 0;
    }
}
