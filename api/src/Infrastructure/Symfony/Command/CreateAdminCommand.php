<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Command;

use App\Domain\Account\Exception\UserByEmailAlreadyExists;
use App\Domain\Admin\AdminUserId;
use App\Domain\Admin\Command\CreateAdminUser;
use App\Domain\Admin\Exception\AdminUserByEmailAlreadyExists;
use App\Domain\Email;
use App\Domain\InputHelper;
use App\Infrastructure\Symfony\Security\User;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Throwable;

final class CreateAdminCommand extends Command
{
    private MessageBusInterface $messageBus;
    private UserPasswordHasherInterface $hasher;

    public function __construct(
        MessageBusInterface $messageBus,
        UserPasswordHasherInterface $hasher
    ) {
        parent::__construct();
        $this->messageBus = $messageBus;
        $this->hasher = $hasher;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:create:admin')
            ->addArgument('email', InputArgument::REQUIRED)
            ->addArgument('password', InputArgument::REQUIRED);
        $this->setDescription('Create Print4Health Admin');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = new Email(InputHelper::string($input->getArgument('email')));
        $hashedPassword = $this->hasher->hashPassword(
            User::forPasswordHashing(),
            InputHelper::string($input->getArgument('password'))
        );

        $command = new CreateAdminUser(
            AdminUserId::create(),
            $email,
            $hashedPassword,
        );

        try {
            $this->messageBus->dispatch($command);
        } catch (HandlerFailedException $exception) {
            $exception = $exception->getPrevious();

            if ($exception instanceof AdminUserByEmailAlreadyExists) {
                $output->writeln("<error>This E-Mail is already used by an Admin.</error>");

                return 1;
            }

            if ($exception instanceof UserByEmailAlreadyExists) {
                $output->writeln("<error>This E-Mail is already used by an User.</error>");

                return 2;
            }

            if ($exception instanceof Throwable) {
                $output->writeln(sprintf("<error>Error: [%s]</error>", get_class($exception)));
                $output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));
                if ($output->isDebug()) {
                    throw $exception;
                }
            }

            return 3;
        }

        $output->writeln(sprintf('Admin [%s] was created successfully.', $email->toString()));

        return 0;
    }
}
