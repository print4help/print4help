<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security;

use Symfony\Component\Validator\Constraints as AssertSymfony;
use Webmozart\Assert\Assert;

final class LoginRequest
{
    #[AssertSymfony\NotBlank()]
    #[AssertSymfony\Email()]
    #[AssertSymfony\Type(type: 'string')]
    public mixed $username;

    #[AssertSymfony\NotBlank]
    #[AssertSymfony\Type(type: 'string')]
    public mixed $password;

    /**
     * @param mixed $username
     * @param mixed $password
     */
    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getUsername(): string
    {
        Assert::string($this->username);
        Assert::stringNotEmpty($this->username);
        return $this->username;
    }

    public function getPassword(): string
    {
        Assert::string($this->password);
        Assert::stringNotEmpty($this->password);
        return $this->password;
    }
}
