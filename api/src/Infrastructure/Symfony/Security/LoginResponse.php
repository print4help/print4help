<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security;

use OpenApi\Annotations as OA;

class LoginResponse
{
    /**
     * @OA\Property(type="integer")
     */
    public int $code;

    /**
     * @OA\Property(type="string")
     */
    public string $message;

    /**
     * @var array<string, string>
     * @OA\Property(
     *      type="array",
     *      @OA\Items(
     *          @OA\Property(property="token", type="string")
     *      )
     * )
     */
    public array $data;

    public function __construct(int $code = 200, string $message = 'OK.', string $token = '')
    {
        $this->code = $code;
        $this->message = $message;
        $this->data = [
            'token' => $token,
        ];
    }
}
