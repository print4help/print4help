<?php

declare(strict_types=1);

namespace App\Infrastructure\EventSourcing\Repository;

use App\Domain\Market\Offer\Exception\OfferNotFound;
use App\Domain\Market\Offer\Offer;
use App\Domain\Market\Offer\OfferId;
use App\Domain\Market\Offer\OfferRepository as BaseOfferRepository;
use Patchlevel\EventSourcing\Repository\AggregateNotFoundException;
use Patchlevel\EventSourcing\Repository\Repository;

final class OfferRepository implements BaseOfferRepository
{
    public function __construct(private Repository $repository)
    {
    }

    public function get(OfferId $offerId): Offer
    {
        try {
            $aggregate = $this->repository->load($offerId->toString());
        } catch (AggregateNotFoundException $exception) {
            throw new OfferNotFound($offerId);
        }

        if (!$aggregate instanceof Offer) {
            throw new OfferNotFound($offerId);
        }

        return $aggregate;
    }

    public function save(Offer $offer): void
    {
        $this->repository->save($offer);
    }
}
