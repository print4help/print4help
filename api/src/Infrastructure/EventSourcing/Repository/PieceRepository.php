<?php

declare(strict_types=1);

namespace App\Infrastructure\EventSourcing\Repository;

use App\Domain\Market\Piece\Exception\PieceNotFound;
use App\Domain\Market\Piece\Piece;
use App\Domain\Market\Piece\PieceId;
use App\Domain\Market\Piece\PieceRepository as BasePieceRepository;
use Patchlevel\EventSourcing\Repository\AggregateNotFoundException;
use Patchlevel\EventSourcing\Repository\Repository;

final class PieceRepository implements BasePieceRepository
{
    public function __construct(private Repository $repository)
    {
    }

    public function get(PieceId $pieceId): Piece
    {
        try {
            $aggregate = $this->repository->load($pieceId->toString());
        } catch (AggregateNotFoundException $exception) {
            throw new PieceNotFound($pieceId);
        }

        if (!$aggregate instanceof Piece) {
            throw new PieceNotFound($pieceId);
        }

        return $aggregate;
    }

    public function save(Piece $piece): void
    {
        $this->repository->save($piece);
    }
}
