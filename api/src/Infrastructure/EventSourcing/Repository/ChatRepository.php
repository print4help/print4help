<?php

declare(strict_types=1);

namespace App\Infrastructure\EventSourcing\Repository;

use App\Domain\Market\Offer\Chat;
use App\Domain\Market\Offer\ChatId;
use App\Domain\Market\Offer\ChatRepository as BaseChatRepository;
use App\Domain\Market\Offer\Exception\ChatNotFound;
use Patchlevel\EventSourcing\Repository\AggregateNotFoundException;
use Patchlevel\EventSourcing\Repository\Repository;

final class ChatRepository implements BaseChatRepository
{
    public function __construct(private Repository $repository)
    {
    }

    public function get(ChatId $chatId): Chat
    {
        try {
            $aggregate = $this->repository->load($chatId->toString());
        } catch (AggregateNotFoundException $exception) {
            throw new ChatNotFound($chatId);
        }

        if (!$aggregate instanceof Chat) {
            throw new ChatNotFound($chatId);
        }

        return $aggregate;
    }

    public function save(Chat $chat): void
    {
        $this->repository->save($chat);
    }
}
