<?php

declare(strict_types=1);

namespace App\Infrastructure\EventSourcing\Repository;

use App\Domain\Market\Inquiry\Exception\InquiryNotFound;
use App\Domain\Market\Inquiry\Inquiry;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Inquiry\InquiryRepository as BaseInquiryRepository;
use Patchlevel\EventSourcing\Repository\AggregateNotFoundException;
use Patchlevel\EventSourcing\Repository\Repository;

final class InquiryRepository implements BaseInquiryRepository
{
    public function __construct(private Repository $repository)
    {
    }

    public function get(InquiryId $inquiryId): Inquiry
    {
        try {
            $aggregate = $this->repository->load($inquiryId->toString());
        } catch (AggregateNotFoundException $exception) {
            throw new InquiryNotFound($inquiryId);
        }

        if (!$aggregate instanceof Inquiry) {
            throw new InquiryNotFound($inquiryId);
        }

        return $aggregate;
    }

    public function save(Inquiry $inquiry): void
    {
        $this->repository->save($inquiry);
    }
}
