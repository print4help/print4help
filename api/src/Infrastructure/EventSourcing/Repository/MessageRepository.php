<?php

declare(strict_types=1);

namespace App\Infrastructure\EventSourcing\Repository;

use App\Domain\Market\Offer\Exception\MessageNotFound;
use App\Domain\Market\Offer\Message;
use App\Domain\Market\Offer\MessageId;
use App\Domain\Market\Offer\MessageRepository as BaseMessageRepository;
use Patchlevel\EventSourcing\Repository\AggregateNotFoundException;
use Patchlevel\EventSourcing\Repository\Repository;

final class MessageRepository implements BaseMessageRepository
{
    public function __construct(private Repository $repository)
    {
    }

    public function get(MessageId $messageId): Message
    {
        try {
            $aggregate = $this->repository->load($messageId->toString());
        } catch (AggregateNotFoundException $exception) {
            throw new MessageNotFound($messageId);
        }

        if (!$aggregate instanceof Message) {
            throw new MessageNotFound($messageId);
        }

        return $aggregate;
    }

    public function save(Message $message): void
    {
        $this->repository->save($message);
    }
}
