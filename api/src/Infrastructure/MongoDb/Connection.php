<?php

declare(strict_types=1);

namespace App\Infrastructure\MongoDb;

use MongoDB\Client;
use MongoDB\Collection;

final class Connection
{
    private Client $client;
    private string $name;

    public function __construct(string $uri, string $name)
    {
        $this->client = new Client($uri);
        $this->name = $name;
    }

    public function collection(string $name): Collection
    {
        return $this->client->selectCollection(
            $this->name,
            $name,
            [
                'typeMap' => [
                    'array' => 'array',
                    'document' => 'array',
                    'root' => 'array',
                ],
            ]
        );
    }
}
