<?php

declare(strict_types=1);

namespace App\Infrastructure\MongoDb\Repository;

use App\Domain\InputHelper;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\MongoDb\MongoDbHelper;
use App\Infrastructure\ReadModel\Exception\MessageByMessageIdNotFoundException;
use App\Infrastructure\ReadModel\Message;
use App\Infrastructure\ReadModel\Projection\MessageProjection;
use App\Infrastructure\ReadModel\Repository\MessageRepository as MessageRepositoryInterface;
use Webmozart\Assert\Assert;

class MessageRepository implements MessageRepositoryInterface
{
    public function __construct(private Connection $db, )
    {
    }

    public function get(string $messageId): Message
    {
        $filter = ['_id' => $messageId];
        $options = [];
        $data = $this->db->collection(MessageProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new MessageByMessageIdNotFoundException($messageId);
        }

        Assert::isArray($data);
        return $this->hydrate(MongoDbHelper::normalizeId($data));
    }

    public function findByIds(array $messageIds): array
    {
        return $this->getAllByFilter([
            '_id' => [
                '$in' => $messageIds,
            ],
        ]);
    }

    /**
     * @param array<string, mixed> $filter
     * @param array<string, mixed> $options
     * @return array<int, Message>
     */
    private function getAllByFilter(array $filter = [], array $options = []): array
    {
        $data = $this->db->collection(MessageProjection::DOCUMENT_NAME)->find($filter, $options);

        return array_map(
            fn (array $data): Message => $this->hydrate($data),
            MongoDbHelper::cursorToArray($data)
        );
    }

    /**
     * @param array<array-key, mixed> $data
     */
    private function hydrate(array $data): Message
    {
        $data = MongoDbHelper::normalizeId($data);

        $message = new Message();
        $message->id = InputHelper::string($data['id']);
        $message->chatId = InputHelper::string($data['chatId']);
        $message->authorId = InputHelper::string($data['authorId']);
        $message->authorName = InputHelper::string($data['authorName']);
        $message->body = InputHelper::string($data['body']);
        $message->subject = InputHelper::nullableString($data['subject']);
        $message->createdAt = InputHelper::string($data['createdAt']);
        $message->updatedAt = InputHelper::nullableString($data['updatedAt']);

        return $message;
    }
}
