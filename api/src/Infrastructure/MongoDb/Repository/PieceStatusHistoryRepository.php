<?php

declare(strict_types=1);

namespace App\Infrastructure\MongoDb\Repository;

use App\Domain\InputHelper;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\MongoDb\MongoDbHelper;
use App\Infrastructure\ReadModel\Exception\PieceStatusHistoryByArticleNumberNotFoundException;
use App\Infrastructure\ReadModel\Exception\PieceStatusHistoryByPieceIdNotFoundException;
use App\Infrastructure\ReadModel\PieceStatusChange;
use App\Infrastructure\ReadModel\PieceStatusHistory;
use App\Infrastructure\ReadModel\Projection\PieceStatusHistoryProjection;
use App\Infrastructure\ReadModel\Repository\PieceStatusHistoryRepository as PieceStatusHistoryRepositoryInterface;
use Webmozart\Assert\Assert;

class PieceStatusHistoryRepository implements PieceStatusHistoryRepositoryInterface
{
    public function __construct(private Connection $db)
    {
    }

    public function get(string $pieceId): PieceStatusHistory
    {
        $filter = ['_id' => $pieceId];
        $options = [];
        $data = $this->db->collection(PieceStatusHistoryProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new PieceStatusHistoryByPieceIdNotFoundException($pieceId);
        }

        Assert::isArray($data);
        return $this->hydrate($data);
    }

    public function getByArticleNumber(string $articleNumber): PieceStatusHistory
    {
        $filter = ['articleNumber' => $articleNumber];
        $options = [];
        $data = $this->db->collection(PieceStatusHistoryProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new PieceStatusHistoryByArticleNumberNotFoundException($articleNumber);
        }

        Assert::isArray($data);
        return $this->hydrate($data);
    }

    public function getAll(): array
    {
        $filter = [];
        $options = [];
        $data = $this->db->collection(PieceStatusHistoryProjection::DOCUMENT_NAME)->find($filter, $options);

        return array_map(
            fn (array $data): PieceStatusHistory => $this->hydrate($data),
            MongoDbHelper::cursorToArray($data)
        );
    }

    /**
     * @param array<array-key, mixed> $data
     */
    private function hydrate(array $data): PieceStatusHistory
    {
        $data = MongoDbHelper::normalizeId($data);

        $statusChanges = array_map(
            static fn (array $statusChange): PieceStatusChange => new PieceStatusChange(
                InputHelper::string($statusChange['status']),
                InputHelper::nullableString($statusChange['updatedAt']),
                InputHelper::nullableString($statusChange['message'])
            ),
            InputHelper::array($data['statusChanges'])
        );

        return new PieceStatusHistory(
            InputHelper::string($data['id']),
            InputHelper::string($data['userId']),
            InputHelper::string($data['articleNumber']),
            $statusChanges
        );
    }
}
