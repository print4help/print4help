<?php

declare(strict_types=1);

namespace App\Infrastructure\MongoDb\Repository;

use App\Domain\InputHelper;
use App\Domain\Market\Inquiry\InquiryStatus;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\MongoDb\MongoDbHelper;
use App\Infrastructure\ReadModel\Exception\InquiryByInquiryIdNotFoundException;
use App\Infrastructure\ReadModel\Inquiry;
use App\Infrastructure\ReadModel\Projection\InquiryProjection;
use App\Infrastructure\ReadModel\Repository\InquiryRepository as InquiryRepositoryInterface;
use Webmozart\Assert\Assert;

class InquiryRepository implements InquiryRepositoryInterface
{
    public function __construct(
        private Connection $db,
        private OfferRepository $offerRepository
    ) {
    }

    public function get(string $inquiryId): Inquiry
    {
        $filter = ['_id' => $inquiryId];
        $options = [];
        $data = $this->db->collection(InquiryProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new InquiryByInquiryIdNotFoundException($inquiryId);
        }

        Assert::isArray($data);
        return $this->hydrate($data);
    }

    public function getWithOffers(string $inquiryId): Inquiry
    {
        $inquiry = $this->get($inquiryId);
        $inquiry->offers = $this->offerRepository->findByIds($inquiry->offerIds);

        return $inquiry;
    }

    /**
     * @return array<int, Inquiry>
     */
    public function getAllOpen(): array
    {
        return $this->getAllByFilter(['status' => InquiryStatus::open()->toString()]);
    }

    /**
     * @return array<int, Inquiry>
     */
    public function getByUser(string $userId): array
    {
        return $this->getAllByFilter(['userId' => $userId]);
    }

    /**
     * @param array<string, mixed> $filter
     * @return array<int, Inquiry>
     */
    private function getAllByFilter(array $filter = []): array
    {
        $options = [];
        $data = $this->db->collection(InquiryProjection::DOCUMENT_NAME)->find($filter, $options);

        return array_map(
            fn (array $data): Inquiry => $this->hydrate($data),
            MongoDbHelper::cursorToArray($data)
        );
    }

    /**
     * @param array<array-key, mixed> $data
     */
    private function hydrate(array $data): Inquiry
    {
        $data = MongoDbHelper::normalizeId($data);

        $inquiry = new Inquiry();
        $inquiry->id = InputHelper::string($data['id']);
        $inquiry->userId = InputHelper::string($data['userId']);
        $inquiry->pieceId = InputHelper::string($data['pieceId']);
        $inquiry->articleNumber = InputHelper::string($data['articleNumber']);
        $inquiry->status = InputHelper::string($data['status']);
        $inquiry->offerIds = InputHelper::arrayOfStrings($data['offerIds']);
        $inquiry->offers = [];
        $inquiry->quantity = InputHelper::int($data['quantity']);
        $inquiry->purpose = InputHelper::nullableString($data['purpose']);
        $inquiry->requirements = InputHelper::arrayOfStrings($data['requirements']);
        $inquiry->createdAt = InputHelper::string($data['createdAt']);
        $inquiry->updatedAt = InputHelper::nullableString($data['updatedAt']);
        $inquiry->inExecutionAt = InputHelper::nullableString($data['inExecutionAt']);
        $inquiry->productionStartedAt = InputHelper::nullableString($data['productionStartedAt']);
        $inquiry->productionAbortedAt = InputHelper::nullableString($data['productionAbortedAt']);
        $inquiry->productionFinishedAt = InputHelper::nullableString($data['productionFinishedAt']);
        $inquiry->shippedAt = InputHelper::nullableString($data['shippedAt']);
        $inquiry->completedAt = InputHelper::nullableString($data['completedAt']);

        return $inquiry;
    }
}
