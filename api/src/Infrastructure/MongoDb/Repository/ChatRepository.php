<?php

declare(strict_types=1);

namespace App\Infrastructure\MongoDb\Repository;

use App\Domain\InputHelper;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\MongoDb\MongoDbHelper;
use App\Infrastructure\ReadModel\Chat;
use App\Infrastructure\ReadModel\Exception\ChatByChatIdNotFoundException;
use App\Infrastructure\ReadModel\Exception\ChatByOfferIdNotFoundException;
use App\Infrastructure\ReadModel\Projection\ChatProjection;
use App\Infrastructure\ReadModel\Repository\ChatRepository as ChatRepositoryInterface;
use App\Infrastructure\ReadModel\Repository\MessageRepository;
use Webmozart\Assert\Assert;

class ChatRepository implements ChatRepositoryInterface
{
    public function __construct(
        private Connection $db,
        private MessageRepository $messageRepository
    ) {
    }

    public function get(string $chatId): Chat
    {
        $filter = ['_id' => $chatId];
        $options = [];
        $data = $this->db->collection(ChatProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new ChatByChatIdNotFoundException($chatId);
        }

        Assert::isArray($data);
        return $this->hydrate(MongoDbHelper::normalizeId($data));
    }

    public function getByOfferId(string $offerId): Chat
    {
        $filter = ['offerId' => $offerId];
        $options = [];
        $data = $this->db->collection(ChatProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new ChatByOfferIdNotFoundException($offerId);
        }

        Assert::isArray($data);
        return $this->hydrate(MongoDbHelper::normalizeId($data));
    }

    public function findByIds(array $chatIds): array
    {
        return $this->getAllByFilter([
            '_id' => [
                '$in' => $chatIds,
            ],
        ]);
    }

    public function getWithMessages(string $chatId): Chat
    {
        $inquiry = $this->get($chatId);
        $inquiry->messages = $this->messageRepository->findByIds($inquiry->messageIds);

        return $inquiry;
    }

    public function findByUser(string $userId, bool $withMessages = false): array
    {
        $chats = $this->getAllByFilter([
            'userIds' => $userId,
        ]);

        if ($withMessages) {
            $chats = array_map(function (Chat $chat): Chat {
                $chat->messages = $this->messageRepository->findByIds($chat->messageIds);
                return $chat;
            }, $chats);
        }

        return $chats;
    }

    /**
     * @param array<string, mixed> $filter
     * @param array<string, mixed> $options
     * @return array<int, Chat>
     */
    private function getAllByFilter(array $filter = [], array $options = []): array
    {
        $data = $this->db->collection(ChatProjection::DOCUMENT_NAME)->find($filter, $options);

        return array_map(
            fn (array $data): Chat => $this->hydrate($data),
            MongoDbHelper::cursorToArray($data)
        );
    }

    /**
     * @param array<array-key, mixed> $data
     */
    private function hydrate(array $data): Chat
    {
        $data = MongoDbHelper::normalizeId($data);

        $chat = new Chat();
        $chat->id = InputHelper::string($data['id']);
        $chat->status = InputHelper::string($data['status']);
        $chat->offerId = InputHelper::string($data['offerId']);
        $chat->userIds = InputHelper::arrayOfStrings($data['userIds']);
        $chat->subject = InputHelper::string($data['subject']);
        $chat->messageIds = InputHelper::arrayOfStrings($data['messageIds']);
        $chat->messages = [];
        $chat->createdAt = InputHelper::string($data['createdAt']);
        $chat->updatedAt = InputHelper::nullableString($data['updatedAt']);
        $chat->closedAt = InputHelper::nullableString($data['closedAt']);

        return $chat;
    }
}
