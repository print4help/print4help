<?php

declare(strict_types=1);

namespace App\Infrastructure\MongoDb\Repository;

use App\Domain\InputHelper;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\MongoDb\MongoDbHelper;
use App\Infrastructure\ReadModel\Exception\OfferByInquiryIdAndUserIdNotFoundException;
use App\Infrastructure\ReadModel\Exception\OfferByOfferIdNotFoundException;
use App\Infrastructure\ReadModel\Offer;
use App\Infrastructure\ReadModel\Projection\OfferProjection;
use App\Infrastructure\ReadModel\Repository\OfferRepository as OfferRepositoryInterface;
use Webmozart\Assert\Assert;

class OfferRepository implements OfferRepositoryInterface
{
    public function __construct(private Connection $db)
    {
    }

    public function get(string $offerId): Offer
    {
        $filter = ['_id' => $offerId];
        $options = [];
        $data = $this->db->collection(OfferProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new OfferByOfferIdNotFoundException($offerId);
        }

        Assert::isArray($data);
        return $this->hydrate(MongoDbHelper::normalizeId($data));
    }

    public function findByIds(array $offerIds): array
    {
        return $this->getAllByFilter([
            '_id' => [
                '$in' => $offerIds,
            ],
        ]);
    }

    /**
     * @param array<string, mixed> $filter
     * @param array<string, mixed> $options
     * @return array<int, Offer>
     */
    private function getAllByFilter(array $filter = [], array $options = []): array
    {
        $data = $this->db->collection(OfferProjection::DOCUMENT_NAME)->find($filter, $options);

        return array_map(
            fn (array $data): Offer => $this->hydrate($data),
            MongoDbHelper::cursorToArray($data)
        );
    }

    /**
     * @return array<int, Offer>
     */
    public function getByUser(string $userId): array
    {
        return $this->getAllByFilter(['userId' => $userId]);
    }

    /**
     * @return array<int, Offer>
     */
    public function getByInquiry(string $inquiryId): array
    {
        return $this->getAllByFilter(['inquiryId' => $inquiryId]);
    }

    public function getByInquiryAndUser(string $inquiryId, string $userId): Offer
    {
        $filter = [
            'inquiryId' => $inquiryId,
            'userId' => $userId,
        ];
        $options = [];
        $data = $this->db->collection(OfferProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new OfferByInquiryIdAndUserIdNotFoundException($inquiryId, $userId);
        }

        Assert::isArray($data);
        return $this->hydrate(MongoDbHelper::normalizeId($data));
    }

    /**
     * @param array<array-key, mixed> $data
     */
    private function hydrate(array $data): Offer
    {
        $data = MongoDbHelper::normalizeId($data);

        $inquiry = new Offer();
        $inquiry->id = InputHelper::string($data['id']);
        $inquiry->status = InputHelper::string($data['status']);
        $inquiry->inquiryId = InputHelper::string($data['inquiryId']);
        $inquiry->userId = InputHelper::string($data['userId']);
        $inquiry->pieceId = InputHelper::string($data['pieceId']);
        $inquiry->quantity = InputHelper::int($data['quantity']);
        $inquiry->message = InputHelper::string($data['message']);
        $inquiry->createdAt = InputHelper::string($data['createdAt']);
        $inquiry->updatedAt = InputHelper::nullableString($data['updatedAt']);
        $inquiry->acceptedAt = InputHelper::nullableString($data['acceptedAt']);
        $inquiry->rejectedAt = InputHelper::nullableString($data['rejectedAt']);
        $inquiry->withdrawnAt = InputHelper::nullableString($data['withdrawnAt']);

        return $inquiry;
    }
}
