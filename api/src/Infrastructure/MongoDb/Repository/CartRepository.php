<?php

declare(strict_types=1);

namespace App\Infrastructure\MongoDb\Repository;

use App\Domain\InputHelper;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\MongoDb\MongoDbHelper;
use App\Infrastructure\ReadModel\Cart;
use App\Infrastructure\ReadModel\CartItem;
use App\Infrastructure\ReadModel\Exception\CartByCartIdNotFoundException;
use App\Infrastructure\ReadModel\Exception\CartByUserIdIdNotFoundException;
use App\Infrastructure\ReadModel\Projection\CartProjection;
use App\Infrastructure\ReadModel\Repository\CartRepository as CartRepositoryInterface;
use Webmozart\Assert\Assert;

class CartRepository implements CartRepositoryInterface
{
    public function __construct(private Connection $db)
    {
    }

    public function get(string $cartId): Cart
    {
        $filter = ['_id' => $cartId];
        $options = [];
        $data = $this->db->collection(CartProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new CartByCartIdNotFoundException($cartId);
        }

        Assert::isArray($data);
        return $this->hydrate($data);
    }

    public function getByUserId(string $userId): Cart
    {
        $filter = ['userId' => $userId];
        $options = [];
        $data = $this->db->collection(CartProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new CartByUserIdIdNotFoundException($userId);
        }

        Assert::isArray($data);
        return $this->hydrate($data);
    }

    public function getAll(): array
    {
        return $this->getAllByFilter();
    }

    /**
     * @param array<string, mixed> $filter
     * @return array<int, Cart>
     */
    private function getAllByFilter(array $filter = []): array
    {
        $options = [];
        $data = $this->db->collection(CartProjection::DOCUMENT_NAME)->find($filter, $options);

        return array_map(
            fn (array $data): Cart => $this->hydrate($data),
            MongoDbHelper::cursorToArray($data)
        );
    }


    /**
     * @param array<array-key, mixed> $data
     */
    private function hydrate(array $data): Cart
    {
        $data = MongoDbHelper::normalizeId($data);

        $cartItems = array_map(
            fn (array $data): CartItem => new CartItem(
                InputHelper::string($data['pieceId']),
                InputHelper::int($data['quantity']),
                InputHelper::nullableString($data['purpose'] ?? null),
                InputHelper::nullableArray($data['requirements'] ?? null)
            ),
            InputHelper::array($data['cartItems'])
        );

        return new Cart(
            InputHelper::string($data['id']),
            InputHelper::string($data['userId']),
            $cartItems
        );
    }
}
