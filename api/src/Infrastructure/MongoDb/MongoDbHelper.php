<?php declare(strict_types=1);

namespace App\Infrastructure\MongoDb;

use MongoDB\Driver\Cursor;
use Webmozart\Assert\Assert;

final class MongoDbHelper
{
    /**
     * @return array<int, array<array-key, mixed>>
     */
    public static function cursorToArray(Cursor $cursor): array
    {
        $result = [];

        foreach ($cursor->toArray() as $row) {
            Assert::isArray($row);
            $result[] = self::normalizeId($row);
        }

        return $result;
    }

    /**
     * @param array<array-key, mixed> $data
     * @return array<array-key, mixed>
     */
    public static function normalizeId(array $data): array
    {
        if (array_key_exists('_id', $data)) {
            $data['id'] = (string)$data['_id'];
            unset($data['_id']);
        }

        return $data;
    }
}
