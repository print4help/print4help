<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

class PieceStatusHistory
{
    public string $id;

    public string $userId;

    public string $articleNumber;

    /**
     * @var PieceStatusChange[]
     */
    public array $statusChanges = [];

    /**
     * @param PieceStatusChange[] $statusChanges
     */
    public function __construct(
        string $id,
        string $userId,
        string $articleNumber,
        array $statusChanges
    ) {
        $this->id = $id;
        $this->userId = $userId;
        $this->articleNumber = $articleNumber;
        $this->statusChanges = $statusChanges;
    }
}
