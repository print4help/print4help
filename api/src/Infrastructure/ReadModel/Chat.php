<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Chat
{
    public string $id;

    public string $status;

    public string $offerId;

    /**
     * @var array<array-key, string>
     * @OA\Property(
     *      property="messageIds",
     *      type="array",
     *      @OA\Items(
     *          type="string", example="uuid"
     *      )
     * )
     */
    public array $messageIds;

    /**
     * @var array<array-key, string>
     * @OA\Property(
     *      property="userIds",
     *      type="array",
     *      @OA\Items(
     *          type="string", example="uuid"
     *      )
     * )
     */
    public array $userIds;

    /**
     * @var array <array-key, Message>
     * @OA\Property(
     *      property="messages",
     *      type="array",
     *      @OA\Items(
     *          ref=@Model(type=App\Infrastructure\ReadModel\Message::class)
     *      )
     * )
     */
    public array $messages;

    public string $subject;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public string $createdAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $updatedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $closedAt;
}
