<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use OpenApi\Annotations as OA;

class Message
{
    public string $id;

    public string $chatId;

    public string $authorId;

    public string $authorName;

    public string $body;

    public ?string $subject;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public string $createdAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $updatedAt;
}
