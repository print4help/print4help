<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

class ReadModelNotFoundException extends ReadModelException
{
}
