<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelNotFoundException;

class NoAcceptedOfferInInquiryException extends ReadModelNotFoundException
{
    public function __construct(string $inquriyId)
    {
        parent::__construct(sprintf('Inquiry with id [%s] does not have an accepted offer', $inquriyId));
    }
}
