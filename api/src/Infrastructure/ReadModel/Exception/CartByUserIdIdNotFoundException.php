<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelNotFoundException;

class CartByUserIdIdNotFoundException extends ReadModelNotFoundException
{
    public function __construct(string $userId)
    {
        parent::__construct(sprintf('Cart for User id [%s] was not found', $userId));
    }
}
