<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelNotFoundException;

class ChatByChatIdNotFoundException extends ReadModelNotFoundException
{
    public function __construct(string $id)
    {
        parent::__construct(sprintf('Chat with id [%s] was not found', $id));
    }
}
