<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelNotFoundException;

class OfferByInquiryIdAndUserIdNotFoundException extends ReadModelNotFoundException
{
    public function __construct(string $inquiryId, string $userId)
    {
        parent::__construct(sprintf('Offer for Inquiry [%s] by User [%s] was not found', $inquiryId, $userId));
    }
}
