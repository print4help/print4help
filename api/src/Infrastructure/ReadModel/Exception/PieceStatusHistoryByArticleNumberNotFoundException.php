<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelNotFoundException;

class PieceStatusHistoryByArticleNumberNotFoundException extends ReadModelNotFoundException
{
    public function __construct(string $articleNumber)
    {
        parent::__construct(sprintf('PieceStatusHistory with articleNumber [%s] was not found', $articleNumber));
    }
}
