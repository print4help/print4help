<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelNotFoundException;

class PieceStatusHistoryByPieceIdNotFoundException extends ReadModelNotFoundException
{
    public function __construct(string $id)
    {
        parent::__construct(sprintf('PieceStatusHistory with id [%s] was not found', $id));
    }
}
