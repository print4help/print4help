<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelNotFoundException;

class MessageByMessageIdNotFoundException extends ReadModelNotFoundException
{
    public function __construct(string $id)
    {
        parent::__construct(sprintf('Message with id [%s] was not found', $id));
    }
}
