<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelNotFoundException;

class CartByCartIdNotFoundException extends ReadModelNotFoundException
{
    public function __construct(string $cartId)
    {
        parent::__construct(sprintf('Cart with id [%s] was not found', $cartId));
    }
}
