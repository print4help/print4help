<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelNotFoundException;

class PieceByArticleNumberNotFoundException extends ReadModelNotFoundException
{
    public function __construct(string $articleNumber)
    {
        parent::__construct(sprintf('Piece with articleNumber [%s] was not found', $articleNumber));
    }
}
