<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use OpenApi\Annotations as OA;

class CartItem
{
    public string $pieceId;
    public int $quantity;
    public ?string $purpose;

    /**
     * @OA\Property(
     *     type="array",
     *     @OA\Items(type="string"),
     *     example="['Color: Red', 'Material: any']"
     * )
     * @var array<int, string>|null
     */
    public ?array $requirements;

    /**
     * @param array<int, string>|null $requirements
     */
    public function __construct(
        string $pieceId,
        int $quantity,
        ?string $purpose = null,
        ?array $requirements = null
    ) {
        $this->pieceId = $pieceId;
        $this->quantity = $quantity;
        $this->purpose = $purpose;
        $this->requirements = $requirements;
    }
}
