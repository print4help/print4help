<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use RuntimeException;

class ReadModelException extends RuntimeException
{
}
