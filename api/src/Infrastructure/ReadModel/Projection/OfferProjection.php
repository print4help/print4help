<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Projection;

use App\Domain\Market\Offer\Event\OfferAccepted;
use App\Domain\Market\Offer\Event\OfferChanged;
use App\Domain\Market\Offer\Event\OfferCreated;
use App\Domain\Market\Offer\Event\OfferRejected;
use App\Domain\Market\Offer\Event\OfferWithdrawn;
use App\Infrastructure\MongoDb\Connection;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Projection\Projection;

class OfferProjection implements Projection
{
    public const DOCUMENT_NAME = 'offer';

    public function __construct(private Connection $db)
    {
    }

    public function handledEvents(): iterable
    {
        yield OfferCreated::class => 'handleOfferCreated';
        yield OfferChanged::class => 'handleOfferChanged';
        yield OfferAccepted::class => 'handleOfferAccepted';
        yield OfferRejected::class => 'handleOfferRejected';
        yield OfferWithdrawn::class => 'handleOfferWithdrawn';
    }

    public function handleOfferCreated(OfferCreated $event): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->insertOne([
            '_id' => $event->offerId()->toString(),
            'userId' => $event->userId()->toString(),
            'inquiryId' => $event->inquiryId()->toString(),
            'pieceId' => $event->pieceId()->toString(),
            'status' => $event->status()->toString(),
            'quantity' => $event->quantity(),
            'message' => $event->message(),
            'createdAt' => $event->createdAt()->format(DateTimeImmutable::ATOM),
            'updatedAt' => null,
            'acceptedAt' => null,
            'rejectedAt' => null,
            'withdrawnAt' => null,
        ]);
    }

    public function handleOfferChanged(OfferChanged $event): void
    {
        $filter = ['_id' => $event->offerId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'quantity' => $event->quantity(),
                    'message' => $event->message(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleOfferAccepted(OfferAccepted $event): void
    {
        $filter = ['_id' => $event->offerId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'acceptedAt' => $event->acceptedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleOfferRejected(OfferRejected $event): void
    {
        $filter = ['_id' => $event->offerId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'rejectedAt' => $event->rejectedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleOfferWithdrawn(OfferWithdrawn $event): void
    {
        $filter = ['_id' => $event->offerId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'withdrawnAt' => $event->withdrawnAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function create(): void
    {
        // not needed for mongodb
    }

    public function drop(): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->drop();
    }
}
