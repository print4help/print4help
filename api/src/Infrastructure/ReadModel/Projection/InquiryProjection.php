<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Projection;

use App\Domain\Market\Inquiry\Event\Closed;
use App\Domain\Market\Inquiry\Event\Completed;
use App\Domain\Market\Inquiry\Event\InquiryCreated;
use App\Domain\Market\Inquiry\Event\Locked;
use App\Domain\Market\Inquiry\Event\MetadataUpdated;
use App\Domain\Market\Inquiry\Event\OfferAdded;
use App\Domain\Market\Inquiry\Event\OfferRemoved;
use App\Domain\Market\Inquiry\Event\ProductionAborted;
use App\Domain\Market\Inquiry\Event\ProductionFinished;
use App\Domain\Market\Inquiry\Event\ProductionStarted;
use App\Domain\Market\Inquiry\Event\Shipped;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Projection\Projection;

class InquiryProjection implements Projection
{
    public const DOCUMENT_NAME = 'inquiry';

    public function __construct(
        private Connection $db,
        private PieceRepository $pieceRepository
    ) {
    }

    public function handledEvents(): iterable
    {
        yield InquiryCreated::class => 'handleInquiryCreated';
        yield MetadataUpdated::class => 'handleMetadataUpdated';
        yield Closed::class => 'handleClosed';
        yield OfferAdded::class => 'handleOfferAdded';
        yield OfferRemoved::class => 'handleOfferRemoved';
        yield Locked::class => 'handleLocked';
        yield ProductionStarted::class => 'handleProductionStarted';
        yield ProductionAborted::class => 'handleProductionAborted';
        yield ProductionFinished::class => 'handleProductionFinished';
        yield Shipped::class => 'handleShipped';
        yield Completed::class => 'handleCompleted';
    }

    public function handleInquiryCreated(InquiryCreated $event): void
    {
        $piece = $this->pieceRepository->get($event->pieceId()->toString());

        $this->db->collection(self::DOCUMENT_NAME)->insertOne([
            '_id' => $event->inquiryId()->toString(),
            'userId' => $event->userId()->toString(),
            'pieceId' => $event->pieceId()->toString(),
            'articleNumber' => $piece->articleNumber,
            'status' => $event->status()->toString(),
            'offerIds' => [],
            'quantity' => $event->quantity(),
            'purpose' => $event->purpose(),
            'requirements' => $event->requirements(),
            'createdAt' => $event->createdAt()->format(DateTimeImmutable::ATOM),
            'updatedAt' => null,
            'inExecutionAt' => null,
            'productionStartedAt' => null,
            'productionFinishedAt' => null,
            'productionAbortedAt' => null,
            'shippedAt' => null,
            'completedAt' => null,
        ]);
    }

    public function handleMetadataUpdated(MetadataUpdated $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'quantity' => $event->quantity(),
                    'purpose' => $event->purpose(),
                    'requirements' => $event->requirements(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleClosed(Closed $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleOfferAdded(OfferAdded $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$push' => [
                    'offerIds' => $event->offerId()->toString(),
                ],
            ]
        );
    }

    public function handleOfferRemoved(OfferRemoved $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$pull' => [
                    'offerIds' => $event->offerId()->toString(),
                ],
            ]
        );
    }

    public function handleLocked(Locked $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->inExecutionAt()->format(DateTimeImmutable::ATOM),
                    'inExecutionAt' => $event->inExecutionAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleProductionStarted(ProductionStarted $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->productionStartedAt()->format(DateTimeImmutable::ATOM),
                    'productionStartedAt' => $event->productionStartedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleProductionAborted(ProductionAborted $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->productionAbortedAt()->format(DateTimeImmutable::ATOM),
                    'productionAbortedAt' => $event->productionAbortedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleProductionFinished(ProductionFinished $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->productionFinishedAt()->format(DateTimeImmutable::ATOM),
                    'productionFinishedAt' => $event->productionFinishedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleShipped(Shipped $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->shippedAt()->format(DateTimeImmutable::ATOM),
                    'shippedAt' => $event->shippedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleCompleted(Completed $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->completedAt()->format(DateTimeImmutable::ATOM),
                    'completedAt' => $event->completedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function create(): void
    {
        // not needed for mongodb
    }

    public function drop(): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->drop();
    }
}
