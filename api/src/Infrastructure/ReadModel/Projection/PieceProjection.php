<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Projection;

use App\Domain\Market\Piece\CategoryRepository;
use App\Domain\Market\Piece\Event\MetadataUpdated;
use App\Domain\Market\Piece\Event\PieceApproved;
use App\Domain\Market\Piece\Event\PieceBlocked;
use App\Domain\Market\Piece\Event\PieceCreated;
use App\Domain\Market\Piece\Event\PieceRejected;
use App\Domain\Market\Piece\Event\PieceReviewRequested;
use App\Infrastructure\MongoDb\Connection;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Projection\Projection;

class PieceProjection implements Projection
{
    public const DOCUMENT_NAME = 'piece';

    public function __construct(
        private Connection $db,
        private CategoryRepository $categoryRepository
    ) {
    }

    public function handledEvents(): iterable
    {
        yield PieceCreated::class => 'handlePieceCreated';
        yield MetadataUpdated::class => 'handleMetadataUpdated';
        yield PieceReviewRequested::class => 'handleReviewRequested';
        yield PieceApproved::class => 'handlePieceApproved';
        yield PieceRejected::class => 'handlePieceRejected';
        yield PieceBlocked::class => 'handlePieceBlocked';
    }

    public function handlePieceCreated(PieceCreated $event): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->insertOne([
            '_id' => $event->pieceId()->toString(),
            'userId' => $event->userId()->toString(),
            'articleNumber' => $event->articleNumber()->toString(),
            'status' => $event->status()->toString(),
            'name' => $event->name(),
            'summary' => null,
            'description' => null,
            'manufacturingNotes' => null,
            'licence' => null,
            'sourceUrl' => null,
            'categoryIds' => [],
            'createdAt' => $event->createdAt()->format(DateTimeImmutable::ATOM),
            'updatedAt' => null,
        ]);
    }

    public function handleMetadataUpdated(MetadataUpdated $event): void
    {
        $filter = ['_id' => $event->pieceId()->toString()];

        $categoryIdsWithAllParentIds = $this->categoryRepository->getCategoryIdsWithAllParentIds($event->categoryIds());

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'name' => $event->name(),
                    'summary' => $event->summary(),
                    'description' => $event->description(),
                    'manufacturingNotes' => $event->manufacturingNotes(),
                    'licence' => $event->licence(),
                    'categoryIds' => $categoryIdsWithAllParentIds,
                    'sourceUrl' => $event->sourceUrl(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleReviewRequested(PieceReviewRequested $event): void
    {
        $filter = ['_id' => $event->pieceId()->toString()];
        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handlePieceApproved(PieceApproved $event): void
    {
        $filter = ['_id' => $event->pieceId()->toString()];
        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handlePieceRejected(PieceRejected $event): void
    {
        $filter = ['_id' => $event->pieceId()->toString()];
        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handlePieceBlocked(PieceBlocked $event): void
    {
        $filter = ['_id' => $event->pieceId()->toString()];
        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function create(): void
    {
        // not needed for mongodb
    }

    public function drop(): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->drop();
    }
}
