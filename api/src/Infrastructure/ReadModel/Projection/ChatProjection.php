<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Projection;

use App\Domain\Account\UserId;
use App\Domain\Market\Offer\Event\ChatClosed;
use App\Domain\Market\Offer\Event\ChatCreated;
use App\Domain\Market\Offer\Event\MessageAdded;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\ReadModel\Repository\MessageRepository;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Projection\Projection;

class ChatProjection implements Projection
{
    public const DOCUMENT_NAME = 'chat';

    public function __construct(
        private Connection $db,
        private MessageRepository $messageRepository
    ) {
    }

    public function handledEvents(): iterable
    {
        yield ChatCreated::class => 'handleChatCreated';
        yield ChatClosed::class => 'handleChatClosed';
        yield MessageAdded::class => 'handleMessageAdded';
    }

    public function handleChatCreated(ChatCreated $event): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->insertOne([
            '_id' => $event->chatId()->toString(),
            'offerId' => $event->offerId()->toString(),
            'userIds' => array_map(
                static fn (UserId $userId): string => $userId->toString(),
                $event->userIds()
            ),
            'status' => $event->status()->toString(),
            'subject' => $event->subject(),
            'messageIds' => [],
            'createdAt' => $event->createdAt()->format(DateTimeImmutable::ATOM),
            'updatedAt' => null,
            'closedAt' => null,
        ]);
    }

    public function handleChatClosed(ChatClosed $event): void
    {
        $filter = ['_id' => $event->chatId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'closedAt' => $event->closedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleMessageAdded(MessageAdded $event): void
    {
        $filter = ['_id' => $event->chatId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$push' => [
                    'messageIds' => $event->messageId()->toString(),
                ],
            ]
        );
    }

    public function create(): void
    {
        // not needed for mongodb
    }

    public function drop(): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->drop();
    }
}
