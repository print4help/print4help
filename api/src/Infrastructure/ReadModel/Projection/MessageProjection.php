<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Projection;

use App\Domain\Market\Offer\Event\MessageCreated;
use App\Domain\Market\Offer\Event\MessageUpdated;
use App\Infrastructure\Doctrine\ORM\Repository\UserRepository;
use App\Infrastructure\MongoDb\Connection;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Projection\Projection;

class MessageProjection implements Projection
{
    public const DOCUMENT_NAME = 'message';

    public function __construct(
        private Connection $db,
        private UserRepository $userRepository
    ) {
    }

    public function handledEvents(): iterable
    {
        yield MessageCreated::class => 'handleMessageCreated';
        yield MessageUpdated::class => 'handleMessageUpdated';
    }

    public function handleMessageCreated(MessageCreated $event): void
    {
        $author = $this->userRepository->get($event->authorId());

        $this->db->collection(self::DOCUMENT_NAME)->insertOne([
            '_id' => $event->messageId()->toString(),
            'chatId' => $event->chatId()->toString(),
            'authorId' => $event->authorId()->toString(),
            'authorName' => $author->getUsername(),
            'body' => $event->body(),
            'subject' => $event->subject(),
            'createdAt' => $event->createdAt()->format(DateTimeImmutable::ATOM),
            'updatedAt' => null,
        ]);
    }

    public function handleMessageUpdated(MessageUpdated $event): void
    {
        $filter = ['_id' => $event->messageId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'body' => $event->body(),
                    'subject' => $event->subject(),
                    'updatedat' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function create(): void
    {
        // not needed for mongodb
    }

    public function drop(): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->drop();
    }
}
