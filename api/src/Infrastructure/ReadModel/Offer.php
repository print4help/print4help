<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use OpenApi\Annotations as OA;

class Offer
{
    public string $id;

    public string $status;

    public string $userId;

    public string $inquiryId;

    public string $pieceId;

    public string $message;

    public int $quantity;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public string $createdAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $updatedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $acceptedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $rejectedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $withdrawnAt;
}
