<?php
declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Infrastructure\ReadModel\Exception\OfferByInquiryIdAndUserIdNotFoundException;
use App\Infrastructure\ReadModel\Exception\OfferByOfferIdNotFoundException;
use App\Infrastructure\ReadModel\Offer;

interface OfferRepository
{
    /**
     * @throws OfferByOfferIdNotFoundException
     */
    public function get(string $offerId): Offer;

    /**
     * @param array<array-key, string> $offerIds
     * @return array<array-key, Offer>
     */
    public function findByIds(array $offerIds): array;

    /**
     * @return Offer[]
     */
    public function getByUser(string $userId): array;

    /**
     * @return Offer[]
     */
    public function getByInquiry(string $inquiryId): array;

    /**
     * @throws OfferByInquiryIdAndUserIdNotFoundException
     */
    public function getByInquiryAndUser(string $inquiryId, string$userId): Offer;
}
