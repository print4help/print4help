<?php
declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Infrastructure\ReadModel\Chat;
use App\Infrastructure\ReadModel\Exception\ChatByChatIdNotFoundException;
use App\Infrastructure\ReadModel\Exception\ChatByOfferIdNotFoundException;

interface ChatRepository
{
    /**
     * @throws ChatByChatIdNotFoundException
     */
    public function get(string $chatId): Chat;

    /**
     * @param array<array-key, string> $chatIds
     * @return array<array-key, Chat>
     */
    public function findByIds(array $chatIds): array;

    public function getWithMessages(string $chatId): Chat;

    /**
     * @return array<array-key, Chat>
     */
    public function findByUser(string $userId, bool $withMessages = false): array;

    /**
     * @throws ChatByOfferIdNotFoundException
     */
    public function getByOfferId(string $offerId): Chat;
}
