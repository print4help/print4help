<?php
declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Infrastructure\ReadModel\Exception\MessageByMessageIdNotFoundException;
use App\Infrastructure\ReadModel\Message;

interface MessageRepository
{
    /**
     * @throws MessageByMessageIdNotFoundException
     */
    public function get(string $messageId): Message;

    /**
     * @param array<array-key, string> $messageIds
     * @return array<array-key, Message>
     */
    public function findByIds(array $messageIds): array;
}
