<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use App\Domain\Market\Offer\OfferStatus;
use App\Infrastructure\ReadModel\Exception\NoAcceptedOfferInInquiryException;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Inquiry
{
    public string $id;

    public string $userId;

    public string $pieceId;

    public string $articleNumber;

    public string $status;

    public int $quantity;

    public ?string $purpose;

    /**
     * @var array<array-key, string>
     * @OA\Property(
     *      property="requirements",
     *      type="array",
     *      @OA\Items(
     *          type="string", example="Color: red"
     *      )
     * )
     */
    public array $requirements;

    /**
     * @var array<array-key, string>
     * @OA\Property(
     *      property="offerIds",
     *      type="array",
     *      @OA\Items(
     *          type="string", example="uuid"
     *      )
     * )
     */
    public array $offerIds;

    /**
     * @var array<array-key, Offer>
     * @OA\Property(
     *      property="offers",
     *      type="array",
     *      @OA\Items(
     *          ref=@Model(type=App\Infrastructure\ReadModel\Offer::class)
     *      )
     * )
     */
    public array $offers;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public string $createdAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $updatedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $inExecutionAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $productionStartedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $productionAbortedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $productionFinishedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $shippedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $completedAt;

    public function acceptedOffer(): Offer
    {
        $acceptedOffers = array_values(array_filter(
            $this->offers,
            static fn (Offer $offer): bool => $offer->status === OfferStatus::accepted()->toString()
        ));

        if (empty($acceptedOffers)) {
            throw new NoAcceptedOfferInInquiryException($this->id);
        }

        return $acceptedOffers[0];
    }
}
