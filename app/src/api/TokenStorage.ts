import jwtDecode, { JwtPayload } from 'jwt-decode';
import { DateTime } from 'luxon';

export const EVENT_LOGGED_IN = 'LOGGED_IN';
export const EVENT_LOGGED_OUT = 'LOGGED_OUT';
export const EVENT_EXPIRED = 'EXPIRED';

class TokenStorage {
  private handlers: ((event: string) => void)[];
  private timeout: NodeJS.Timeout | null;

  constructor() {
    this.timeout = null;
    this.handlers = [];

    const token = this.getToken();

    if (!token) {
      return;
    }

    this.startTimeout(token);
  }

  getToken() {
    const token = localStorage.getItem('token');

    if (!token) {
      return null;
    }

    if (this.isTokenExpired(token)) {
      this.expired();

      return null;
    }

    return token;
  }

  getData() {
    const token = this.getToken();

    if (!token) {
      return null;
    }

    return jwtDecode(token);
  }

  expired() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    localStorage.removeItem('token');

    this.fire(EVENT_EXPIRED);
  }

  logout() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    localStorage.removeItem('token');

    this.fire(EVENT_LOGGED_OUT);
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
    this.startTimeout(token);

    this.fire(EVENT_LOGGED_IN);
  }

  startTimeout(token: string) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.expired();
    }, this.secondsUntilExpired(token));
  }

  subscribe(handler: (event: string) => void) {
    this.handlers.push(handler);
  }

  unsubscribe(handlerToBeRemoved: (event: string) => void) {
    const currentHandlers = this.handlers;

    this.handlers = currentHandlers.filter((handler) => {
      return handler !== handlerToBeRemoved;
    });
  }

  fire(event: string) {
    for (const handler of this.handlers) {
      handler(event);
    }
  }

  secondsUntilExpired(token: string) {
    const decoded = jwtDecode<JwtPayload>(token);

    if (typeof decoded === 'undefined' || typeof decoded.exp === 'undefined') {
      return 0;
    }
    const diff = DateTime.fromFormat(decoded.exp.toString(), 'U').diff(DateTime.now());

    if (diff.seconds < 0) {
      return 0;
    }

    return diff.seconds;
  }

  isTokenExpired(token: string) {
    return this.secondsUntilExpired(token) === 0;
  }
}

const tokenStorage = new TokenStorage();

export default tokenStorage;
