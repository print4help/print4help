import tokenStorage from './TokenStorage';

export async function get(path: string) {
  const url = getUrl(path);
  const token = tokenStorage.getToken();

  const headers: HeadersInit = new Headers();
  headers.set('Content-Type', 'application/json');

  if (token !== null) {
    headers.set('X-AUTH-TOKEN', 'Bearer ' + token);
  }

  let response;

  try {
    response = await fetch(url, {
      method: 'GET',
      headers,
    });
  } catch (e) {
    throw new Error(`Api call Fail with method="GET", url="${url}", data="${{}}"`);
  }

  if (response.status === 401) {
    tokenStorage.expired();

    throw new Error('unauthorized');
  }

  return response;
}

export async function post(path: string, data: any) {
  const url = getUrl(path);
  const token = tokenStorage.getToken();

  const headers = {
    'Content-Type': 'application/json',
    'X-AUTH-TOKEN': '',
  };

  if (token !== null) {
    headers['X-AUTH-TOKEN'] = 'Bearer ' + token;
  }

  let response;

  try {
    response = await fetch(url, {
      method: 'POST',
      headers,
      body: JSON.stringify(data),
    });
  } catch (e) {
    throw new Error(`Api call Fail with method="POST", url="${url}", data="${data}"`);
  }

  if (response.status === 401) {
    tokenStorage.expired();

    throw new Error('unauthorized');
  }

  return response;
}

export async function upload(path: string, file: File) {
  const url = getUrl(path);
  const token = tokenStorage.getToken();

  const headers = {
    'X-AUTH-TOKEN': '',
  };

  if (token !== null) {
    headers['X-AUTH-TOKEN'] = 'Bearer ' + token;
  }

  let response;

  const formData = new FormData();
  formData.append('file', file);

  try {
    response = await fetch(url, {
      method: 'POST',
      headers,
      body: formData,
    });
  } catch (e) {
    throw new Error(`Api call Fail with method="POST", url="${url}", data="${file}"`);
  }

  if (response.status === 401) {
    tokenStorage.expired();

    throw new Error('unauthorized');
  }

  return response;
}

export async function login(email: string, password: string) {
  const url = getUrl('/login');
  const data = { username: email || '', password: password || '' };

  try {
    return await fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    });
  } catch (e) {
    throw new Error(`Api call Fail with method="POST", url="${url}", data="${data}"`);
  }
}

function getUrl(path: string) {
  return process.env.REACT_APP_API_URL + path;
}
