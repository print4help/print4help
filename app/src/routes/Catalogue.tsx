import React from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { CardColumns, Col, Row, Spinner } from 'react-bootstrap';
import { Piece } from '../domain/market/Piece';
import { PieceApi } from '../domain/market/PieceApi';
import PiecePreview from '../components/piece/PiecePreview';
import Page from '../components/global/Page';

interface IState {
  pieces: Piece[];
  loading: boolean;
}

class Catalogue extends React.PureComponent<WithTranslation, IState> {
  state: IState = {
    pieces: [],
    loading: false,
  };

  async componentDidMount() {
    await this.fetchPieces();
  }

  async fetchPieces() {
    this.setState({
      loading: true,
    });
    const pieces = await PieceApi.fetchList();
    this.setState({
      pieces,
      loading: false,
    });
  }

  render() {
    const { t } = this.props;
    const { pieces, loading } = this.state;

    return (
      <Page>
        <Row>
          <Col>
            <h1>{t('catalogue:title')}</h1>
          </Col>
        </Row>
        <Row>
          <Col sm={3}>{t('catalogue:filter.title')}</Col>
          {loading && (
            <Col className="justify-content-center d-flex">
              <Spinner animation="border" />
            </Col>
          )}
          {!loading && (
            <Col>
              <CardColumns>
                {pieces.map((piece) => (
                  <PiecePreview key={piece.articleNumber} piece={piece} />
                ))}
              </CardColumns>
            </Col>
          )}
        </Row>
      </Page>
    );
  }
}

export default withTranslation()(Catalogue);
