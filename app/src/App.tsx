import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import './App.scss';
import Index from './routes/Index';
import Faq from './routes/Faq';
import Catalogue from './routes/Catalogue';
import Piece from './routes/Piece';

class App extends React.Component<{}> {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Index} />
          <Route exact path="/catalogue" component={Catalogue} />
          <Route exact path="/catalogue/piece/:articleNumber" component={Piece} />
          <Route exact path="/faq" component={Faq} />
        </Switch>
      </Router>
    );
  }
}

export default App;
