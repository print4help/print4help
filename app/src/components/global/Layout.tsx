import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';

interface ILayoutProps {
  children: React.ReactNode;
}

class Layout extends React.PureComponent<ILayoutProps> {
  render() {
    return (
      <Container fluid>
        <Row>
          <Col md={{ span: 12, offset: 0 }} lg={{ span: 10, offset: 1 }}>
            {this.props.children}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Layout;
