import React from 'react';
import { Link } from 'react-router-dom';
import { WithTranslation, withTranslation } from 'react-i18next';
import { Breadcrumb, Container, Jumbotron, Nav, Navbar } from 'react-bootstrap';
import LanguageDropdown from '../LanguageDropdown';

class Header extends React.PureComponent<WithTranslation> {
  render() {
    const { t } = this.props;

    return (
      <React.Fragment>
        <Container>
          <Jumbotron>
            <h1 className="text-primary">{t('global:title')}</h1>
          </Jumbotron>
          <Navbar className="pl-0 pr-0">
            <Navbar.Brand href="/">{t('global:nav.brand')}</Navbar.Brand>
            <Nav className="mr-auto">
              <NavLink to="/">{t('global:nav.home')}</NavLink>
              <NavLink to="/catalogue">{t('global:nav.catalogue')}</NavLink>
              <NavLink to="/faq">{t('global:nav.faq')}</NavLink>
            </Nav>
            <LanguageDropdown />
          </Navbar>
        </Container>
        <Container fluid className="pl-0 pr-0">
          <Breadcrumb>
            <Container>
              <BreadCrumbItem to="/">{t('global:nav.home')}</BreadCrumbItem>
            </Container>
          </Breadcrumb>
        </Container>
      </React.Fragment>
    );
  }
}

interface LinkProps {
  to: string;
  children: string;
}

function NavLink({ to, children }: LinkProps) {
  return (
    <Link
      to={to}
      component={({ href }) => {
        return <Nav.Link href={href}>{children}</Nav.Link>;
      }}
    />
  );
}

function BreadCrumbItem({ to, children }: LinkProps) {
  return (
    <Link
      to={to}
      component={({ href }) => {
        return <Breadcrumb.Item href={href}>{children}</Breadcrumb.Item>;
      }}
    />
  );
}

export default withTranslation()(Header);
