import React from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { Container, Col, Row } from 'react-bootstrap';

class Footer extends React.PureComponent<WithTranslation> {
  render() {
    return (
      <Container fluid className="pl-0 pr-0 bg-light bg-light-gray mt-auto" style={{ height: '100px' }}>
        <Container>
          <Row className="pt-2">
            <Col>Footer..</Col>
            <Col>Footer..</Col>
            <Col>Footer..</Col>
            <Col>Footer..</Col>
          </Row>
        </Container>
      </Container>
    );
  }
}

export default withTranslation()(Footer);
