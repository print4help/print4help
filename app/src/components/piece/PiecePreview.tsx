import React from 'react';
import { Link } from 'react-router-dom';
import { WithTranslation, withTranslation } from 'react-i18next';
import { Piece } from '../../domain/market/Piece';
import { Card } from 'react-bootstrap';
import benchy from '../../assets/images/benchy-500.png';

interface IProps extends WithTranslation {
  piece: Piece;
}

class PiecePreview extends React.PureComponent<IProps> {
  public render() {
    const { piece } = this.props;

    return (
      <Card>
        <Link to={`/catalogue/piece/${piece.articleNumber}`}>
          <Card.Img variant="top" src={benchy} />
        </Link>
        <Card.Body>
          <Card.Title>
            <Link to={`/catalogue/piece/${piece.articleNumber}`}>{piece.name}</Link>
          </Card.Title>
          <Card.Text>{piece.summary}</Card.Text>
        </Card.Body>
      </Card>
    );
  }
}

export default withTranslation()(PiecePreview);
