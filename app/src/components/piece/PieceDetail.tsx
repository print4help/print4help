import React from 'react';
import { Col, Figure } from 'react-bootstrap';
import benchy from '../../assets/images/benchy-500.png';
import ReactMarkdown from 'react-markdown';
import ArticleNumber from './ArticleNumber';
import { withTranslation, WithTranslation } from 'react-i18next';
import { Piece } from '../../domain/market/Piece';

interface IProps extends WithTranslation {
  piece: Piece;
}

class PieceDetail extends React.PureComponent<IProps> {
  render() {
    const { t, piece } = this.props;

    return (
      <>
        <Col>
          <Figure>
            <Figure.Image alt={piece.name} src={benchy} />
            <Figure.Caption className="text-right">
              {piece.name} - {piece.summary}
            </Figure.Caption>
          </Figure>
          <h6>{t('catalogue:piece.licence')}</h6>
          <p>{piece.licence}</p>
        </Col>
        <Col>
          <article style={{ maxWidth: '100%' }}>
            <h1>{piece.name}</h1>
            <h2 className="h5">{piece.summary}</h2>
            <ArticleNumber articleNumber={piece.articleNumber} />

            <h3 className="h4 mt-5">{t('catalogue:piece.description')}</h3>
            <Text>{piece.description}</Text>

            <h3 className="h4 mt-5">{t('catalogue:piece.manufacturingNotes')}</h3>
            <Text>{piece.manufacturingNotes}</Text>

            <h3 className="h4 mt-5">{t('catalogue:piece.sourceUrl')}</h3>
            <a href={piece.sourceUrl} target="_blank" rel="noreferrer">
              {piece.sourceUrl}
            </a>
          </article>
        </Col>
      </>
    );
  }
}

interface TextProps extends WithTranslation {
  children?: string;
}

function text({ children, t }: TextProps) {
  if (typeof children === 'string' && children.length > 0) {
    return <ReactMarkdown>{children}</ReactMarkdown>;
  }

  return (
    <p className="text-muted">
      <small>{t('catalogue:piece.infoMissing')}</small>
    </p>
  );
}

const Text = withTranslation()(text);

export default withTranslation()(PieceDetail);
