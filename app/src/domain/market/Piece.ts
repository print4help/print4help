import { DateTime } from 'luxon';

export interface IPiece {
  id: string;
  userId?: string;
  articleNumber: string;
  status: PieceStatus;
  name: string;
  summary?: string;
  description?: string;
  manufacturingNotes?: string;
  licence?: string;
  sourceUrl?: string;
  createdAt: DateTime;
  updatedAt: DateTime;
}

export enum PieceStatus {
  DRAFT = 'draft',
  IN_REVIEW = 'in-review',
  APPROVED = 'approved',
  REJECTED = 'rejected',
  BLOCKED = 'blocked',
}

export class Piece implements IPiece {
  public id: string;
  public userId?: string;
  public articleNumber: string;
  public status: PieceStatus;
  public name: string;
  public summary?: string;
  public description?: string;
  public manufacturingNotes?: string;
  public licence?: string;
  public sourceUrl?: string;
  public createdAt: DateTime;
  public updatedAt: DateTime;

  private constructor(data: IPiece) {
    this.id = data.id;
    this.userId = data.userId;
    this.articleNumber = data.articleNumber;
    this.status = data.status;
    this.name = data.name;
    this.summary = data.summary;
    this.description = data.description;
    this.manufacturingNotes = data.manufacturingNotes;
    this.licence = data.licence;
    this.sourceUrl = data.sourceUrl;
    this.createdAt = data.createdAt;
    this.updatedAt = data.updatedAt;
  }

  public static deserialize(data: any): Piece {
    return new Piece({
      id: data.id,
      userId: data.userId,
      articleNumber: data.articleNumber,
      status: data.status as PieceStatus,
      name: data.name,
      summary: data.summary,
      description: data.description,
      manufacturingNotes: data.manufacturingNotes,
      licence: data.licence,
      sourceUrl: data.sourceUrl,
      createdAt: DateTime.fromISO(data.createdAt),
      updatedAt: DateTime.fromISO(data.updatedAt),
    });
  }

  public serialize(): string {
    const data = {
      id: this.id,
      userId: this.userId,
      articleNumber: this.articleNumber,
      status: this.status,
      name: this.name,
      summary: this.summary,
      description: this.description,
      manufacturingNotes: this.manufacturingNotes,
      licence: this.licence,
      sourceUrl: this.sourceUrl,
      createdAt: this.createdAt.toISO(),
      updatedAt: this.updatedAt.toISO(),
    };

    return JSON.stringify(data);
  }
}
